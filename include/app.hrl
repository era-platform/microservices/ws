%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 18.05.2021

%% ====================================================================
%% Types
%% ====================================================================
   
%% ====================================================================
%% Constants and defaults
%% ====================================================================

-define(ClassesCN, <<"classes">>).
-define(UsersCN, <<"users">>).
-define(SessionsCN, <<"sessions">>).
-define(ConnectionsCN, <<"connections">>).

-define(MsvcDms, <<"dms">>).
-define(MsvcSubscr, <<"subscr">>).
-define(MsvcIam, <<"iam">>).
-define(MsvcDc, <<"dc">>).

-define(CookieSession, <<"Session">>).
-define(MakeToken(SessionId,Domain), ?BU:strbin("~ts@~ts", [SessionId,Domain])).
-define(ParseToken(Token), case binary:split(Token,<<"@">>) of [SessionId_0,Domain_0] -> {ok,SessionId_0,Domain_0}; _ -> error end).
-define(SessionTTL, 86400).
-define(ServerName, <<"Era">>).

-define(NoAuthDomain, 'no_auth').

-type proplist() :: [{Key::atom()|binary(),Value::term()}].

%% ====================================================================
%% Define modules
%% ====================================================================

-define(BASICLIB, basiclib).
-define(PLATFORMLIB, platformlib).
-define(WEBSERVERLIB, webserverlib).

-define(APP, ws).
-define(MsvcType, <<"ws">>).

-define(SUPV, ws_supv).
-define(CFG, ws_config).
-define(WsUtils, ws_utils).

-define(FixturerCallback, ws_svc_fixturer_callback).
-define(FixturerLeaderName, 'ws_fixturer_leader_srv').

-define(FixturerClasses, ws_svc_fixture_coll_classes).

-define(VALIDATOR, ws_svc_validator_srv).
-define(ValidatorSessions, ws_svc_validator_coll_sessions).
-define(ValidatorConnections, ws_svc_validator_coll_connections).

-define(DomainTreeCallback, ws_domaintree_callback).

-define(DSUPV, ws_svc_domain_supv).
-define(DSRV, ws_svc_domain_srv).
-define(DSearchSrv, ws_svc_domain_search_srv).
-define(DModifySrv, ws_svc_domain_modify_srv).
-define(DCrud, ws_svc_domain_crud).
-define(DU, ws_svc_domain_utils).

-define(NoAuth_Supv, ws_svc_noauth_supv).
-define(NoAuth_Srv, ws_svc_noauth_srv).
-define(NoAuth_ModifySrv, ws_svc_noauth_modify_srv).

-define(HTTPU, ws_rest_utils_http).
-define(COWBU, ws_rest_utils_cowboy).
-define(MPART, ws_rest_utils_multipart).
-define(BAN, ws_rest_utils_ban).

-define(FILESVC, ws_rest_utils_file_service).
-define(FILER, ws_rest_utils_file_router).
-define(FILEU, ws_rest_utils_file_utils).
-define(FILEPATHS, ws_rest_utils_file_paths).

-define(RoutesRest, ws_rest_routes).
-define(RoutesWebsocket, ws_websocket_routes).
-define(RoutesStatic, ws_static_routes).

%% ------
%% From basiclib
%% ------
-define(BU, basiclib_utils).
-define(BLmulticall, basiclib_multicall).
-define(BLstore, basiclib_store).
-define(BLmonitor, basiclib_monitor_srv).
-define(BLlog, basiclib_log).
-define(BLselect, basiclib_collection_select).
-define(BLfilelib, basiclib_wrapper_filelib).
-define(BLfile, basiclib_wrapper_file).
-define(BLzip, basiclib_wrapper_zip).
-define(BLrpc, basiclib_rpc).

%% ------
%% From platformlib
%% ------
-define(PCFG, platformlib_config).

-define(GLOBAL, platformlib_globalnames_global).
-define(GN_NAMING, platformlib_globalnames_naming).
-define(GN_REG, platformlib_globalnames_registrar).

-define(LeaderSupv, platformlib_leader_app_supv).
-define(LeaderU, platformlib_leader_app_utils).

-define(DMS_CACHE, platformlib_dms_cache).
-define(DMS_CRUD, platformlib_dms_crud).
-define(DMS_ATTACH, platformlib_dms_crud_attach).
-define(DMS_ATTACH_COMPLEX, platformlib_dms_crud_attach_complex).
-define(DMS_SUBSCR, platformlib_dms_subscribe_helper).
-define(DMS_FIXTURE_HELPER, platformlib_dms_fixture_helper).
-define(DMS_FIXTURER_SUPV, platformlib_dms_fixturer_leader_supv).

-define(DTREE_SUPV, platformlib_domaintree_supv).

-define(COPIER, platformlib_filestorage_copier_srv).

%% -----
%% From webserverlib
%% -----
-define(WSL_STATIC, webserverlib_handler_static).

%% ====================================================================
%% Define logs
%% ====================================================================

-define(LOGFILE, {ws,?APP}).

-define(LOG(Level,Fmt,Args), ?BLlog:write(Level, ?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(LOG(Level,Text), ?BLlog:write(Level, ?CFG:log_destination(?LOGFILE), Text)).

-define(OUT(Level,Fmt,Args), ?BLlog:writeout(Level, ?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(OUT(Level,Text), ?BLlog:writeout(Level, ?CFG:log_destination(?LOGFILE), Text)).

%% ====================================================================
%% Define other
%% ====================================================================
