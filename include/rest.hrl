%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.06.2021

-define(BASIC_METHODS, [<<"GET">>,<<"HEAD">>,<<"OPTIONS">>,<<"POST">>]). % these do not get fallback URLs
-define(FB_METHOD_SEP, <<"!">>). % fallback method separator

%% behavior modules
-define(H_GEN, ws_rest_behavior_generic).
-define(H_UNV, ws_rest_behavior_universal).
-define(H_FALLBACK, ws_rest_behavior_sub_fallback).
-define(H_BASE_BEHAVIOR, ws_rest_behavior_sub_base).
%-define(H_FILE, ws_rest_behavior_sub_file).
-define(H_FOLDER, ws_rest_behavior_sub_folder).
%-define(H_ZIPFOLDER, ws_rest_behavior_sub_zipfolder).

%% utility modules
-define(RESTU, ws_rest_behavior_utils).
-define(RESTU_ERR, ws_rest_behavior_utils_error).
-define(RESTU_IAM, ws_rest_behavior_utils_iam).
-define(RESTU_MSG, ws_rest_behavior_utils_msg).
-define(RESTU_FILE, ws_rest_behavior_utils_file).
-define(RESTU_FS, ws_rest_behavior_utils_fs).
%-define(RESTU_SCR, ws_rest_behavior_utils_scripts).
%-define(RESTU_SCR_RT, ws_rest_behavior_utils_script_runtimes).

%% limits
-define(REST_MAX_CONTENT_LENGTH_JSON, 1024*1024*2). % 2 Mb
-define(REST_MAX_CONTENT_LENGTH_FILE, 1024*1024*512). % 512 Mb

