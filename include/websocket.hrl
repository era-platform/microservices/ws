%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.06.2021

%% ====================================================================
%% Define records and types
%% ====================================================================

-type json_term() :: binary() | number() | boolean() | map() | [json_term()].

%% -------------------------------
%% Basic websocket connection states
%% -------------------------------
-define(Inited, 'inited').
-define(Authenticated, 'authenticated').

%% -------------------------------
%% General websocket connection StateData type
%% -------------------------------
-record(wsstate, {
    state = ?Inited,
    connid, % connection unique id
    pid, % pid of cowboy's connection fsm
    domain, % current domain
    sessionid, % session id, could be undefined
    userid, % for user api
    %
    capabilities = [], % for user api
    api_modules = [],
    %
    setup_params = [], % from setup message (static params of roleapp)
    appPath = undefined, % from setup message (basic path from app url)
    %
    initial_req, % incoming http request
    peer, % remote address
    path, % path to websocket endpoint
    %
    opts,
    data :: map(), % custom hash, managed by ?WsStore:store_data etc.
    timerref,
    debug_level = 0
}).

%% -------------------------------
%% Info about client requests waiting responses
%% -------------------------------
-record(resp_waiter, {
    qid :: json_term(),
    pid :: pid(),
    ts :: non_neg_integer(),
    expire_ts :: non_neg_integer()
}).

%% Name of ets for waitors.
-define(WAITREQS, 'waitreqs').

%%
-type conn_data() :: map().

%% ====================================================================
%% Define modules
%% ====================================================================

-define(WsCap, ws_websocket_utils_cap).
-define(WsMake, ws_websocket_utils_make).
-define(WsProc, ws_websocket_utils_proc).
-define(WsReg, ws_websocket_utils_reg).
-define(WsReply, ws_websocket_utils_reply).
-define(WsSess, ws_websocket_utils_session).
-define(WsStore, ws_websocket_utils_store).
-define(WsTmp, ws_websocket_utils_tmpurl).
-define(WsWaiter, ws_websocket_utils_waiter).


-define(FsmInited, ws_websocket_capabilities_fsm_inited).
-define(FsmAuthenticated, ws_websocket_capabilities_fsm_authenticated).

-define(CapRest, ws_websocket_capabilities_general_rest).
-define(CapSubscr, ws_websocket_capabilities_general_subscr).