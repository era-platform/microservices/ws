%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 27.06.2021
%%% @doc Filesystem target temp REST endpoint.

-module(ws_rest_endpoints_temp_upload_handler).
-author('Peter Bukashin <tbotc@yandex.ru>').


-export([init/2]).

-export([
    allowed_methods/2,
    destination_info/2
]).

-export([
    after_upload_file/2,
    after_upload_files/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("rest.hrl").
-include("ws_rest_endpoints_temp.hrl").

%% ====================================================================
%% Cowboy initial handler callback
%% ====================================================================

init(Req, Opts) -> {?H_UNV, Req, Opts}.

%% ====================================================================
%% REST behaviour callback
%% ====================================================================

allowed_methods(Req, State) ->
    {[<<"POST">>,<<"OPTIONS">>], Req, State}.

%% --------------------------------------------
%% On 'forbidden' callback fills state by destination type
%% --------------------------------------------
destination_info(Req, State) ->
    case cowboy_req:method(Req) of
        <<"OPTIONS">> -> ?UploadDestinationInfo:build_destination_info_for_options(Req, State);
        _ -> ?UploadDestinationInfo:build_destination_info(Req, State)
    end.

%% --------------------------------------------
after_upload_file(Req,State) ->
    ?UploadFiles:after_upload_file(Req,State).

after_upload_files(Req,State) ->
    ?UploadFiles:after_upload_files(Req,State).