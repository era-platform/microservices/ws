%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 27.06.2021
%%% @doc

-module(ws_rest_endpoints_temp_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_path/1,
         get_domain/2]).

-export([node_temp_path/0]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("rest.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ----------------------------------------------
%% Return path of current request
%% ----------------------------------------------
get_path(Req) ->
    ?BU:urldecode(cowboy_req:path(Req)).

%% ----------------------------------------------
%% Return domain of current request
%% ----------------------------------------------
get_domain(Req,State) ->
    case ?RESTU:check_req_get_domain(Req, State) of
        {ok,Domain,_} -> Domain;
        _ -> undefined
    end.

%% -----------------------------
%% Return node temp path
%% -----------------------------
node_temp_path() ->
    filename:join([
        ?FILEPATHS:get_node_temp_path(),
        "upload_download"
    ]).

%% ====================================================================
%% Internal functions
%% ====================================================================