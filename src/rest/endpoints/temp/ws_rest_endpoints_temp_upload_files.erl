%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 16.06.2021
%%% @doc Implementation of after upload temp file methods

-module(ws_rest_endpoints_temp_upload_files).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    after_upload_file/2,
    after_upload_files/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("rest.hrl").
-include("ws_rest_endpoints_temp.hrl").

-define(TTL,180).

%% ====================================================================
%% Public functions
%% ====================================================================

%% ------------------------------
%% notify after single file upload operation
%% upload file to file storage, set fileinfo to attachment property
%% ------------------------------
after_upload_file(Req,State) ->
    DI = maps:get('destination_info',State),
    DirPath = maps:get('filesystem_dirpath',DI),
    spawn(fun() -> timer:sleep(?TTL*1000), catch ?BU:directory_delete(DirPath) end),
    UploadedFiles0 = maps:get('uploaded_files',State),
    Content = case lists:filtermap(fun({file,_FilePathF,ok}) -> true; (_) -> false end, UploadedFiles0) of
                  [UploadedFile|_] -> lists:nth(1,make_response_content([UploadedFile],maps:get('tempdir',DI)));
                  _ -> make_response_content(UploadedFiles0,maps:get('tempdir',DI))
              end,
    {forward,?RESTU:send_response(Content, Req, State)}.

%% ------------------------------
%% notify after multi files upload operation
%% upload file(s) to file storage, append fileinfo(s) to attachment property
%% ------------------------------
after_upload_files(Req,State) ->
    DI = maps:get('destination_info',State),
    DirPath = maps:get('filesystem_dirpath',DI),
    spawn(fun() -> timer:sleep(?TTL*1000), catch ?BU:directory_delete(DirPath) end),
    UploadedFiles0 = maps:get('uploaded_files',State),
    Content = make_response_content(UploadedFiles0,maps:get('tempdir',DI)),
    {forward,?RESTU:send_response(Content, Req, State)}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
make_response_content(UploadedFiles,TempDir) ->
    lists:map(fun({file,FilePath,ok}=FileRes) ->
                     [Map] = ?RESTU_FILE:format_uploaded_statuses([FileRes]),
                     TempDirL = ?BU:to_unicode_list(TempDir),
                     FilePathL = ?BU:to_unicode_list(FilePath),
                     {TempDirL,RelPathL} = lists:split(length(TempDirL),FilePathL),
                     Map#{url => ?BU:strbin("temp://~ts", [string:trim(RelPathL,leading,[$/])]),
                          expires => ?TTL};
                 (FileRes) ->
                     [Map] = ?RESTU_FILE:format_uploaded_statuses([FileRes]),
                     Map
              end, UploadedFiles).