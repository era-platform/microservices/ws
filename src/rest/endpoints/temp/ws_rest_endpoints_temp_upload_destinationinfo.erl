%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 18.06.2021
%%% @doc Routines to aprior build of destionation info map
%%%      Used by handler to determine next algorithm and use previously computed data

-module(ws_rest_endpoints_temp_upload_destinationinfo).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([build_destination_info/2]).
-export([build_destination_info_for_options/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("rest.hrl").
-include("ws_rest_endpoints_temp.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------------------------------------
%% Define destination and return map() for next operations and behavior process
%% Keys:
%%      domain :: root | binary(),
%%      method :: <<"POST">> | <<"OPTIONS">>
%%      resource_type :: folder,
%%      file_mode :: '$multi'
%%      tempdir :: string(),
%%      filesystem_dirpath :: binary()]
%% --------------------------------------------
build_destination_info(Req, State) ->
    Method = ?RESTU:method(Req, State),
    Domain = ?TempU:get_domain(Req,State),
    TempDir = ?TempU:node_temp_path(),
    TempDir2 = ?BU:to_binary(filename:join([TempDir,?BU:str("~p-~5..0B",[?BU:timestamp(),?BU:random(100000)])])),
    {ok, #{'domain' => Domain,
           'method' => Method,
           'resource_type' => 'folder',
           'file_mode' => '$multi',
           'tempdir' => TempDir,
           'filesystem_dirpath' => ?BU:to_binary(TempDir2)}}.

%% --------------------------------------------
build_destination_info_for_options(Req, State) ->
    Domain = ?TempU:get_domain(Req,State),
    {ok, #{'resource_type' => 'collection',
           'domain' => Domain}}.
