%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 16.06.2021
%%% @doc Implementation of attachments crud methods

-module(ws_rest_endpoints_model_attachments).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([after_upload_file/2,
         after_upload_files/2,
         after_delete_file/2,
         list_folder/2,
         clear_folder/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("rest.hrl").
-include("ws_rest_endpoints_model.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ------------------------------
%% notify after single file upload operation
%% upload file to file storage, set fileinfo to attachment property
%% ------------------------------
after_upload_file(Req,State) ->
    DI = maps:get('destination_info',State),
    [Domain,ClassItem,Property,Id,DirPath] = ?BU:maps_get(['domain','class_item','property','id','filesystem_dirpath'],DI),
    [UploadedFilePath|_] = lists:filtermap(fun({file,FilePathF,ok}) -> {true,FilePathF}; (_) -> false end, maps:get('uploaded_files',State)),
    Res = ?DMS_ATTACH_COMPLEX:upload_complex(Domain,ClassItem,Id,Property,build_opts_for_attachment_complex(DI,State),UploadedFilePath),
    catch ?BLfile:delete(UploadedFilePath),
    catch ?BU:directory_delete(DirPath),
    case Res of
        {ok,_FileInfo,_Entity} -> {ok,Req,State};
        {error,_}=Err -> Err
    end.

%% ------------------------------
%% notify after multi files upload operation
%% upload file(s) to file storage, append fileinfo(s) to attachment property
%% ------------------------------
after_upload_files(Req,State) ->
    DI = maps:get('destination_info',State),
    [Domain,ClassItem,Property,Id,DirPath] = ?BU:maps_get(['domain','class_item','property','id','filesystem_dirpath'],DI),
    UploadedFiles0 = maps:get('uploaded_files',State),
    UploadedFiles1 = lists:filtermap(fun({file,FilePath,ok}) -> {true,FilePath}; (_) -> false end, UploadedFiles0),
    Res = ?DMS_ATTACH_COMPLEX:upload_multi_complex(Domain,ClassItem,Id,Property,build_opts_for_attachment_complex(DI,State),UploadedFiles1),
    lists:foreach(fun(FilePath) -> catch ?BLfile:delete(FilePath) end, UploadedFiles1),
    catch ?BU:directory_delete(DirPath),
    case Res of
        {ok,_FileInfos,_Entity} -> {ok,Req,State};
        {error,_}=Err -> Err
    end.

%% ------------------------------
%% notify after delete file operation
%% delete file from file storage, delete fileinfo from attachment property
%% ------------------------------
after_delete_file(Req,State) ->
    DI = maps:get('destination_info',State),
    [Domain,ClassItem,Property,Id] = ?BU:maps_get(['domain','class_item','property','id'],DI),
    Res = ?DMS_ATTACH_COMPLEX:delete_complex(Domain,ClassItem,Id,Property,build_opts_for_attachment_complex(DI,State)),
    case Res of
        {ok,_Entity} -> {ok,Req,State};
        {error,_}=Err -> Err
    end.

%% ------------------------------
%% return list of file info from entity's property
%% ------------------------------
list_folder(Req,State) ->
    % TODO: request file info from storage (local cache returned from entity, storage info returned from attached property)
    % extract context
    DI = maps:get('destination_info',State),
    [Domain,CN,ClassItem,Property] = ?BU:maps_get(['domain','classname','class_item','property'],DI),
    try
        case read_entity(Domain,CN,ClassItem,DI,State) of
            {ok,Entity} ->
                % from entity
                Attaches = maps:get(Property,Entity,[]),
                {forward, ?RESTU:to_json({Attaches, Req, State})};
            {error,_}=Err -> Err
        end
    catch
        throw:{error,_}=ErrC -> ErrC;
        E:R:ST ->
            ?LOG('$crash',"File operation ('~ts', '~ts') crashed: {~120tp,~120tp}~n\tStack: ~120tp",[Domain,CN,E,R,ST]),
            {error,{500,?BU:strbin("File operation crashed. See logs of '~ts'", [node()])}}
    end.

%% ------------------------------
%% clear files (by attachment property meta) from entity's property
%% ------------------------------
clear_folder(Req,State) ->
    DI = maps:get('destination_info',State),
    [Domain,ClassItem,Property,Id] = ?BU:maps_get(['domain','class_item','property','id'],DI),
    Res = ?DMS_ATTACH_COMPLEX:clear_complex(Domain,ClassItem,Id,Property,build_opts_for_attachment_complex(DI,State)),
    case Res of
        {ok,_Entity} -> {ok,Req,State};
        {error,_}=Err -> Err
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
build_opts_for_attachment_complex(DI,State) ->
    Opts0 = #{'initiator' => {'user',maps:get('user_id',maps:get('iam',State))}},
    Opts1 = case maps:get('subid',DI,undefined) of
                undefined -> Opts0;
                SubId -> Opts0#{'filename' => SubId}
            end,
    Opts2 = case maps:get('prepath',DI,undefined) of
                undefined -> Opts1;
                Prepath -> Opts1#{'prepath' => Prepath}
            end,
    Opts2.

%%
read_entity(Domain,CN,ClassItem,DI,State) ->
    case maps:get('entity',DI,undefined) of
        Entity when is_map(Entity) -> {ok,Entity};
        _ ->
            % build operation params
            Map0 = ?MU:build_crud_map(DI,State),
            % build reply funciton
            FunReply = fun(Reply) -> Reply end,
            ?DMS_CRUD:crud({Domain,'read',ClassItem,CN,Map0,[]}, FunReply)
    end.