%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 12.06.2021
%%% @doc Implementation of json CRUD methods

-module(ws_rest_endpoints_model_methods).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    get/2,
    post/2,
    put/2,
    patch/2,
    delete/2,
    lookup/2,
    clear/2
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("rest.hrl").
-include("ws_rest_endpoints_model.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---------------------------------
%% GET /rest/v1/... -> list entities from collection
%% GET /rest/v1/.../:id -> get entity
%% GET /rest/v1/.../:partition_path/:id -> get entity
%% ---------------------------------
get(Req, State) ->
    Domain = ?MU:get_domain(Req,State),
    case route(Req,State) of
        {ok,'collection',ClassItem,CN,[]} ->
            make_crud_request({Domain,'read',ClassItem,CN,#{'object' => 'collection'},[]}, Req, State);
        {ok,'entity',ClassItem,CN,[Id]} ->
            make_crud_request({Domain,'read',ClassItem,CN,#{'object' => {'entity',Id,[]}},[]}, Req, State);
        {ok,'path',ClassItem,CN,PathParts} ->
            make_crud_request({Domain,'read',ClassItem,CN,#{'object' => {'path',PathParts}},[]}, Req, State);
        {ok,_,_ClassItem,_CN,_Path} ->
            ?RESTU:send_error(418, Req, State) % not implemented
    end.

%% ---------------------------------
%% POST /rest/v1/... -> new entity to collection
%% ---------------------------------
post(Req, State) ->
    Domain = ?MU:get_domain(Req,State),
    case route(Req,State) of
        {ok,'collection',ClassItem,CN,[]} ->
            make_crud_request({Domain,'create',ClassItem,CN,#{'object' => 'collection'},[body]}, Req, State);
        {ok,'entity',_ClassItem,_CN,_} ->
            ?RESTU:send_error(405, Req, State); % method not allowed
        {ok,'path',_ClassItem,_CN,_} ->
            ?RESTU:send_error(405, Req, State); % method not allowed
        {ok,_,_ClassItem,_CN,_Path} ->
            ?RESTU:send_error(418, Req, State) % not implemented
    end.

%% ---------------------------------
%% PUT /rest/v1/.../:id -> create/replace entity
%% PUT /rest/v1/.../:partition_path/:id -> create/replace entity
%% ---------------------------------
put(Req, State) ->
    Domain = ?MU:get_domain(Req,State),
    case route(Req,State) of
        {ok,'collection',_ClassItem,_CN,[]} ->
            ?RESTU:send_error(405, Req, State); % method not allowed
        {ok,'entity',ClassItem,CN,[Id]} ->
            make_crud_request({Domain,'replace',ClassItem,CN,#{'object' => {'entity',Id,[]}},[body]}, Req, State);
        {ok,'path',ClassItem,CN,PathParts} ->
            make_crud_request({Domain,'replace',ClassItem,CN,#{'object' => {'path',PathParts}},[body]}, Req, State);
        {ok,_,_ClassItem,_CN,_PathParts} ->
            ?RESTU:send_error(418, Req, State) % not implemented
    end.

%% ---------------------------------
%% PATCH /rest/v1/.../:id -> update entity
%% PATCH /rest/v1/.../:partition_path/:id -> update entity
%% ---------------------------------
patch(Req, State) ->
    Domain = ?MU:get_domain(Req,State),
    case route(Req,State) of
        {ok,'collection',_ClassItem,_CN,[]} ->
            ?RESTU:send_error(405, Req, State); % method not allowed
        {ok,'entity',ClassItem,CN,[Id]} ->
            make_crud_request({Domain,'update',ClassItem,CN,#{'object' => {'entity',Id,[]}},[body]}, Req, State);
        {ok,'path',ClassItem,CN,PathParts} ->
            make_crud_request({Domain,'update',ClassItem,CN,#{'object' => {'path',PathParts}},[body]}, Req, State);
        {ok,_,_ClassItem,_CN,_Path} ->
            ?RESTU:send_error(418, Req, State) % not implemented
    end.

%% ---------------------------------
%% DELETE /rest/v1/.../:id -> delete entity
%% DELETE /rest/v1/.../:partition_path/:id -> delete entity
%% ---------------------------------
delete(Req, State) ->
    Domain = ?MU:get_domain(Req,State),
    case route(Req,State) of
        {ok,'collection',_ClassItem,_CN,[]} ->
            ?RESTU:send_error(405, Req, State); % method not allowed
        {ok,'entity',ClassItem,CN,[Id]} ->
            make_crud_request({Domain,'delete',ClassItem,CN,#{'object' => {'entity',Id,[]}},[]}, Req, State);
        {ok,'path',ClassItem,CN,PathParts} ->
            make_crud_request({Domain,'delete',ClassItem,CN,#{'object' => {'path',PathParts}},[]}, Req, State);
        {ok,_,_ClassItem,_CN,_Path} ->
            ?RESTU:send_error(418, Req, State) % not implemented
    end.

%% ---------------------------------
%% LOOKUP /rest/v1/... -> search entities in collection
%% ---------------------------------
lookup(Req, State) ->
    Domain = ?MU:get_domain(Req,State),
    case route(Req,State) of
        {ok,'collection',ClassItem,CN,[]} ->
            make_crud_request({Domain,'lookup',ClassItem,CN,#{'object' => 'collection'},[body]}, Req, State);
        {ok,'entity',_ClassItem,_CN,[_Id]} ->
            ?RESTU:send_error(405, Req, State); % method not allowed
        {ok,'path',_ClassItem,_CN,[_Id|_RestParts]} ->
            ?RESTU:send_error(405, Req, State); % method not allowed
        {ok,_,_ClassItem,_CN,_Path} ->
            ?RESTU:send_error(418, Req, State) % not implemented
    end.

%% ---------------------------------
%% CLEAR /rest/v1/... -> remove all entities from collection
%% ---------------------------------
clear(Req, State) ->
    Domain = ?MU:get_domain(Req,State),
    case route(Req,State) of
        {ok,'collection',ClassItem,CN,[]} ->
            make_crud_request({Domain,'clear',ClassItem,CN,#{'object' => 'collection'},[]}, Req, State);
        {ok,'entity',_ClassItem,_CN,[_Id]} ->
            ?RESTU:send_error(405, Req, State); % method not allowed
        {ok,'path',_ClassItem,_CN,_PathParts} ->
            ?RESTU:send_error(405, Req, State); % method not allowed
        {ok,_,_ClassItem,_CN,_Path} ->
            ?RESTU:send_error(418, Req, State) % not implemented
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----------------------------
%% instead of duplicated call to ?MU:parse_path(Domain,?MU:get_path(Req))
%% -----------------------------
route(_Req,State) ->
    DI = maps:get('destination_info',State),
    CN = maps:get('classname',DI),
    ClassItem = maps:get('class_item',DI),
    case maps:get('resource_type',DI) of
        'collection' ->
            {ok,'collection',ClassItem,CN,[]};
        'entity' ->
            Id = maps:get('id',DI),
            case maps:get('prepath',DI,undefined) of
                undefined -> {ok,'entity',ClassItem,CN,[Id]};
                Prepath -> {ok,'path',ClassItem,CN,[Prepath,Id]}
            end;
        'file' ->
            Id = maps:get('id',DI),
            {ok,'file',ClassItem,CN,[Id]};
        'folder' ->
            Id = maps:get('id',DI),
            {ok,'folder',ClassItem,CN,[Id]}
    end.

%% -----------------------------------
%% Call to service
%% -----------------------------------
%% when CN == sessions
make_crud_request({Domain,Operation,ClassItem,?SessionsCN=CN,Map0,Opts},Req,State) ->
    Map1 = ?Sessions:update_map(Map0,State),
    FunReply = ?Sessions:get_reply_fun({Domain,Operation,Map1},fun apply_result/4,Domain,Req,State),
    make_crud_request_1({Domain,Operation,ClassItem,CN,Map1,Opts},FunReply,Req,State);
%% other CN
make_crud_request({Domain,Operation,ClassItem,CN,Map0,Opts},Req,State) ->
    FunReply = fun(Reply) -> apply_result(Reply,Domain,Req,State) end,
    make_crud_request_1({Domain,Operation,ClassItem,CN,Map0,Opts},FunReply,Req,State).

%% @private next
make_crud_request_1({Domain,Operation,ClassItem,CN,Map0,Opts},FunReply,Req,State) ->
    Map1 = Map0#{'qs' => cowboy_req:parse_qs(Req),
                 'initiator' => {'user',maps:get('user_id',maps:get('iam',State,#{}),undefined)}},
    case lists:member('body',Opts) of
        false ->
            % NO BODY NEED
            ?DMS_CRUD:crud({Domain,Operation,ClassItem,CN,Map1,Opts}, FunReply);
        true ->
            MaxEntitySize = 1000000, % TODO ICP: max entity size could be optionally set by class
            case cowboy_req:body_length(Req) of
                I when I=<0 -> ?RESTU:send_error(413, {invalid_params,?BU:strbin("Content expected",[])}, Req, State);
                I when I>MaxEntitySize -> ?RESTU:send_error(413, {invalid_params,?BU:strbin("Too large content. Expected max ~p bytes",[MaxEntitySize])}, Req, State);
                _ ->
                    case maps:get(input_type,State,undefined) of
                        undefined ->
                            ?RESTU:send_error(400, {invalid_params,<<"Invalid content format. Expected JSON term">>}, Req, State);
                        InputType ->
                            % Body JSON object read from generic
                            Content = case InputType of
                                          object -> maps:get(input_map,State);
                                          array -> maps:get(input_array,State);
                                          string -> maps:get(input_string,State);
                                          number -> maps:get(input_number,State);
                                          boolean -> maps:get(input_boolean,State)
                                      end,
                            ?DMS_CRUD:crud({Domain,Operation,ClassItem,CN,Map1#{content => Content},Opts}, FunReply)
                    end end end.

%% @private
apply_result(Res,Domain,Req,State) ->
    Method = cowboy_req:method(Req),
    case Res of
        {'EXIT',Reason} ->
            ?LOG('$crash', "DMS call to '~ts' crashed: ~120tp",[Domain, Reason]),
            ?RESTU:send_error({internal_error,?BU:strbin("DMS call crashed. See error reason in logs of '~ts'",[node()])}, Req, State);
        {error,{Code,Reason}} when is_integer(Code) -> ?RESTU:send_error(Code, Reason, Req, State);
        {error,{Code,Reason}} when is_atom(Code) -> ?RESTU:send_error({Code,Reason}, Req, State);
        {error,Reason} when is_binary(Reason) -> ?RESTU:send_error(Reason, Req, State);
        {error,Reason} ->
            % Hide real reason, only in logs
            ?LOG('$error', "DMS call to '~ts' error: ~120tp",[Domain, Reason]),
            Reason1 = ?BU:strbin("See error reason in logs of '~ts'",[node()]),
            ?RESTU:send_error(Reason1, Req, State);
        {ok,Result} when Method==<<"GET">> ->
            {?BU:filter_jsonable(Result), Req, State};
        {ok,Result} ->
            ?RESTU:send_response(?BU:filter_jsonable(Result), Req, State);
        ok -> ?RESTU:send_response(204, Req, State)
    end.