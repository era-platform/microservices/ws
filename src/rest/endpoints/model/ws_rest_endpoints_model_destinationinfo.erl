%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 18.06.2021
%%% @doc Routines to aprior build of destionation info map
%%%      Used by handler to determine next algorithm and use previously computed data

-module(ws_rest_endpoints_model_destinationinfo).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([build_destination_info/2]).
-export([build_destination_info_for_options/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("rest.hrl").
-include("ws_rest_endpoints_model.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------------------------------------
%% Define destination and return map() for next operations and behavior process
%% Keys:
%%      domain :: root | binary(),
%%      class_item :: map(),
%%      classname :: binary(),
%%      resource_type :: collection | entity | file | folder,
%%      path :: [Part::binary()],
%%      id :: binary(),
%%      prepath :: binary(),
%%      property :: binary(),
%%      subid :: binary(),
%%    when files/folders:
%%      file_mode :: '$single' | '$multi' | '$file_in_folder'
%%      filesystem_filepath :: binary(),
%%      filename :: binary(),
%%      filesystem_dirpath :: binary()]
%% --------------------------------------------
build_destination_info(Req, State) ->
    Domain = ?MU:get_domain(Req,State),
    Path = ?MU:get_path(Req),
    do_build_destination_info(Domain,Path,Req,State).

%% --------------------------------------------
build_destination_info_for_options(Req, State) ->
    Domain = ?MU:get_domain(Req,State),
    Path = ?MU:get_path(Req),
    F = fun(ClassItem,CN) ->
            {ok, #{'resource_type' => 'collection',
                   'domain' => Domain,
                   'classname' => CN,
                   'class_item' => ClassItem,
                   'path' => []}}
        end,
    case ?MU:parse_path(Domain, Path) of
        {ok,'collection',ClassItem,CN,_} -> F(ClassItem,CN);
        {ok,'entity',ClassItem,CN,_} -> F(ClassItem,CN);
        {ok,'path',ClassItem,CN,_} -> F(ClassItem,CN);
        {error,_}=Err -> Err
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
do_build_destination_info(Domain, Path, Req, State) ->
    try
        case ?MU:parse_path(Domain, Path) of
            {ok,'collection',ClassItem,CN,[]} ->
                {ok, #{'resource_type' => 'collection',
                       'domain' => Domain,
                       'classname' => CN,
                       'class_item' => ClassItem,
                       'path' => []}};
            {ok,'entity',ClassItem,CN,[Id]} ->
                {ok, #{'resource_type' => 'entity',
                       'domain' => Domain,
                       'classname' => CN,
                       'class_item' => ClassItem,
                       'id' => Id,
                       'path' => []}};
            {ok,'path',ClassItem,CN,PathParts} ->
                Dinfo = #{'domain' => Domain,
                          'classname' => CN,
                          'class_item' => ClassItem,
                          'path' => PathParts},
                case maps:get(<<"storage_mode">>,ClassItem) of
                    StT when StT==<<"history">>; StT==<<"transactionlog">> ->
                        case PathParts of
                            [PathDt,Id] ->
                                {ok, Dinfo#{'resource_type' => 'entity',
                                            'id' => Id,
                                            'prepath' => PathDt}};
                            [PathDt,Id|RestParts] ->
                                Dinfo1 = Dinfo#{'id' => Id,
                                                'prepath' => PathDt},
                                walk_path(Dinfo1,ClassItem,CN,RestParts,Req,State)
                        end;
                    _ ->
                        [Id|RestParts]=PathParts,
                        Dinfo1 = Dinfo#{'id' => Id},
                        walk_path(Dinfo1,ClassItem,CN,RestParts,Req,State)
                end;
            {error,_}=Err -> Err
        end
    catch
        throw:ErrC -> ErrC;
        E:R:ST ->
            ?LOG('$crash',"Build destination info crashed: {~120tp,~120tp}~n\tStack: ~120tp",[E,R,ST]),
            {error,{internal_error,<<"Operation crashed">>}}
    end.

%% @private
%% fill State by meta information: {classname, path, resource_type, property, subid, filesystem_filepath, filesystem_dirpath}
walk_path(Dinfo,ClassItem,_CN,RestParts,Req,State) ->
    Domain = maps:get('domain',Dinfo),
    case RestParts of
        [Property] ->
            case ?MU:get_property_type(Property,ClassItem) of
                false -> {error,404,{not_found,?BU:strbin("Property not found: '~ts'",[Property])}};
                {ok,<<"attachment">>,false} ->
                    update_destination_info('$single',{Domain,ClassItem,Property},
                                            Dinfo#{'property' => Property},
                                            Req, State);
                {ok,<<"attachment">>,true} ->
                    update_destination_info('$multi',{Domain,ClassItem,Property},
                                            Dinfo#{'property' => Property},
                                            Req, State);
                {ok,<<"entity">>,false} ->
                    do_build_destination_info(Domain,get_new_path(Domain,Dinfo,RestParts,ClassItem,State),Req,State);
                {ok,<<"entity">>,true} ->
                    do_build_destination_info(Domain,get_new_path(Domain,Dinfo,RestParts,ClassItem,State),Req,State);
                {ok,_,_} ->
                    {error,404,{invalid_params,?BU:strbin("Invalid property '~ts'. Expected type of entity to walk inside",[Property])}}
            end;
        [Property,SubId] ->
            case ?MU:get_property_type(Property,ClassItem) of
                false -> {error,404,{not_found,?BU:strbin("Property not found: '~ts'",[Property])}};
                {ok,<<"attachment">>,false} -> {error,400,{not_found,<<"Property is not a collection">>}};
                {ok,<<"attachment">>,true} ->
                    update_destination_info('$file_in_folder',{Domain,ClassItem,Property},
                                            Dinfo#{'property' => Property,
                                                   'subid' => SubId},
                                            Req, State);
                {ok,<<"entity">>,false} ->
                    do_build_destination_info(Domain,get_new_path(Domain,Dinfo,RestParts,ClassItem,State),Req,State);
                {ok,<<"entity">>,true} ->
                    do_build_destination_info(Domain,get_new_path(Domain,Dinfo,RestParts,ClassItem,State),Req,State);
                {ok,_,_} ->
                    {error,400,{invalid_params,?BU:strbin("Invalid property '~ts'. Expected type of attachment or entity to walk inside",[Property])}}
            end;
        [Property,_,_|_] ->
            case ?MU:get_property_type(Property,ClassItem) of
                false -> {error,404,{not_found,?BU:strbin("Property not found: '~ts'",[Property])}};
                {ok,<<"entity">>,false} ->
                    do_build_destination_info(Domain,get_new_path(Domain,Dinfo,RestParts,ClassItem,State),Req,State);
                {ok,<<"entity">>,true} ->
                    do_build_destination_info(Domain,get_new_path(Domain,Dinfo,RestParts,ClassItem,State),Req,State);
                _ ->
                    {error,400,{invalid_params,?BU:strbin("Invalid property '~ts'. Expected type of entity to walk inside",[Property])}}
            end
    end.

%% ---------------------
%% @private
update_destination_info(FileMode,{Domain,ClassItem,Property},Dinfo0,Req,State)
  when FileMode=='$single'; FileMode=='$multi'; FileMode=='$file_in_folder' ->
    Method = ?RESTU:method(Req, State),
    Id = maps:get('id',Dinfo0),
    TempDir = ?DMS_ATTACH:get_temp_path(Domain,ClassItem,Id,Property),
    Dinfo = Dinfo0#{'file_mode' => FileMode,
                    'filesystem_dirpath' => ?BU:to_binary(TempDir)},
    update_destination_info(FileMode,{Domain,ClassItem,Property},{Method,Id,TempDir},Dinfo,Req,State).

%% @private
update_destination_info('$single',{Domain,ClassItem,Property},{Method,Id,TempDir},Dinfo,_Req,State) ->
    case ?MU:read_entity(Dinfo,State) of
        {ok,Entity} ->
            FileInfo = maps:get(Property,Entity,null),
            FileName = case FileInfo of
                           'null' -> <<>>;
                           FileInfo when is_map(FileInfo) -> maps:get(<<"name">>,FileInfo,<<>>)
                       end,
            TempFile = ?DMS_ATTACH:get_temp_path(Domain,ClassItem,maps:get('id',Dinfo),Property,?SingleFileName),
            Dinfo1 = Dinfo#{'resource_type' => 'file',
                            'entity' => Entity,
                            'filename' => FileName},
            case Method of
                M when (M==<<"GET">> orelse M==<<"HEAD">> orelse M==<<"OPTIONS">>) andalso FileName==<<>> ->
                    {error,404,{not_found,<<"File not found (G)">>}};
                M when M==<<"GET">>;M==<<"HEAD">>;M==<<"OPTIONS">> ->
                    Res = ?DMS_ATTACH:download(Domain,ClassItem,Id,Property,?SingleFileName,FileInfo),
                    case Res of
                        {ok,FilePath} -> {ok,Dinfo1#{'filesystem_filepath' => FilePath}};
                        _ -> {ok,Dinfo1#{'filesystem_filepath' => ?BU:to_binary(TempFile)}}
                    end;
                M when M==<<"POST">>; M==<<"PUT">> ->
                    TempDir2 = ?BU:to_binary(filename:join([TempDir,?BU:str("~p-~5..0B",[?BU:timestamp(),?BU:random(100000)])])),
                    {ok,Dinfo1#{'filesystem_filepath' => ?BU:to_binary(filename:join([TempDir2,?BU:to_list(?SingleFileName)])),
                                'filesystem_dirpath' => ?BU:to_binary(TempDir2)
                                }};
                M when M==<<"DELETE">>, FileName==<<>> ->
                    {error,404,{not_found,<<"File not found (D)">>}};
                M when M==<<"DELETE">> ->
                    ?BLfilelib:ensure_dir(TempFile),
                    ?BLfile:write_file(TempFile,<<>>,[append,binary,raw]),
                    {ok,Dinfo1#{'filesystem_filepath' => ?BU:to_binary(TempFile)}};
                M when M==<<"OPTIONS">> ->
                    {ok,Dinfo1#{'filesystem_filepath' => ?BU:to_binary(TempFile)}};
                M ->
                    {error,405,{method_not_allowed,?BU:strbin("Unable to '~ts' attachment property",[M])}}
            end;
        {error,_}=Err -> Err
    end;
%%
update_destination_info('$multi',{_Domain,_ClassItem,_Property},{Method,_Id,TempDir},Dinfo,_Req,_State) ->
    case Method of
        M when M==<<"GET">>;M==<<"HEAD">>;M==<<"OPTIONS">> ->
            {ok,Dinfo#{'resource_type' => 'folder'}};
        M when M==<<"POST">>; M==<<"CLEAR">> ->
            TempDir2 = ?BU:to_binary(filename:join([TempDir,?BU:str("~p-~5..0B",[?BU:timestamp(),?BU:random(100000)])])),
            {ok,Dinfo#{'resource_type' => 'folder',
                       'filesystem_dirpath' => ?BU:to_binary(TempDir2)}};
        M when M==<<"DELETE">> -> % would fail with 405 later
            %{ok,Dinfo#{'resource_type' => 'folder'}}
            {error,405,{method_not_allowed,<<"Unable to delete collection property">>}};
        M when M==<<"OPTIONS">> ->
            {ok,Dinfo};
        M ->
            {error,405,{method_not_allowed,?BU:strbin("Unable to '~ts' attachment collection property",[M])}}
    end;
%%
update_destination_info('$file_in_folder',{Domain,ClassItem,Property},{Method,Id,TempDir},Dinfo,_Req,State) ->
    SubId = maps:get('subid',Dinfo),
    TempFile = ?DMS_ATTACH:get_temp_path(Domain,ClassItem,maps:get('id',Dinfo),Property,SubId),
    Dinfo1 = Dinfo#{'resource_type' => 'file',
                    'filename' => SubId},
    case Method of
        M when M==<<"GET">>;M==<<"HEAD">>;M==<<"OPTIONS">> ->
            case ?MU:read_entity(Dinfo1,State) of
                {ok,Entity} ->
                    CheckFileInfo = case lists:filter(fun(FInfo) -> maps:get(<<"name">>,FInfo)==SubId end, maps:get(Property,Entity)) of
                                        [] -> #{};
                                        [FileInfo] -> FileInfo
                                    end,
                    Dinfo2 = Dinfo1#{'entity' => Entity},
                    case ?DMS_ATTACH:download(Domain,ClassItem,Id,Property,SubId,CheckFileInfo) of
                        {ok,FilePath} -> {ok,Dinfo2#{'filesystem_filepath' => FilePath}};
                        _ -> {ok,Dinfo2#{'filesystem_filepath' => ?BU:to_binary(TempFile)}}
                    end;
                _ -> {error,404,{not_found,<<"Entity not found">>}}
            end;
        M when M==<<"POST">>; M==<<"PUT">> ->
            TempDir2 = ?BU:to_binary(filename:join([TempDir,?BU:str("~p-~5..0B",[?BU:timestamp(),?BU:random(100000)])])),
            {ok,Dinfo1#{'filesystem_filepath' => ?BU:to_binary(filename:join([TempDir2,SubId])),
                        'filesystem_dirpath' => ?BU:to_binary(TempDir2)}};
        M when M==<<"DELETE">> -> % would fail with 405 later
            ?BLfilelib:ensure_dir(TempFile),
            ?BLfile:write_file(TempFile,<<>>,[append,binary,raw]),
            {ok,Dinfo1#{'filesystem_filepath' => ?BU:to_binary(TempFile)}};
        M when M==<<"OPTIONS">> ->
            {ok,Dinfo1#{'filesystem_filepath' => ?BU:to_binary(TempFile)}};
        M ->
            {error,405,{method_not_allowed,?BU:strbin("Unable to '~ts' attachment property",[M])}}
    end.

%% ---------------------
%% @private
get_new_path(Domain,Dinfo,PathParts,ClassItem,State) ->
    [Property|RestParts]=PathParts,
    {ok,Prop} = ?MU:find_property(Property,ClassItem),
    % read child class
    Map0 = #{'object' => {'entity',maps:get(<<"idclass">>,Prop),[]},
             'qs' => [],
             'initiator' => {'user',maps:get('user_id',maps:get('iam',State))}},
    case catch ?DMS_CRUD:crud({Domain,'read',#{},?ClassesCN,Map0,[]}, fun(Reply) -> Reply end) of
        {'EXIT',Reason} -> throw({error,Reason});
        {error,_}=Err -> throw(Err);
        {ok,ChildClassItem} ->
            CN = maps:get(<<"classname">>,ChildClassItem),
            CNParts = binary:split(CN,<<"/">>,[global,trim_all]),
            % read current class entity
            Map1 = Map0#{'object' => {'entity',maps:get(id,Dinfo),[]}},
            case catch ?DMS_CRUD:crud({Domain,'read',ClassItem,maps:get(<<"classname">>,ClassItem),Map1,[]}, fun(Reply) -> Reply end) of
                {'EXIT',Reason} -> throw({error,Reason});
                {error,_}=Err -> throw(Err);
                {ok,Entity} ->
                    PropValue = maps:get(Property,Entity,null),
                    case maps:get(<<"multi">>,Prop,false) of
                        _ when PropValue==null ->
                            throw({error,404,{not_found,?BU:strbin("Wrong path. Property '~ts' is null",[Property])}});
                        false ->
                            ChildId = PropValue,
                            CNParts ++ [ChildId] ++ RestParts;
                        true when RestParts==[] ->
                            throw({error,400,{invalid_params,?BU:strbin("Wrong path. Expected id to walk inside multi-object property",[])}});
                        true ->
                            [ChildId|RestParts2]=RestParts,
                            CNParts ++ [ChildId] ++ RestParts2
                    end end end.
