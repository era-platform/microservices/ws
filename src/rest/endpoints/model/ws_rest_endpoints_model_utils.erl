%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.06.2021
%%% @doc

-module(ws_rest_endpoints_model_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_path/1,
         get_domain/2]).

-export([parse_path/2]).

-export([find_property/2,
         get_property_type/2,
         find_attachment_property/3]).

-export([read_entity/2,
         build_crud_map/2]).

-export([build_opts_for_attachment_complex/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("rest.hrl").
-include("ws_rest_endpoints_model.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ----------------------------------------------
%% Return path of current request
%% ----------------------------------------------
get_path(Req) ->
    ?BU:urldecode(cowboy_req:path(Req)).

%% ----------------------------------------------
%% Return domain of current request
%% ----------------------------------------------
get_domain(Req,State) ->
    case ?RESTU:check_req_get_domain(Req, State) of
        {ok,Domain,_} -> Domain;
        _ -> undefined
    end.

%% ----------------------------------------------
%% Parse path and search existing classes.
%% Priority to greedy hunt of longer class names.
%% ----------------------------------------------
-spec parse_path(Domain::binary(),Path::binary()) ->
    {ok,Type::collection|entity|path,ClassItem::map(),ClassName::binary(),Path::[Part::binary()]} |
    {error,{Code::atom() | non_neg_integer(), Reason::binary()}}.
%% ----------------------------------------------
parse_path(Domain, Path) ->
    Key = {datamodel,parse_path,Domain,Path},
    Fun = fun() -> do_parse_path(Domain,Path) end,
    % do now use transaction, simply count several times is better than blocking store process. The only one process works better using store.
    ?BLstore:lazy_t(Key,Fun,{10000,6000,1000},{false,false}).

%% @private
do_parse_path(Domain, <<"/rest/v1/",Rest/binary>>) ->
    do_parse_path(Domain,Rest);
do_parse_path(?NoAuthDomain, ?SessionsCN) ->
    {ok,'collection',?FixturerClasses:sessions(?NoAuthDomain),?SessionsCN,[]};
do_parse_path(?NoAuthDomain, _) ->
    {error,{401,<<"Unauthorized">>}};
do_parse_path(Domain, Path) when is_binary(Path) ->
    CN1 = string:trim(Path,trailing,[$/]),
    do_parse_path(Domain,binary:split(CN1,<<"/">>,[global,trim_all]));
do_parse_path(_Domain,[]) ->
    {error,{invalid_request,<<"Invalid rest path">>}};
do_parse_path(Domain,[_First|_]=PathParts) when is_binary(_First) ->
    case ?DMS_CACHE:read_cache(Domain,?ClassesCN,#{},auto) of
        {error,noproc} -> {error,{internal_error,<<"DMS not found">>}};
        {error,timeout} -> {error,{internal_error,<<"DMS is not responding">>}};
        {error,_} -> {error,{internal_error,<<"DMS is not responding">>}};
        {ok,Classes,_} ->
            case find_class(PathParts,Classes) of
                false -> {error,{not_found,<<"Path not exists">>}};
                {ok,ClassItem,CN,[]} -> {ok,'collection',ClassItem,CN,[]};
                {ok,ClassItem,CN,[_]=Path} -> {ok,'entity',ClassItem,CN,Path};
                {ok,ClassItem,CN,[_|_]=Path} -> {ok,'path',ClassItem,CN,Path}
            end end.

%% @private
find_class(Parts,Classes) ->
    Candidates = build_candidates(Parts),
    Tuples = lists:sort(fun({ACN,_},{BCN,_}) -> size(ACN) > size(BCN) end,
                        lists:map(fun(ClassItem) -> {maps:get(<<"classname">>,ClassItem),ClassItem} end, Classes)),
    lists:foldl(fun({CN,ClassItem}, false) ->
                        case lists:keyfind(CN,1,Candidates) of
                            {_,RestParts} -> {ok,ClassItem,CN,RestParts};
                            false -> false
                        end;
                   (_, Acc) -> Acc
                end, false, Tuples).

%% @private
build_candidates(Parts) ->
    Res = lists:foldl(fun(_,{SubPaths, AccSubPath, [NextPart|RestParts]}) ->
                             SubPath = case AccSubPath of <<>> -> NextPart; _ -> <<AccSubPath/binary,"/", NextPart/binary>> end,
                             {[{SubPath,RestParts}|SubPaths], SubPath, RestParts}
                      end, {[],<<>>,Parts}, lists:seq(1,length(Parts))),
    element(1,Res).

%% ----------------------------------------------
%% Find and return class' property by name
%% ----------------------------------------------
find_property(PropName,Item) when is_map(Item) ->
    Props = maps:get(<<"properties">>,Item),
    lists:foldl(fun(Prop,false) ->
                        case maps:get(<<"name">>,Prop) of
                            PropName -> {ok,Prop};
                            _ -> false
                        end;
                   (_,Acc) -> Acc
                end, false, Props).

%% ----------------------------------------------
%% Find and return class' property by name, checking if it is attachment and multi-flag
%% ----------------------------------------------
get_property_type(PropName,Item) when is_binary(PropName), is_map(Item) ->
    case find_property(PropName,Item) of
        {ok,Prop} ->
            DataType = maps:get(<<"data_type">>,Prop),
            Multi = ?BU:to_bool(maps:get(<<"multi">>,Prop,false)),
            {ok,DataType,Multi};
        false -> false
    end.

%% ----------------------------------------------
%% Find and return class' property by name, checking if it is attachment and multi-flag
%% ----------------------------------------------
find_attachment_property(PropName,Multi,Item) when is_binary(PropName), is_boolean(Multi), is_map(Item) ->
    case find_property(PropName,Item) of
        {ok,Prop} ->
            case maps:get(<<"data_type">>,Prop) of
                <<"attachment">> ->
                    case ?BU:to_bool(maps:get(<<"multi">>,Prop,false)) of
                        Multi -> {ok,Prop};
                        _ -> false
                    end;
                _ -> false
            end;
        false -> false
    end.

%% -----------------------------------
%% Return entity from dms (or other appserver)
%% -----------------------------------
read_entity(DI,State) ->
    Domain = maps:get('domain',DI),
    ClassItem = maps:get('class_item',DI),
    CN = maps:get('classname',DI),
    FunReply = fun(Reply) -> Reply end,
    Map0 = build_crud_map(DI,State),
    case catch ?DMS_CRUD:crud({Domain,'read',ClassItem,CN,Map0,[]}, FunReply) of
        {'EXIT',Reason} -> {error,Reason};
        {error,_}=Err -> Err;
        {ok,Entity} -> {ok,Entity}
    end.

%% -----------------------------------
%% Return map options for crud operation
%% -----------------------------------
build_crud_map(DI,State) ->
    Id = maps:get('id',DI),
    Object = case maps:get('prepath',DI,undefined) of
                 undefined -> {'entity',Id,[]};
                 Prepath -> {'entity',Id,[{path,Prepath}]}
             end,
    #{'object' => Object,
      'qs' => [],
      'initiator' => {'user',maps:get('user_id',maps:get('iam',State))}}.

%% -----------------------------------
%% Return map options for attachments complex operation
%% -----------------------------------
build_opts_for_attachment_complex(DI,State) ->
    Opts0 = #{'initiator' => {'user',maps:get('user_id',maps:get('iam',State))}},
    Opts1 = case maps:get('subid',DI,undefined) of
                undefined -> Opts0;
                SubId -> Opts0#{'filename' => SubId}
            end,
    Opts2 = case maps:get('prepath',DI,undefined) of
                undefined -> Opts1;
                Prepath -> Opts1#{'prepath' => Prepath}
            end,
    Opts2.

%% ====================================================================
%% Internal functions
%% ====================================================================