%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 26.06.2021
%%% @doc

-module(ws_rest_endpoints_model_sessions).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([update_map/2,
         get_reply_fun/5]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ----------------------------------------
%% Update crud request map by inserting there 'session_id' and changing 'current' on 'session_id'
%% ----------------------------------------
update_map(Map0,State) ->
    case get_current_session(State) of
        false -> Map0;
        {ok,SessionId} ->
            case maps:get('object',Map0) of
                {'entity',<<"current">>,[]} ->
                    Map0#{'session_id' => SessionId,
                          'object' => {'entity',SessionId,[]}};
                _ -> Map0#{'session_id' => SessionId}
            end end.

%% ----------------------------------------
%% Return custom FunReply, that putting cookies inside
%% ----------------------------------------
get_reply_fun({Domain,Operation,Map},DefaultReplyFun,Domain,Req,State) ->
    CurrSessionId =  maps:get('session_id',Map,undefined),
    Object = maps:get('object',Map),
    fun(Reply) ->
        Req2 = case {Operation,Object,Reply} of
                   {'create', 'collection', {ok,Item}} ->
                       NewId = maps:get(<<"id">>,Item),
                       NewDomain = maps:get(<<"domain">>,Item),
                       NewTTL = maps:get(<<"expires">>,Item),
                       _Req1 = ?WsUtils:set_cookie(?CookieSession,?MakeToken(NewId,NewDomain),NewTTL,Req);
                   {'delete', {'entity',Id,[]}, ok} when Id==<<"current">>; Id==CurrSessionId ->
                       _Req1 = ?WsUtils:erase_cookie(?CookieSession,Req);
                   _ -> Req
               end,
        DefaultReplyFun(Reply,Domain,Req2,State)
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
get_current_session(State) ->
    case maps:get(iam,State,undefined) of
        undefined -> false;
        Iam when is_map(Iam) ->
            case maps:get('session_id',Iam,undefined) of
                undefined -> false;
                SessionId -> {ok,SessionId}
            end end.