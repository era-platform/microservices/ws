%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.06.2021
%%% @doc

-module(ws_rest_utils_http).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---------------------------------------
%% Extract parameters from request (GET request line or POST content in x-www-form-urlencoded)
%% ---------------------------------------

extract_method_qsvals(Req) ->
    extract_method_qsvals(Req, []).

extract_method_qsvals(Req, Opts) ->
    Method = cowboy_req:method(Req),
    try
        {QsVals,Req1} = case Method of
                            <<"POST">> -> parse_post_params(Req, Opts);
                            _ -> {cowboy_req:parse_qs(Req),Req}
                        end,
        {Method,QsVals,Req1}
    catch throw:Res -> Res
    end.

%% @private #460
parse_post_params(Req, Opts) ->
    AppJson = lists:member(application_json, Opts), % #460
    {QsVals,Req1} =
        case cowboy_req:parse_header(<<"content-type">>, Req) of
            {<<"application">>, CTS, _} when CTS==<<"x-www-form-urlencoded">> orelse (CTS==<<"json">> andalso AppJson) ->
                case cowboy_req:read_body(Req,[{length,524288},{read_length,64000},{read_timeout,5000}]) of
                    {ok,Body,R2} ->
                        case CTS of
                            <<"x-www-form-urlencoded">> ->
                                {[{body,Body} | cow_qs:parse_qs(Body)], R2};
                            <<"json">> ->
                                try case jsx:decode(Body) of
                                        [{}] -> {[], R2};
                                        [_|_]=JD -> {[{body,Body} | lists:map(fun ({K,V}) -> {K,reencode_param(V)} end, JD)], R2};
                                        _JO -> throw({{error,{invalid_request,<<"JSON object expected">>}},R2})
                                    end
                                catch error:badarg ->
                                    throw({{error,{invalid_request,<<"Invalid json in body">>}},R2})
                                end
                        end;
                    {more,_,R2} -> throw({{error,{invalid_request,<<"Too large body. Expected max 524288 bytes">>}},R2})
                end;
            _ -> {[],Req}
        end,
    QsVals1 = lists:ukeymerge(1, lists:keysort(1,QsVals), lists:keysort(1,cowboy_req:parse_qs(Req1))),
    {QsVals1,Req1}.

%% @private
reencode_param(Str) when is_binary(Str) -> Str; % if param is simple string, do not reencode it
reencode_param(P) -> jsx:encode(P). % the rest param types (list and object) are reencoded

%% returns header from request or undefined
extract_header(Name, Req) ->
    cowboy_req:header(Name, Req, undefined).

%% returns cookie from request or undefined
extract_cookie(Name, Req) ->
    case extract_header(<<"cookie">>, Req) of
        undefined -> undefined;
        Cookies ->
            Cookies1 = lists:map(fun(X) -> binary:replace(X, <<" ">>, <<>>, [global]) end,
                                 binary:split(Cookies, [<<",">>,<<";">>], [global, trim_all])),
            lists:foldl(fun(T, undefined) ->
                                case binary:split(T, <<"=">>, [global, trim_all]) of
                                    [Name,Value] -> Value;
                                    _ -> undefined
                                end;
                           (_, Acc) -> Acc
                        end, undefined, Cookies1)
    end.

%% ---------------------------------------
%% Modify json parameters from request, makes Opts by them.
%% ---------------------------------------
modify_json_params(Keys, Opts) when is_list(Keys), is_list(Opts) ->
    F = fun(_,{error,_}=Err) -> Err;
           (K,Acc) ->
                case lists:keyfind(K,1,Opts) of
                    false -> Acc;
                    {_,V} when byte_size(V) > 10485760 ->
                        {error, {invalid_params, <<"Too large json in ", K/bitstring, ". Must be less than 10MB">>}};
                    {_,V} ->
                        try jsx:decode(V) of V1 -> [{K,V1}|lists:keydelete(K,1,Acc)]
                        catch error:_ -> {error, {invalid_params, <<"Invalid json in '", K/bitstring, "'">>}}
                        end
                end end,
    case lists:foldl(F, [], Keys) of
        {error,_}=Err -> Err;
        Opts1 -> Opts1
    end.

%% ---------------------------------------
%% Make response
%% ---------------------------------------

%% access-control-allow-*
allow_origin(Req, _State) ->
    Host = cowboy_req:header(<<"host">>, Req, <<>>),
    Origin = cowboy_req:header(<<"origin">>, Req, Host),
    case lists:last(binary:split(Origin, <<"://">>, [])) of
        Host -> Req;
        _ ->
            Req1 = case cowboy_req:header(<<"access-control-request-method">>, Req, undefined) of
                       undefined -> Req;
                       M -> cowboy_req:set_resp_header(<<"access-control-allow-methods">>, M, Req)
                   end,
            Req2 = case cowboy_req:header(<<"access-control-request-headers">>, Req1, undefined) of
                       undefined -> Req1;
                       H-> cowboy_req:set_resp_header(<<"access-control-allow-headers">>, H, Req1)
                   end,
            Req3 = cowboy_req:set_resp_header(<<"access-control-allow-origin">>, Origin, Req2),
            _Req4 = cowboy_req:set_resp_header(<<"access-control-allow-credentials">>, <<"true">>, Req3)
    end.

%% %% @private access-control-allow-*
%% allow_headers(Req, _State) ->
%%     RMethod = cowboy_req:header(<<"access-control-request-method">>, Req, <<>>),
%%     RHeaders = cowboy_req:header(<<"access-control-request-headers">>, Req, <<>>),
%%     case {RMethod,RHeaders} of
%%         {<<>>,<<>>} -> Req;
%%         {_,_} ->
%%             Req1 = cowboy_req:set_resp_header(<<"access-control-allow-methods">>, RMethod, Req),
%%             Req2 = cowboy_req:set_resp_header(<<"access-control-allow-headers">>, RHeaders, Req1),
%%             _Req3 = cowboy_req:set_resp_header(<<"access-control-max-age">>, <<"86400">>, Req2)
%%
%%     end.

%%% -------------------------------

%% Handle async/sync svcscript start result and prepare for response/3.
prepare_custom_response({error,_}=Err, Req) -> {Err, Req};
prepare_custom_response(ok, Req) -> {ok, Req};
prepare_custom_response({ok,Res}, Req) when is_map(Res) ->
    [Code,Headers,Content] = ?BU:maps_get([code,headers,content],Res),
    HeadersPL = case is_map(Headers) of true -> maps:to_list(Headers); false -> Headers end,
    Req2 = lists:foldl(fun({_H,HV},Acc) when is_list(HV)-> Acc; % ignore what ? header values as string ? or header values as list of lines ?
                          ({H,HV},Acc) -> cowboy_req:set_resp_header(?BU:to_binary(H), ?BU:to_binary(HV), Acc);
                          (_,Acc) -> Acc
                       end, Req, HeadersPL),
    Req3 = cowboy_req:set_resp_body(Content, Req2),
    {{code,Code},Req3}.

%%% -------------------------------

ok(Req, Body) -> ok(Req, Body, 200).

ok(Req, BodyFun, RespCode) when is_function(BodyFun), is_integer(RespCode) ->
    ok(Req, BodyFun(), RespCode);
ok(Req, Body, RespCode) when is_binary(Body), is_integer(RespCode) ->
    Req0 = flush_req_body(Req),
    Req1 = cowboy_req:set_resp_header(<<"server">>, ?ServerName, Req0),
    Req2 = cowboy_req:set_resp_header(<<"content-type">>, <<"application/json; charset=utf-8">>, Req1),
    Req3 = set_nocache_headers(Req2),
    Req4 = cowboy_req:set_resp_body(Body, Req3),
    cowboy_req:reply(RespCode, Req4).

%%% -------------------------------

response(R, Req, State) ->
    Req1 = flush_req_body(Req), % #419
    try do_response(R, Req1, State)
    catch
        _C:_E:StackTrace ->
            ?LOG('$crash',"Catched ~120tp:~120tp", [_C, _E]),
            ?OUT('$crash',"Wrong response: ~120p~n\tStack: ~120p", [R,StackTrace]),
            do_response({error,{internal_error,<<"Prepare response error. See log">>}},Req1,State)
    end.

%% @private
do_response({file, Path}, Req, State) when is_list(Path); is_binary(Path) ->
    [BFileName|_] = lists:reverse(binary:split(?BU:to_unicode_binary(Path), <<"/">>, [global])),
    do_response({file, Path, BFileName}, Req, State);

do_response({file, Path, Filename}, Req, State) when (is_list(Path) orelse is_binary(Path)) andalso is_list(Filename) ->
    do_response({file, Path, ?BU:to_binary(Filename)}, Req, State);

do_response({file, Path, BFileName0}, Req, State) when (is_list(Path) orelse is_binary(Path)) andalso is_binary(BFileName0) ->
    BFileName = ?BU:urlencode(BFileName0),
    Req0 = allow_origin(Req, State),
    Req1 = cowboy_req:set_resp_header(<<"server">>, ?ServerName, Req0), % Rostell
    Req2 = cowboy_req:set_resp_header(<<"content-type">>, <<"application/octet-stream">>, Req1),
    Req3 = cowboy_req:set_resp_header(<<"content-disposition">>, <<"attachment; filename*=UTF-8''", BFileName/bitstring>>, Req2),
    Req4 = set_nocache_headers(Req3),
    FileSize = ?BLfilelib:file_size(Path),
    Req5 = cowboy_req:set_resp_body({sendfile, 0, FileSize, Path}, Req4),
    cowboy_req:reply(200, Req5),
    {ok, Req5, State};

do_response({html, Data}, Req, State) when is_binary(Data) ->
    Req1 = set_nocache_headers(Req),
    Req2 = cowboy_req:reply(200, [{<<"content-type">>, <<"text/html">>}], Data, Req1),
    {ok, Req2, State};

do_response(ok, Req, State) ->
    R = [{<<"resultcode">>,0},
         {<<"resultmsg">>,<<"OK">>}],
    RespBody = jsx:prettify(jsx:encode(R)),
    Req1 = allow_origin(Req, State),
    Req2 = ok(Req1, RespBody),
    {ok, Req2, State};

do_response({ok, Data}, Req, State) ->
    R = [{<<"resultcode">>,0},
         {<<"resultmsg">>,<<"OK">>},
         {<<"data">>,Data}],
    RespBody = jsx:prettify(jsx:encode(R)),
    Req1 = allow_origin(Req, State),
    Req2 = ok(Req1, RespBody),
    {ok, Req2, State};

%% ----------------
%% handler module that uses 'static' to return response must have content_types_provided/2 and get_file/2 exported
%% ----------------
do_response({static, Path}, Req, _State) ->
    StaticOpts = {file, Path, [{mimetypes, cow_mimetypes, all}]},
    ?COWBU:cowboy_static(Req, StaticOpts);

do_response({error,Err}, Req, State) ->
    {Req1, RespBody} = do_response_error_prepare_results(Err, Req, State),
    Req2 = ok(Req1, RespBody),
    {ok, Req2, State};

do_response({error,RespCode,Err}, Req, State) ->
    {Req1, RespBody} = do_response_error_prepare_results(Err, Req, State),
    Req2 = ok(Req1, RespBody, RespCode),
    {ok, Req2, State};

do_response(webhook, Req, State) ->
    Req1 = set_nocache_headers(Req),
    Req2 = cowboy_req:reply(200, Req1),
    {ok, Req2, State};

do_response({code,RespCode}, Req, State) ->
    Req1 = set_nocache_headers(Req),
    Req2 = cowboy_req:reply(RespCode, Req1),
    {ok, Req2, State};

do_response(loop, Req, State) ->
    {cowboy_loop, Req, State, hibernate};

do_response({end_loop, Result}, Req, State) ->
    {ok, Req2, State2} = response(Result, Req, State),
    {stop, Req2, State2};

do_response({'EXIT',{error,_}=Err}, Req, State) ->
    do_response(Err, Req, State).

%% ---------------------------------------
% @private #419
flush_req_body(Req) ->
    FunReadBody = fun F(ReqX) ->
                        case cowboy_req:read_body(ReqX,[{length,1000000}]) of
                            {ok,_,ReqX1} -> ReqX1;
                            {more,_,ReqX1} -> F(ReqX1)
                        end end,
    case cowboy_req:has_body(Req) of
        true -> FunReadBody(Req);
        _ -> Req
    end.

%% ---------------------------------------
%% @private
set_nocache_headers(Req) ->
    NoCacheHeaders = [{<<"cache-control">>,<<"no-cache, no-store, must-revalidate">>},
        {<<"pragma">>,<<"no-cache">>},
        {<<"expires">>,<<"0">>}],
    F = fun({HeaderName,HeaderVal},ReqX) ->
                case cowboy_req:has_resp_header(HeaderName,Req) of
                    true -> ReqX;
                    false -> cowboy_req:set_resp_header(HeaderName,HeaderVal,ReqX)
                end end,
    lists:foldl(F,Req,NoCacheHeaders).

%% ---------------------------------------
%% return filter fun by properties on monitor (and some other) requests
%% ---------------------------------------

filter_fun(Props) ->
    [PSite,PSites,PServer,PServers] = ?BU:extract_optional_props([<<"site">>,<<"sites">>,<<"server">>,<<"servers">>], Props),
    FunSiteFilter = ?BU:get_by_key(site_filter_fun, Props, undefined),
    F = fun(List) ->
                case List of
                    undefined -> undefined;
                    _ when is_list(List) -> List;
                    _ -> binary:split(?BU:to_binary(List),<<",">>,[global,trim_all])
                end end,
    PSites1 = F(PSites),
    PServers1 = F(PServers),
    fun(_) when PSite==undefined, PSites==undefined, PServer==undefined, PServers==undefined, FunSiteFilter==undefined -> true;
       ({Site,Node,SrvIdx,Addr}) ->
            true
            andalso (FunSiteFilter==undefined orelse FunSiteFilter(Site)) % RP-410
            andalso (PSite==undefined orelse PSite==Site)
            andalso (PSites==undefined orelse lists:member(Site,PSites1))
            andalso (PServer==undefined
                     orelse PServer==?BU:to_binary(SrvIdx)
                     orelse PServer==Addr
                     orelse PServer==?BU:to_binary(Node)
                     orelse PServer==?BU:to_binary(lists:takewhile(fun($@)->false;(_)->true end, ?BU:to_list(Node))))
            andalso (PServers==undefined
                     orelse (is_list(PServers) andalso lists:member(SrvIdx,PServers))
                     orelse (is_list(PServers1) andalso lists:member(?BU:to_binary(SrvIdx),PServers1)))
    end.

%% ---------------------------------------
%% Parse error responses
%% ---------------------------------------

%%
parse_error(Code) when is_integer(Code) -> {Code, <<>>};
parse_error(Code) when is_atom(Code); is_binary(Code) -> {parse_error_code(?BU:to_binary(Code)), ?BU:to_binary(Code)};
parse_error({Code,Reason}) when is_integer(Code), is_binary(Reason) -> {Code, Reason};
parse_error({Code,Reason}) when is_integer(Code), is_list(Reason) -> {Code, ?BU:to_binary(Reason)};
parse_error({Code,Reason}) when is_atom(Code) orelse is_binary(Code) andalso is_binary(Reason) -> {parse_error_code(?BU:to_binary(Code)), Reason};
parse_error({Code,Reason}) when is_atom(Code) orelse is_binary(Code) andalso is_list(Reason) -> {parse_error_code(?BU:to_binary(Code)), ?BU:to_binary(Reason)};
parse_error(_) -> {1500, <<"Unknown domain result">>}.

%%
parse_error_code(<<"access_denied">>) -> 1401;
parse_error_code(<<"already_exists">>) -> 1402;
parse_error_code(<<"not_implemented">>) -> 1403;
parse_error_code(<<"not_found">>) -> 1404;
parse_error_code(<<"invalid_request">>) -> 1405;
parse_error_code(<<"invalid_operation">>) -> 1406;
parse_error_code(<<"unauthorized">>) -> 1407;
parse_error_code(<<"not_loaded">>) -> 1408;
parse_error_code(<<"operation_failed">>) -> 1409;
parse_error_code(<<"limited">>) -> 1410;
parse_error_code(<<"license_restriction">>) -> 1411;
parse_error_code(<<"config_restriction">>) -> 1412;
parse_error_code(<<"invalid_params">>) -> 1413;
parse_error_code(<<"internal_error">>) -> 1501;
parse_error_code(<<"exception">>) -> 1502;
parse_error_code(<<"throw">>) -> 1503;
parse_error_code(<<"exit">>) -> 1504;
parse_error_code(A) when is_atom(A) -> 1505;
parse_error_code(_) -> 1506.

%% message could contain extra fields inside text. ex. wrong field names in crud
parse_reason_msg(<<>>) -> {ok, <<>>, []};
parse_reason_msg(Reason) when is_binary(Reason) ->
    RE = <<"([A-Za-z0-9_-]+)=([^\\|]+)\\|">>,
    case re:run(Reason,RE,[global]) of
        nomatch -> {ok, Reason, []};
        {match,List} ->
            Opts = lists:foldl(fun([_,{KS,KL},{VS,VL}],Acc) ->
                                         [{binary_part(Reason,KS,KL), binary_part(Reason,VS,VL)} | Acc]
                               end, [], List),
            {ok, re:replace(Reason,RE,<<>>,[global,{return,binary}]), Opts}
    end.

%% ---------------------------------------
%% makes json tree struct: sites -> nodes -> data by [{{Site,Node},Data}]
% could be setup by opts
%% ---------------------------------------
build_site_node_data(R,Opts) ->
    [SiteKey,NodesKey,NodeKey,DataKey] = ?BU:extract_optional_default([{sitekey,<<"site">>},{nodeskey,<<"nodes">>},{nodekey,<<"node">>},{datakey,<<"data">>}],Opts),
    Hasht = lists:foldl(fun({{S,N},Data}, Acc) ->
                                NData = [{NodeKey,N},{DataKey,Data}],
                                case maps:find(S,Acc) of
                                    error -> maps:put(S,[NData],Acc);
                                    {_,SN} -> maps:put(S,ordsets:add_element(NData,SN),Acc)
                                end end, maps:new(), R),
    lists:sort(maps:fold(fun(S,SN,Acc) -> [[{SiteKey,S},{NodesKey,SN}] | Acc] end, [], Hasht)).

% makes json response tree of flatten elements.
% each element in list is [{N1,FieldsFun,ItemsKey},{N2,FieldsFun,ItemsKey},{N3,FieldsFun,ItemsKey},...]
%   when every tuple is appropriate level item, N - is key of item, FieldsFun/0 is function that makes basic properties, ItemsKey - is key of sub items (next level)
build_json_tree(R) when is_list(R) ->
    % test
    %[[{a0,fun()->[{name,a0},{fld1,a01},{fld2,a02}] end,<<"ax">>},{b0,fun() -> [{name,b0},{fld1,b01},{fld2,b02}] end,<<"bx">>}],
    % [{a0,fun()->[{name,a0},{fld1,a01},{fld2,a02}] end,<<"ax">>},{b1,fun() -> [{name,b1},{fld1,b11},{fld2,b12}] end,<<"by">>}],
    % [{a1,fun()->[{name,a1},{fld1,a11},{fld2,a12}] end,<<"ay">>},{b0,fun() -> [{name,b0},{fld1,b01},{fld2,b02}] end,<<"bz">>}],
    % [{a1,fun()->[{name,a1},{fld1,a11},{fld2,a12}] end,<<"ay">>},{b2,fun() -> [{name,b2},{fld1,b21},{fld2,b22}] end}]]
    HashtX = lists:foldl(fun(L,Acc0) when is_list(L) ->
                                F = fun F([],Acc1) -> Acc1;
                                        F([{K,FF}],Acc1) ->
                                            maps:put(K,{FF()},Acc1);
                                        F([{K,FF,IK}|Rest],Acc1) ->
                                            {Flds0,Dict0} = case maps:find(K,Acc1) of
                                                                error -> {FF(),maps:new()};
                                                                {_,{Flds,_,Hasht}} -> {Flds,Hasht}
                                                            end,
                                            maps:put(K,{Flds0,IK,F(Rest,Dict0)},Acc1)
                                    end,
                                F(L,Acc0)
                         end, maps:new(), R),
    FX = fun FX(_K,{Flds},Acc) -> [Flds | Acc];
             FX(_K,{Flds,IK,SubHasht},Acc) -> [Flds ++ [{IK,maps:fold(FX, [], SubHasht)}] | Acc] end,
    Res = maps:fold(FX, [], HashtX),
    lists:sort(Res).

%% ====================================================================
%% Internal functions
%% ====================================================================

do_response_error_prepare_results(Err, Req, State) ->
    {Code,Reason} = parse_error(Err),
    {ok,Msg,ExOpts} = parse_reason_msg(Reason),
    R = [{<<"resultcode">>,Code},
         {<<"resultmsg">>,Msg}
         |ExOpts],
    RespBody = jsx:prettify(jsx:encode(R)),
    Req1 = allow_origin(Req, State),
    {Req1, RespBody}.
