%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>, Peter Bukashin <tbotc@yandex.ru>

-module(ws_rest_utils_multipart).

-export([multipart/1, multipart/2, multipart/3, multipart/5]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(MAX_FILE_SIZE_LIMIT, 536870912). % 512Mb
% -define(MAX_FILES,unlimited).

%% ====================================================================
%% Public functions
%% ====================================================================
%% multipart()
%%   returns tuple {ok, Request, Filelist :: [{file, Filename, ok} | {file, Filename, {error, Reason}}]}
%% ====================================================================

multipart(Req) ->
    multipart(Req, false).
multipart(Req, C) ->
    multipart(get_temp_path(), Req, C, ?MAX_FILE_SIZE_LIMIT,[]).
multipart(Path, Req, C) ->
    multipart(Path, Req, C, ?MAX_FILE_SIZE_LIMIT,[]).
multipart(Path, Req, C, MaxFileSizeLimit, FileNameList) ->
    case check_diskspace() of
        true -> do_multipart(Path, Req, C, MaxFileSizeLimit, FileNameList);
        false -> {error, {internal_error, <<"Not enough free disk space">>}}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
do_multipart(Path, Req, C, MaxFileSizeLimit, FileNameList) when is_integer(MaxFileSizeLimit) ->
    case cowboy_req:read_part(Req) of
        {ok, Headers, Req2} ->
            HandleEmptyFileNameFun =
            fun(FieldName) ->
                    ?LOG('$info', "Empty file name -> ignored, FieldName ~p", [FieldName]),
                    NewFileNameList = lists:append(FileNameList, [{file, FieldName, {error, <<"Empty file name!">>}}]),
                    {Req2, NewFileNameList}
            end,
            HandleFileFun =
            fun(FieldName, FileName) ->
                    case upload_file(Path, FileName, Req2, MaxFileSizeLimit) of
                        {ok, NewFileName, FileSize, Req4} ->
                            case C of
                                false -> ok;
                                _ when is_atom(C) ->
                                    ok = C:file_payload(FieldName, FileName, NewFileName, FileSize);
                                {_,CF} when is_function(CF) ->
                                    ok = CF(FieldName, FileName, NewFileName, FileSize);
                                {_,{CFM,CFF}} when is_atom(CFM), is_atom(CFF) ->
                                    ok = CFM:CFF(FieldName, FileName, NewFileName, FileSize);
                                {_,false} -> ok;
                                _ -> ?LOG('$warning', "Multipart file_payload callback argument is invalid: ~tp", [C])
                            end,
                            NewFileNameList = lists:append(FileNameList, [{file, NewFileName, ok}]),
                            {Req4, NewFileNameList};
                        {limit, NewFileName, Reason, Req4} ->
                            ?LOG('$warning', "Upload limit detected! Type: ~p; FieldName: ~p; Filename: ~p; Req: ~p", [Reason, FieldName, FileName, Req4]),
                            ok = ?BLfile:delete(NewFileName),
                            NewFileNameList = lists:append(FileNameList, [{file, NewFileName, {error, <<"File size limit reached!">>}}]),
                            {Req4, NewFileNameList};
                        {error, NewFileName, Reason} ->
                            ?LOG('$error', "Upload file op error! FileName: '~ts'; error reason: ~120tp", [NewFileName, Reason]),
                            NewFileNameList = lists:append(FileNameList, [{file, NewFileName, {error, <<"File uploading error!">>}}]),
                            {Req2, NewFileNameList}
                    end
            end,
            {Req5, NewFileNameList} =
            case cow_multipart:form_data(Headers) of
                {data, FieldName} ->
                    {ok, Body, Req3} = cowboy_req:read_part_body(Req2),
                    case C of
                        false -> ok;
                        _ when is_atom(C) ->
                            ok = C:data_payload(FieldName, Body);
                        {CD,_} when is_function(CD) ->
                            ok = CD(FieldName, Body);
                        {{CDM,CDF},_} when is_atom(CDM), is_atom(CDF) ->
                            ok = CDM:CDF(FieldName, Body);
                        {false,_} -> ok;
                        _ -> ?LOG('$warning',"Multipart data_payload callback argument is invalid: ~tp", [C])
                    end,
                    {Req3, FileNameList};
                {file, FieldName, <<>>, _CType} -> HandleEmptyFileNameFun(FieldName);
                {file, FieldName, FileName, _CType} -> HandleFileFun(FieldName, FileName);
                % support older cowlib versions that still passed Content-Transfer-Encoding header
                {file, FieldName, <<>>, _CType, _CTE} -> HandleEmptyFileNameFun(FieldName);
                {file, FieldName, FileName, _CType, _CTE} -> HandleFileFun(FieldName, FileName)
            end,
            multipart(Path, Req5, C, MaxFileSizeLimit, NewFileNameList);
        {done, Req2} ->
            {ok, Req2, FileNameList}
    end.

%% ------
%% @private
%%   returns tuple {ok | limit, NewFilePath, FileSize, Requerst} | {error, Reason}
upload_file(Path, FileName, Req, MaxFileSizeLimit) ->
    NewFileName = make_file_path(Path, FileName),
    try ?BLfile:open(NewFileName, [raw, write]) of
        {ok, IoDevice} ->
            Result = case stream_file(Req, IoDevice, 0, MaxFileSizeLimit) of
                {ok, FileSize, Req4} -> {ok, NewFileName, FileSize, Req4};
                {limit, Reason, Req4} -> {limit, NewFileName, Reason, Req4};
                {error, Reason} -> {error, FileName, Reason};
                Rsf -> Rsf
            end,
            ok = ?BLfile:close(IoDevice),
            Result;
        {error, IOReason} ->
            {error, NewFileName, IOReason}
    catch
        Err:Reason ->
            {Err, NewFileName, Reason}
    end.

%% ------
%%   returns tuple {limit, file_size, Requerst} | {ok, NewFileSize, Request}
stream_file(Req, IoDevice, FileSize, MaxFileSizeLimit) ->
    stream_file(Req, IoDevice, FileSize, MaxFileSizeLimit, 0). % #419

stream_file(Req, IoDevice, FileSize, MaxFileSizeLimit, Cnt) ->
    Opts = #{length => 0},
    {Control, Data, Req2} = cowboy_req:read_part_body(Req,Opts),
    NewFileSize = erlang:byte_size(Data) + FileSize,
    case NewFileSize > MaxFileSizeLimit of
        true -> {limit, file_size, Req2};
        false ->
            ok = ?BLfile:write(IoDevice, Data),
            case Control of
                ok -> {ok, NewFileSize, Req2};
                more ->
                    NewCnt = case NewFileSize==FileSize of true -> timer:sleep(20), Cnt+1; false -> 0 end, %% #419
                    case NewCnt > 250 of
                        true -> {error, <<"regularly_read_empty_body">>};
                        false -> stream_file(Req2, IoDevice, NewFileSize, MaxFileSizeLimit,NewCnt)
                    end
            end
    end.

%%% -------------------------------------------------------

%% @private
%% check if free disk space is larger than 3GB
check_diskspace() ->
    D = ?FILEPATHS:get_node_temp_path(),
    case ?BU:get_disk_space(D) of
        undefined -> true;
        {_,Cap,Perc} -> Cap - (Cap div 100 * Perc) > 5000000 % 5 GB
    end.

%% @private
get_temp_path() ->
    % @todo we must make unique folder for temporary files
    Path = ?FILEPATHS:get_node_temp_path(),
    ?BLfilelib:ensure_dir(Path),
    ?BLfile:make_dir(Path),
    Path.

%% @private
make_file_path(Path, FileName) ->
    ?BLfilelib:ensure_dir(Path),
    ?BLfile:make_dir(Path),
    unicode:characters_to_binary(filename:join(Path, unicode:characters_to_list(FileName))).
