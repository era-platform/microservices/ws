%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author George Makarov <georgemkrv@gmail.com>, Peter Bukashin <tbotc@yandex.ru>

-module(ws_rest_utils_file_router).

-export([get_file_list/4, check_file_existing/1, check_uploaded_files/1, delete_file/1, make_zip/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% -----------------------------------
%% getting file list from standard directories
%% -----------------------------------
-spec get_file_list(FPath :: string(), FMask :: string(), Ls_Opts :: boolean(), Subd :: boolean()) ->
    {error, {atom(), string()}} | {ok, term()}.
%% -----------------------------------
get_file_list(FPath, FMask, LS_Opts, Subd) ->
    ?FILESVC:get_file_list(FPath, FMask, LS_Opts, Subd).

%% -----------------------------------
%% checking file existence : used in download request
%% -----------------------------------
-spec check_file_existing(UPath :: string()) -> boolean().
%% -----------------------------------
check_file_existing(UPath) ->
    ?FILESVC:check_file_existing(UPath).

%% -----------------------------------
%% check uploaded files
%% -----------------------------------
-spec check_uploaded_files(FileList :: [string()]) -> [{FileName :: string(), Result :: boolean()}].
%% -----------------------------------
check_uploaded_files(FileNameList) ->
    ?FILESVC:check_uploaded_files(FileNameList).

%% -----------------------------------
%% delete file or empty directory
%% -----------------------------------
delete_file(UPath) ->
    ?FILESVC:delete_file(UPath).

%% -----------------------------------
make_zip(Mask, Path, ZipName) ->
    ?FILESVC:make_zip(Mask, Path, ZipName).

%% ====================================================================
%% Internal functions
%% ====================================================================


