%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 17.11.2016
%%% @doc

-module(ws_rest_utils_file_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    do_upload/2,
    do_uploadzip/3,
    ensure_dir/1,
    get_file_path_by_type/1, get_file_path_by_type/2
]).

-export([get_subpath_websocktemp/1]).

-export([
    get_file_path_statictemp/0,
    get_file_path_websocktemp/0, get_file_path_websocktemp/1,
    get_file_path_downloadziptemp/0,
    get_file_path_uploadziptemp/0
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ----------------------------------------
%% upload helper function
%% ----------------------------------------
do_upload(Path, Req) ->
    Result = case ?MPART:multipart(Path, Req, false) of
        {ok, ReqM, FileNameList} ->
            FResp = ?FILESVC:check_uploaded_files(FileNameList),
            Data = [{<<"upload">>, [[{<<"name">>,File},{<<"res">>,Res}] || {File,Res} <- FResp]}],
            {multipart, {ok,Data}, ReqM};
        {error,_}=Err -> Err
    end,
    Result.

%% ----------------------------------------
do_uploadzip(Temp, Path, Req) ->
    Result = case ?MPART:multipart(Temp, Req, false) of
        {ok, ReqM, FileNameList} ->
            FResp = unzip_files(Path, FileNameList, []),
             F = fun({file,FP,_}) -> ?BLfile:delete(FP);
                    (E) -> ?LOG('$error', "upload zip error -- undefined element ~p", [E])
                 end,
             lists:foreach(F, FileNameList),
            Data = [{<<"upload">>, [[{<<"name">>,File},{<<"res">>,Res}] || {File,Res} <- FResp]}],
            {multipart, {ok,Data}, ReqM};
        {error,_}=Err -> Err
    end,
    Result.

%% ----------------------------------------
%% checking for existing path
%% ----------------------------------------
ensure_dir(Path) ->
    ?BLfilelib:ensure_dir(Path),
    ?BLfile:make_dir(Path),
    Path.

%% ------------------------------
%% Paths to media folders
%% ------------------------------

%% --------
%% subpaths
%% --------

get_subpath_websocktemp(SessionId) ->
    filename:join(["websocktemp", SessionId]).

%% ----------------------------------------
%% getting full standard media path
%% ----------------------------------------
get_file_path_by_type(_Target) -> false.
get_file_path_by_type(_Target, _Domain) -> false.

get_file_path_statictemp() ->
    ensure_dir(filename:join([?FILEPATHS:get_node_temp_path(), "statictemp"])).

get_file_path_websocktemp() ->
    ensure_dir(filename:join([?FILEPATHS:get_node_temp_path(), "websocktemp"])).

get_file_path_websocktemp(WSSessId) ->
    ensure_dir(filename:join([?FILEPATHS:get_node_temp_path(), "websocktemp", WSSessId])).

get_file_path_downloadziptemp() ->
    ensure_dir(filename:join([?FILEPATHS:get_node_temp_path(), "downloadzip"])).

get_file_path_uploadziptemp() ->
    ensure_dir(filename:join([?FILEPATHS:get_node_temp_path(), "uploadzip"])).

%% -----------------------------------
%% deleting files from temporary folder
%% -----------------------------------
%% delete_upload_files([]) -> ok;
%% delete_upload_files([FileNameBin|Tail]) ->
%%     ?BLfile:delete(FileNameBin),
%%     delete_upload_files(Tail).

%% ----------------------------------------
-spec unzip_files(Path :: string() | binary(), FileNameList :: list(), Acc::list()) -> list().
%% ----------------------------------------
unzip_files(_Path, [], Acc) ->
    Acc;
unzip_files(Path, [{file, FilePath, ok}|Rest], Acc) ->
    FRes = case unzip_file(Path, FilePath) of
               {ok,_} -> {unicode:characters_to_binary(filename:basename(FilePath), utf8), <<"ok">>};
               {error, Reason} -> {unicode:characters_to_binary(filename:basename(FilePath), utf8),
                                   unicode:characters_to_binary(?BU:str("zip error: ~s", [Reason]), utf8)}
           end,
    unzip_files(Path, Rest, Acc ++ [FRes]);
unzip_files(Path, [{file, FilePath, {error, Reason}}|Rest], Acc) ->
    FRes = {unicode:characters_to_binary(filename:basename(FilePath), utf8),
            unicode:characters_to_binary(?BU:str("web error: ~s", [Reason]), utf8)},
    unzip_files(Path, Rest, Acc ++ [FRes]).

unzip_file(Path, Filename) ->
    Z = ?BU:to_unicode_list(Filename),
    HP = ?BU:to_unicode_list(Path),
    ?BLzip:unzip(Z, [{cwd, HP}]).

%% ====================================================================
