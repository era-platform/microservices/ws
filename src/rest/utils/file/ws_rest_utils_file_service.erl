%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author George Makarov <georgemkrv@gmail.com>, Peter Bukashin <tbotc@yandex.ru>
%%% @doc

-module(ws_rest_utils_file_service).

-export([
    get_file_list/4,
    check_file_existing/1,
    check_dir_exists/1,
    check_uploaded_files/1,
    delete_file/1,
    make_zip/3
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% -----------------------------------
%% getting file list from standard directories
%% -----------------------------------
-spec get_file_list(FPath :: string(), FMask :: string(), Ls_Opts :: boolean(), Subd :: boolean()) ->
    {ok, [{binary(),[{binary(),binary()}]}]}. % [{<<"files">>,..}]
%% -----------------------------------
get_file_list(FPath, FMask, LS_Opts, Subd) ->
    {ok, [collect_file_info(filename:join(FPath, filename:dirname(FMask)), {filename:basename(FMask), LS_Opts, Subd})]}.

%% -----------------------------------
%% checking file existence : used in download request
%% -----------------------------------
-spec check_file_existing(UPath :: string()) -> boolean().
%% -----------------------------------
check_file_existing(UPath) ->
    ?BLfilelib:is_regular(UPath).

%% -----------------------------------
%% checking file existence : used in download request
%% -----------------------------------
-spec check_dir_exists(UPath :: file:filename()) -> boolean().
%% -----------------------------------
check_dir_exists(UPath) ->
    ?BLfilelib:is_dir(UPath).

%% -----------------------------------
%% check uploaded files
%% -----------------------------------
-spec check_uploaded_files(FileList :: [{file | data, FileName :: string(), ok} | {file | data, FileName :: string(), error, Reason :: string()}]) ->
          [{FileName :: string(), Result :: boolean()}].
%% -----------------------------------
check_uploaded_files(FileNameList) ->
    check_uploaded_files_Acc(FileNameList, []).

%% -----------------------------------
%% delete file or empty directory
%% -----------------------------------
-spec delete_file(UPath :: string()) -> boolean().
%% -----------------------------------
delete_file(UPath) ->
    case ?BLfilelib:is_dir(UPath) of
        true ->
            ?BU:directory_delete_empties(UPath);
            %%?BLfile:del_dir(UPath);
        false ->
            case ?BLfilelib:is_regular(UPath) of
                true ->
                    ?BLfile:delete(UPath),
                    ?BU:directory_delete_empties(filename:dirname(UPath));
                false ->
                    {error, {not_found, <<"File not found (D4)">>}}
            end
    end.

%% -----------------------------------
-spec make_zip(Mask :: string() | binary(), Path :: binary() | string(), ZipName :: binary() | string()) ->
          ZipTempPath :: string() | false.
%% -----------------------------------
make_zip(Mask, Path, ZipName) ->
    TempPath = ?FILEU:get_file_path_downloadziptemp(),
    ZipTempFile = filename:join(TempPath, ZipName),
    case ?BLfilelib:wildcard(?BU:to_unicode_list(Mask), ?BU:to_unicode_list(Path)) of
        [] -> false;
        FileList ->
            case ?BLzip:create(ZipTempFile, FileList, [{cwd, Path}]) of
                {ok, FileName} -> FileName;
                {ok, FileName, _B} -> FileName;
                _Err -> false
            end
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================
%% ====================================================================
%% filelist short struct
%% ====================================================================
%% [{
%% "files":[
%%   {"file":{"name":"f1"}},
%%   {"file":{"name":"f2"}},
%%   {"file":{"name":"f3"}},
%%   {"dir":[{"name":"f4"}, {"files":[
%%     {"file":{"name":"f1"}},
%%     {"file":{"name":"f2"}},
%%     {"file":{"name":"f3"}}
%%    ]}]}
%%   ]
%% }]

%% data: {
%%     files: [
%%         {t: f, name: N1, size: S1},
%%         {t: f, name: N2, size: S2},
%%         {t: d, name: N3, files: []}
%%     ]
%% }

%% ====================================================================
%% filelist full struct
%% ====================================================================
%% [{
%% "files":[
%%   {"file":[{"name":"f1"}, {"size":32456}]},
%%   {"file":[{"name":"f2"}, {"size":1234}]},
%%   {"file":[{"name":"f3"},{"size":1234}]},
%%   {"dir":[{"name":"f4"}, {"files":[
%%     {"file":[{"name":"f1"}, {"size":32456}]},
%%     {"file":[{"name":"f2"}, {"size":1234}]},
%%     {"file":[{"name":"f3"},{"size":1234}]}
%%    ]}]}
%% ]
%% }]

%% ====================================================================
collect_file_info(FPath, {Mask, _LS, _Subd}=Opts) ->
    FileList = ?BLfilelib:wildcard(?BU:to_unicode_list(filename:join(FPath, Mask))),
    {<<"files">>, collect_file_info_ACC(FileList, Opts, [])}.

collect_file_info_ACC([], _Opts, Acc) ->
    Acc;
collect_file_info_ACC([FilePath|Rest], Opts, Acc) ->
    FileInfo = get_file_info(FilePath, Opts),
    collect_file_info_ACC(Rest, Opts, lists:append(Acc, [FileInfo])).

%% full info and subdirectories
get_file_info(FilePath, {_Mask, true, true}=Opts) ->
    case ?BLfilelib:is_dir(FilePath) of
        true -> [{<<"dir">>, [{<<"name">>, unicode:characters_to_binary(filename:basename(FilePath))}, collect_file_info(FilePath, Opts)]}];
        false -> [{<<"file">>,[{<<"name">>, unicode:characters_to_binary(filename:basename(FilePath))},
                               {<<"size">>, to_string(?BLfilelib:file_size(FilePath))},
                               {<<"last_modified">>, to_datetime(?BLfilelib:last_modified(FilePath))}]}]
    end;
get_file_info(FilePath, {_Mask, false, true}=Opts) ->
    case ?BLfilelib:is_dir(FilePath) of
        true -> [{<<"dir">>, [{<<"name">>, unicode:characters_to_binary(filename:basename(FilePath))}, collect_file_info(FilePath, Opts)]}];
        false -> [{<<"file">>,[{<<"name">>, unicode:characters_to_binary(filename:basename(FilePath))}]}]
    end;

get_file_info(FilePath, {_Mask, true, false}) ->
    case ?BLfilelib:is_dir(FilePath) of
        true -> [{<<"dir">>,[{<<"name">>, unicode:characters_to_binary(filename:basename(FilePath))},
                             {<<"last_modified">>, to_datetime(?BLfilelib:last_modified(FilePath))}]}];
        false -> [{<<"file">>,[{<<"name">>, unicode:characters_to_binary(filename:basename(FilePath))},
                               {<<"size">>, to_string(?BLfilelib:file_size(FilePath))},
                               {<<"last_modified">>, to_datetime(?BLfilelib:last_modified(FilePath))}]}]
    end;

get_file_info(FilePath, {_Mask, false, false}) ->
    case ?BLfilelib:is_dir(FilePath) of
        true -> [{<<"dir">>,[{<<"name">>, unicode:characters_to_binary(filename:basename(FilePath))}]}];
        false-> [{<<"file">>,[{<<"name">>, unicode:characters_to_binary(filename:basename(FilePath))}]}]
    end.

to_datetime({{Y,M,D},{H,Mi,S}}) ->
    ?BU:strbin("~4..0B-~2..0B-~2..0BT~2..0B:~2..0B:~2..0BZ", [Y,M,D,H,Mi,S]).

to_string(Ord) when is_integer(Ord) ->
    ?BU:strbin("~p", [Ord]).

check_uploaded_files_Acc([], Acc) -> lists:reverse(Acc);
check_uploaded_files_Acc([{file, FilePath, ok}|List], Acc) ->
    Res = {unicode:characters_to_binary(filename:basename(FilePath), utf8), <<"ok">>},
    check_uploaded_files_Acc(List, [Res|Acc]);
check_uploaded_files_Acc([{file, FilePath, {error, Reason}}|List], Acc) ->
    Res = {unicode:characters_to_binary(filename:basename(FilePath), utf8),
           unicode:characters_to_binary(?BU:str("error: ~s", [Reason]), utf8)},
    check_uploaded_files_Acc(List, [Res|Acc]).

%% ====================================================================
