%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.06.2021
%%% @doc Universal behavior (can at the same time handle files, folders and entities)

-module(ws_rest_behavior_universal).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behavior(cowboy_sub_protocol).

%% ====================================================================
%% REST generic behavior
%% ====================================================================

%% init/2 - is a required callback in line with cowboy_handler behavior.
%%          Typically returns {?H_XYZ, Req, Opts}. % to switch to that sub-protocol.
-callback init(Req, Opts) ->
    {ok | module(), Req, any()}
    | {module(), Req, any(), hibernate}
    | {module(), Req, any(), timeout()}
    | {module(), Req, any(), timeout(), hibernate}
    when Req::cowboy_req:req(), Opts::[{_,_}].

%% terminate/3 - is optional callback in line with cowboy_handler behavior.
%%               It probably does not make sense. Enable when there's a need for it.
%-callback terminate(any(), cowboy_req:req(), any()) -> ok.
%-optional_callbacks([terminate/3]).

%% Must return map filled by destination description:
%%  'resource_type' :: folder | file | entity | collection | undefined
%%  'file_mode' :: $single | $multi | $file_in_folder
%%  'filename' :: binary(),
%%  'filesystem_filepath' :: binary(),
%%  'filesystem_dirpath' :: binary(),
-callback destination_info(Req::cowboy_req:req(), State::map()) ->
    {ok, Info::map()}
    | {error, Reason}                  % yields http code from reason atom
    | {error, Code::integer(), Reason} % yields Code
    when Reason::{atom(),binary()}.

%% Generic behavior callbacks by method name

-callback get(Req, State) ->
    {binary()|term(), Req, State} % NB: content must be returned
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([get/2]).

-callback post(Req, State) ->
    {boolean(), Req, State} % NB: response body must be set using ?RESTU:send_response()
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([post/2]).

-callback patch(Req, State) ->
    {boolean(), Req, State} % NB: response body must be set using ?RESTU:send_response()
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([patch/2]).

-callback put(Req, State) ->
    {boolean(), Req, State} % NB: response body must be set using ?RESTU:send_response()
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([put/2]).

-callback delete(Req, State) ->
    {boolean(), Req, State} % NB: response body must be set using ?RESTU:send_response()
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([delete/2]).

%% Generic behavior callbacks for serving files.

-callback get_file(Req, State) ->
    {{stream, non_neg_integer(), fun()}, Req, State}
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([get_file/2]).

-callback head_file(Req, State) ->
    {{stream, non_neg_integer(), fun()}, Req, State}
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([head_file/2]).

%% The same set of callbacks as in cowboy_rest is also provided.
%% Only used/useful callbacks are defined.
%% When more is needed, please uncomment/implement them.

-callback allowed_methods(Req, State) ->
    {[binary()], Req, State}
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().

-callback valid_entity_length(Req, State) ->
    {boolean(), Req, State}
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([valid_entity_length/2]).

-callback content_types_accepted(Req, State) ->
    {[{binary() | {binary(), binary(), '*' | [{binary(), binary()}]}, atom()}], Req, State}
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([content_types_accepted/2]).

-callback content_types_provided(Req, State) ->
    {[{binary() | {binary(), binary(), '*' | [{binary(), binary()}]}, atom()}], Req, State}
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([content_types_provided/2]).

-callback charsets_provided(Req, State) ->
    {[binary()], Req, State}
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([charsets_provided/2]).

%%-callback resource_exists(Req, State) ->
%%    {boolean(), Req, State}
%%    | {stop, Req, State}
%%    when Req::cowboy_req:req(), State::map().
%%-optional_callbacks([resource_exists/2]).

-callback allow_missing_post(Req, State) ->
    {boolean(), Req, State}
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([allow_missing_post/2]).

-callback delete_resource(Req, State) ->
    {boolean(), Req, State}
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([delete_resource/2]).

%% ====================================================================
%% REST generic implementation
%% ====================================================================

%% cowboy_sub_protocol callbacks
-export([upgrade/4, upgrade/5]).

%% cowboy_rest callbacks
-export([
    known_methods/2,
    allowed_methods/2,
    is_authorized/2,
    forbidden/2,
    valid_entity_length/2,
    content_types_accepted/2,
    content_types_provided/2,
    charsets_provided/2,
    %resource_exists/2,
    last_modified/2,
    allow_missing_post/2,
    delete_resource/2
]).

%% callbacks returned by content_types_accepted()
-export([
    accept_json/2,
    accept_form/2,
    accept_multipart/2,
    accept_octet/2,
    accept_any/2
]).

%% callbacks returned by content_types_provided()
-export([
    provide_json/2, % in case ?H_BASE_BEHAVIOR:content_types_provided() is used
    provide_content/2
]).

-export([get_file/2]). % in case cowboy_static:content_types_provided() is used

-export([set_content_disposition/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("rest.hrl").
-include_lib("kernel/include/file.hrl").

%% ====================================================================
%% Interface functions
%% ====================================================================

%% -----------------------------
-spec upgrade(Req, Env, module(), any()) -> {ok, Req, Env}
    when Req::cowboy_req:req(), Env::cowboy_middleware:env().
%% -----------------------------
upgrade(Req0, Env, Handler, HandlerState0) ->
    ?H_BASE_BEHAVIOR:upgrade(?MODULE, Req0, Env, Handler, HandlerState0).

%% -----------------------------
%% cowboy_rest takes no options.
%% -----------------------------
-spec upgrade(Req, Env, module(), any(), any()) -> {ok, Req, Env}
    when Req::cowboy_req:req(), Env::cowboy_middleware:env().
%% -----------------------------
upgrade(Req, Env, Handler, HandlerState, _Opts) ->
    ?H_BASE_BEHAVIOR:upgrade(?MODULE, Req, Env, Handler, HandlerState).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ----------------------------------
known_methods(Req, State) ->
    ?H_BASE_BEHAVIOR:known_methods(Req, State).

%% ----------------------------------
allowed_methods(Req, State) ->
    ?H_BASE_BEHAVIOR:allowed_methods(Req, State). % utility, will use endpoint callback.

%% ----------------------------------
is_authorized(Req, State) ->
    ?H_BASE_BEHAVIOR:is_authorized(Req, State). % IAM is not to be customized.

%% ----------------------------------
forbidden(Req, State) ->
    case ?H_BASE_BEHAVIOR:forbidden(Req, State) of % use IAM service
        {true,_,_}=Forbidden -> Forbidden;
        {false,Req1,State1} -> after_forbidden_false(Req1, State1);
        {stop,_,_}=Stop -> Stop
    end.

%% @private
after_forbidden_false(Req, State) ->
    case ?RESTU:save_cb_val_or_fail(Req, State, 'destination_info') of
        {ok, Req1, State1} -> {false, Req1, State1}; % not forbidden
        {error, Req1, State1} -> {true, Req1, State1}; % 403 Forbidden - cannot build path to dir (e.g. session/domain is missing).
        {stop,_,_}=Stop -> Stop
    end.

%% ----------------------------------
valid_entity_length(Req, State) ->
    DestinationInfo = maps:get('destination_info',State,undefined),
    Default = case maps:get('resource_type',DestinationInfo) of
                  'folder' -> ?REST_MAX_CONTENT_LENGTH_FILE;
                  'file' -> ?REST_MAX_CONTENT_LENGTH_FILE;
                  _ -> ?REST_MAX_CONTENT_LENGTH_JSON
              end,
    ?H_BASE_BEHAVIOR:valid_entity_length(Req, State, Default). % will try callback, then default itself.

%% ----------------------------------
content_types_accepted(Req, State) ->
    F = fun(ReqF, StateF) ->
                DestinationInfo = maps:get('destination_info',StateF),
                Method = ?RESTU:method(ReqF, StateF),
                case maps:get('resource_type',DestinationInfo) of
                    'folder' -> ?H_BASE_BEHAVIOR:content_types_accepted(ReqF, StateF, [accept_multipart, accept_octet, accept_any]);
                    'file' when Method==<<"POST">> -> ?RESTU:send_error(405, ReqF, StateF); % it is forbidden to accept POST on a file endpoint.
                    'file' -> ?H_BASE_BEHAVIOR:content_types_accepted(ReqF, StateF, [accept_multipart, accept_octet, accept_any]);
                    _ -> ?H_BASE_BEHAVIOR:content_types_accepted(ReqF, StateF)
                end end,
    ?RESTU:call(Req, State, content_types_accepted, F).

%% ----------------------------------
content_types_provided(Req, State) ->
    F = fun(ReqF, StateF) ->
                    DestinationInfo = maps:get('destination_info',StateF),
                    Method = ?RESTU:method(ReqF, StateF),
                    case maps:get('resource_type',DestinationInfo) of
                        'folder' -> ?H_BASE_BEHAVIOR:content_types_provided(ReqF, StateF); % when Mode == '$multi'
                        'file' when Method==<<"GET">>; Method==<<"HEAD">> ->
                            file_content_types_provided(DestinationInfo,ReqF,StateF); % when Mode == '$single'; Mode == '$direct'
                        'file' -> ?H_BASE_BEHAVIOR:content_types_provided(ReqF, StateF); % when Mode == '$multi'
                        _ -> ?H_BASE_BEHAVIOR:content_types_provided(ReqF, StateF)
                    end end,
    ?RESTU:call(Req, State, content_types_provided, F).

%% @private
file_content_types_provided(DestinationInfo,Req,State) ->
    MediaType = case get_content_type(DestinationInfo, Req, State) of
                    undefined -> {<<"application">>,<<"octet-stream">>,[]}; % default MediaType
                    MType -> MType
                end,
    CTP = [{MediaType, provide_content}],
    {CTP, Req, State}.

%% @private
get_content_type(DestinationInfo, Req, State) ->
    case ?RESTU:call(Req, State, content_type) of
        no_call -> get_content_type_by_extension(DestinationInfo);
        MediaType -> MediaType
    end.

%% @private
get_content_type_by_extension(DestinationInfo) ->
    F = fun(FileName) ->
                case filename:extension(?BU:to_binary(FileName)) of
                    <<>> -> undefined;
                    <<$.,_/binary>> -> cow_mimetypes:all(FileName)
                end end,
    case maps:get('filename', DestinationInfo, undefined) of
        undefined -> undefined;
        FileName -> F(FileName)
    end.

%% ----------------------------------
charsets_provided(Req, State) ->
    F = fun(ReqF, StateF) -> ?H_BASE_BEHAVIOR:charsets_provided(ReqF, StateF) end,
    ?RESTU:call(Req, State, charsets_provided, F).

%% ----------------------------------
%% resource_exists(Req, State) ->
%%    ?H_BASE_BEHAVIOR:resource_exists(Req, State). % will try callback, then default itself.

%% ----------------------------------
last_modified(Req, State) ->
    DestinationInfo = maps:get('destination_info',State),
    case maps:get('resource_type',DestinationInfo) of
        'folder' -> ?H_BASE_BEHAVIOR:last_modified(Req, State);
        'file' ->
            FilePath = maps:get('filesystem_filepath', DestinationInfo),
            case ?RESTU_FS:meta(#{path => FilePath}) of
                {error,_} -> no_call;
                {ok, #file_info{mtime=Time}} -> {Time, Req, State}
            end;
        _ -> no_call
    end.

%% ----------------------------------
allow_missing_post(Req, State) ->
    ?RESTU:call(Req, State, allow_missing_post, false).

%% ----------------------------------
delete_resource(Req, State) ->
    DestinationInfo = maps:get('destination_info',State),
    case maps:get('resource_type',DestinationInfo) of
        'folder' -> handle_folder_method(?FUNCTION_NAME, Req, State);
        'file' -> handle_file_method(?FUNCTION_NAME, Req, State);
        _ -> handle_json_method(Req,State)
    end.

%% ----------------------------------
accept_json(Req, State) ->
    DestinationInfo = maps:get('destination_info',State),
    case maps:get('resource_type',DestinationInfo) of
        'folder' -> handle_folder_method(?FUNCTION_NAME, Req, State);
        'file' -> handle_file_method(?FUNCTION_NAME, Req, State);
        _ -> accept_any(accept_json, Req, State)
    end.

accept_form(Req, State) ->
    DestinationInfo = maps:get('destination_info',State),
    case maps:get('resource_type',DestinationInfo) of
        'folder' -> error;
        'file' -> error;
        _ -> accept_any(accept_form, Req, State)
    end.

accept_multipart(Req, State) ->
    DestinationInfo = maps:get('destination_info',State),
    case maps:get('resource_type',DestinationInfo) of
        'folder' -> handle_folder_method(?FUNCTION_NAME, Req, State);
        'file' -> handle_file_method(?FUNCTION_NAME, Req, State);
        _ -> accept_any(accept_multipart, Req, State)
    end.

accept_octet(Req, State) ->
    DestinationInfo = maps:get('destination_info',State),
    case maps:get('resource_type',DestinationInfo) of
        'folder' -> handle_folder_method(?FUNCTION_NAME, Req, State);
        'file' -> handle_file_method(?FUNCTION_NAME, Req, State);
        _ -> accept_any(accept_octet, Req, State)
    end.

accept_any(Req, State) ->
    DestinationInfo = maps:get('destination_info',State),
    case maps:get('resource_type',DestinationInfo) of
        'folder' -> handle_folder_method(?FUNCTION_NAME, Req, State);
        'file' -> handle_file_method(?FUNCTION_NAME, Req, State);
        _ -> error % handle_json_method(Req, State);
    end.

accept_any(Callback,Req,State) ->
    case ?H_BASE_BEHAVIOR:Callback(Req, State) of
        {stop,_,_}=Stop -> Stop; % already replied, just terminate
        {true, Req2, State2} -> handle_json_method(Req2, State2);
        {false, Req2, State2} -> ?RESTU:send_error(400, Req2, State2)
    end.

%% --------------------------------------------
%% callback returned by ?H_BASE_BEHAVIOR:content_types_provided()
%% --------------------------------------------
provide_json(Req, State) ->
    DestinationInfo = maps:get('destination_info',State),
    case maps:get('resource_type',DestinationInfo) of
        'folder' -> handle_folder_method(?FUNCTION_NAME, Req, State);
        'file' -> handle_file_method(?FUNCTION_NAME, Req, State); % ?RESTU:send_error(406, Req, State);
        _ ->
            case handle_json_method(Req, State) of
                {stop,_,_}=Stop -> Stop; % already replied, just terminate
                {{error,Reason}, Req2, State2} -> ?RESTU:send_error(Reason, Req2, State2);
                {Response, Req2, State2} -> ?RESTU:to_json({Response, Req2, State2})
            end
    end.

%% --------------------------------------------
%% callback returned by ?H_BASE_BEHAVIOR:content_types_provided()
%% --------------------------------------------
provide_content(Req, State) ->
    DestinationInfo = maps:get('destination_info',State),
    case maps:get('resource_type',DestinationInfo) of
        'folder' -> handle_folder_method(?FUNCTION_NAME, Req, State); % ?RESTU:send_error(406, Req, State);
        'file' -> handle_file_method(?FUNCTION_NAME, Req, State);
        _ -> error % handle_json_method(Req,State)
    end.

%% --------------------------------------------
%% callback returned by cowboy_static:content_types_provided()
%% --------------------------------------------
get_file(Req, State) ->
    EHandler = maps:get(actual_handler, State, maps:get(endpoint_handler, State)),
    case {cowboy_req:method(Req), ?BU:function_exported(EHandler, head_file, 2)} of
        {<<"HEAD">>,true} -> ?RESTU:call(Req, State, head_file);
        _ -> ?RESTU:call(Req, State, get_file)
    end.

%% ====================================================================
%% File functions
%% ====================================================================

set_content_disposition(FileName, Req, State) ->
    QsVals = cowboy_req:parse_qs(Req),
    Attachment = case lists:keyfind(<<"attachment">>, 1, QsVals) of {_,V} -> ?BU:to_bool(V); _ -> false end,
    case Attachment of
        true ->
            CDisp = case ?RESTU:call(Req, State, content_filename, fun(_,StateF) -> maps:get(content_filename,StateF,FileName) end) of
                        no_call -> <<"attachment">>;
                        FileNameX ->
                            case ?BU:urlencode(FileNameX) of
                                FN when FN==FileNameX -> <<"attachment; filename=",FileNameX/binary>>;
                                FileNameU -> <<"attachment; filename*=UTF-8''",FileNameU/binary>>
                            end end,
            cowboy_req:set_resp_header(<<"content-disposition">>, CDisp, Req);
        false -> Req
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---------------------------------------------------
%% JSON handlers
%% ---------------------------------------------------
handle_json_method(Req, State) ->
    Method = ?RESTU:method(Req, State),
    Callback = ?RESTU:get_method_func(Method),
    Ferr = fun(ReqF, StateF) ->
                {DMethods,_,_} = ?H_BASE_BEHAVIOR:declared_methods(Req, State),
                case lists:member(Method, DMethods) of
                    true ->
                        Reason = ?BU:strbin("callback '~p' is not implemented", [Callback]),
                        ?RESTU:send_error(500, {internal_error,Reason}, ReqF, StateF);
                    false ->
                        ?RESTU:send_error(405, ReqF, StateF)
                end end,
    ?RESTU:call(Req, State, Callback, Ferr).

%% ---------------------------------------------------
%% File handlers
%%   It happens that only requests to file attachments go through here.
%% ---------------------------------------------------
handle_file_method(FromFunName, Req, State) ->
    DestinationInfo = maps:get('destination_info', State),
    FileName = maps:get('filename', DestinationInfo),
    FilePath = maps:get('filesystem_filepath', DestinationInfo),
    DirPath = maps:get('filesystem_dirpath', DestinationInfo),
    case ?RESTU:method(Req, State) of
        <<"GET">> -> download_file(FilePath, FileName, Req, State);
        <<"HEAD">> -> head_file(FilePath, FileName, Req, State);
        <<"POST">> -> upload_file(FromFunName, DirPath, FilePath, Req, State);
        <<"PUT">> -> upload_file(FromFunName, DirPath, FilePath, Req, State);
        <<"DELETE">> -> delete_file(FilePath, Req, State);
        _Method -> ?RESTU:method_not_allowed(Req, State)
    end.

%% --------
%% @private
download_file(FilePath, FileName, Req0, State) ->
    Req = set_content_disposition(FileName, Req0, State),
    case ?RESTU_FS:download(#{path => FilePath}, Req) of
        {error,Reason} -> ?RESTU:send_error(Reason, Req, State);
        {ok, Req1} -> {<<>>, Req1, State}
    end.

%% --------
%% @private
head_file(FilePath, FileName, Req0, State) ->
    Req = set_content_disposition(FileName, Req0, State),
    case ?RESTU_FS:meta(#{path => FilePath}) of
        {error,Reason} -> ?RESTU:send_error(Reason, Req, State);
        {ok, #file_info{size=S}} ->
            Req1 = cowboy_req:set_resp_header(<<"content-length">>, ?BU:to_binary(S), Req),
            {<<>>, Req1, State}
    end.

%% --------
%% @private
upload_file(accept_multipart, DirPath, _FilePath, Req, State) ->
    case ?RESTU_FILE:accept_upload_many(DirPath, Req, State) of
        {ok, Req1, Files0} ->
            case lists:split(1,lists:filter(fun({file,_,ok}) -> true; (_) -> false end, Files0)) of
                {[File|_],_}  ->
                    State1 = State#{'uploaded_files' => [File]},
                    case ?RESTU:call(Req1,State1,after_upload_file,fun(ReqF,StateF) -> {ok,ReqF,StateF} end) of
                        {forward,R} -> R;
                        {ok,Req2,State2} ->
                            [FmtFile] = ?RESTU_FILE:format_uploaded_statuses([File]),
                            ?RESTU:send_response(FmtFile, Req2, State2); % single file answer (may be multi file answer is better)
                        {error,Reason} -> ?RESTU:send_error(Reason,Req1,State1)
                    end;
                _ ->
                    FmtFiles = ?RESTU_FILE:format_uploaded_statuses(Files0),
                    ?RESTU:send_response(FmtFiles, Req, State) % multi file answer (may be error is better)
            end;
        {stop,_,_}=Stop -> Stop
    end;
%%
upload_file(_, _DirPath, FilePath, Req, State) ->
    case ?RESTU_FILE:accept_upload_single(FilePath, Req, State) of
        {ok, Req1, Files} ->
            State1 = State#{'uploaded_files' => Files},
            case ?RESTU:call(Req1,State1,after_upload_file,fun(ReqF,StateF) -> {ok,ReqF,StateF} end) of
                {forward,R} -> R;
                {ok,Req2,State2} ->
                    [FmtFile] = ?RESTU_FILE:format_uploaded_statuses(Files),
                    ?RESTU:send_response(FmtFile, Req2, State2); % single file answer
                {error,Reason} -> ?RESTU:send_error(Reason,Req1,State1)
            end;
        {stop,_,_}=Stop -> Stop
    end.

%% --------
%% @private
delete_file(FilePath, Req, State) ->
    case ?RESTU_FS:delete(#{path => FilePath}) of
        {error,Reason} -> ?RESTU:send_error(Reason, Req, State);
        ok ->
            case ?RESTU:call(Req,State,after_delete_file,fun(ReqF,StateF) -> {ok,ReqF,StateF} end) of
                {forward,R} -> R;
                {ok,Req2,State2} -> {true, Req2, State2}; % 204 No Content
                {error,Reason} -> ?RESTU:send_error(Reason,Req,State)
            end end.

%% ---------------------------------------------------
%% Folder handlers
%%   It happens that only requests to folders of attachments go through here, hence the name.
%% ---------------------------------------------------
handle_folder_method(FromFunName, Req, State) ->
    case ?RESTU:method(Req, State) of
        M when M==<<"GET">>; M==<<"HEAD">> -> fs_list_folder(Req, State);
        <<"POST">> -> fs_upload_to_folder(FromFunName, Req, State);
        <<"CLEAR">> -> fs_clear_folder(Req, State);
        %<<"DELETE">> -> fs_delete_folder(Req, State);
        _Method -> ?RESTU:method_not_allowed(Req, State)
    end.

%% --------
%% @private
fs_list_folder(Req, State) ->
    Fdefault = fun(ReqF,StateF) ->
                        DestinationInfo = maps:get('destination_info', StateF),
                        DirPath = maps:get('filesystem_dirpath', DestinationInfo),
                        case ?RESTU_FS:list(#{path => DirPath}) of
                            {ok, DirList} ->
                                {DirList2,ReqF2,StateF2} = ?RESTU:apply_collselect_filters(DirList, ReqF, StateF),
                                ?RESTU:to_json({DirList2, ReqF2, StateF2});
                            {error,Reason} -> ?RESTU:send_error(Reason, ReqF, StateF)
                        end end,
    case ?RESTU:call(Req,State,list_folder,Fdefault) of
        {forward,R} -> R;
        %{ok,Req1,State1} -> ?RESTU:send_response(ok, Req1, State1);
        {error,Reason} -> ?RESTU:send_error(Reason,Req,State)
    end.

%% --------
%% @private
fs_upload_to_folder(accept_multipart, Req, State) ->
    DestinationInfo = maps:get('destination_info', State),
    DirPath = maps:get('filesystem_dirpath', DestinationInfo),
    case ?RESTU_FILE:accept_upload_many(DirPath, Req, State) of
        {ok, Req1, Files} ->
            State1 = State#{'uploaded_files' => Files},
            case ?RESTU:call(Req1,State1,after_upload_files,fun(ReqF,StateF) -> {ok,ReqF,StateF} end) of
                {forward,R} -> R;
                {ok,Req2,State2} ->
                    FmtFiles = ?RESTU_FILE:format_uploaded_statuses(Files),
                    ?RESTU:send_response(FmtFiles, Req2, State2); % multi file answer
                {error,Reason} -> ?RESTU:send_error(Reason,Req1,State1)
            end;
        {stop,_,_}=Stop -> Stop
    end;
%%
fs_upload_to_folder(_, Req, State) ->
    DestinationInfo = maps:get('destination_info', State),
    DirPath = maps:get('filesystem_dirpath', DestinationInfo),
    Filename = binary:replace(?BU:newid(),<<"-">>,<<>>,[global]),
    FilePath = filename:join([DirPath,Filename]),
    case ?RESTU_FILE:accept_upload_single(FilePath, Req, State) of
        {ok, Req1, Files} ->
            State1 = State#{'uploaded_files' => Files},
            case ?RESTU:call(Req1,State1,after_upload_file,fun(ReqF,StateF) -> {ok,ReqF,StateF} end) of
                {forward,R} -> R;
                {ok,Req2,State2} ->
                    [FmtFile] = ?RESTU_FILE:format_uploaded_statuses(Files),
                    ?RESTU:send_response(FmtFile, Req2, State2); % single file answer
                {error,Reason} -> ?RESTU:send_error(Reason,Req1,State1)
            end;
        {stop,_,_}=Stop -> Stop
    end.

%% --------
%% @private
fs_clear_folder(Req, State) ->
    Fdefault = fun(ReqF,StateF) -> ?RESTU:send_error(405, {invalid_operation,<<"CLEAR is not implemented">>},ReqF,StateF) end,
    case ?RESTU:call(Req,State,clear_folder,Fdefault) of
        {forward,R} -> R;
        {ok,Req1,State1} -> {true, Req1, State1}; % 204 No Content
        {stop,_,_}=Stop -> Stop
    end.
