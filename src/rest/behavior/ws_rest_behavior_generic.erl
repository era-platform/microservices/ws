%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.06.2021
%%% @doc

-module(ws_rest_behavior_generic).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behavior(cowboy_sub_protocol).

%% ====================================================================
%% REST generic behavior
%% ====================================================================

%% init/2 - is a required callback in line with cowboy_handler behavior.
%%          Typically returns {?H_XYZ, Req, Opts}. % to switch to that sub-protocol.
-callback init(Req, Opts) ->
    {ok | module(), Req, any()}
    | {module(), Req, any(), hibernate}
    | {module(), Req, any(), timeout()}
    | {module(), Req, any(), timeout(), hibernate}
    when Req::cowboy_req:req(), Opts::[{_,_}].

%% terminate/3 - is optional callback in line with cowboy_handler behavior.
%%               It probably does not make sense. Enable when there's a need for it.
%-callback terminate(any(), cowboy_req:req(), any()) -> ok.
%-optional_callbacks([terminate/3]).

%% Generic behavior callbacks by method name

-callback get(Req, State) ->
    {binary()|term(), Req, State} % NB: content must be returned
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([get/2]).

-callback post(Req, State) ->
    {boolean(), Req, State} % NB: response body must be set using ?RESTU:send_response()
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([post/2]).

-callback patch(Req, State) ->
    {boolean(), Req, State} % NB: response body must be set using ?RESTU:send_response()
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([patch/2]).

-callback put(Req, State) ->
    {boolean(), Req, State} % NB: response body must be set using ?RESTU:send_response()
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([put/2]).

-callback delete(Req, State) ->
    {boolean(), Req, State} % NB: response body must be set using ?RESTU:send_response()
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([delete/2]).

%% Generic behavior callbacks for serving files.

-callback get_file(Req, State) ->
    {{stream, non_neg_integer(), fun()}, Req, State}
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([get_file/2]).

-callback head_file(Req, State) ->
    {{stream, non_neg_integer(), fun()}, Req, State}
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([head_file/2]).

%% The same set of callbacks as in cowboy_rest is also provided.
%% Only used/useful callbacks are defined.
%% When more is needed, please uncomment/implement them.

-callback allowed_methods(Req, State) ->
    {[binary()], Req, State}
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().

-callback valid_entity_length(Req, State) ->
    {boolean(), Req, State}
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([valid_entity_length/2]).

-callback content_types_accepted(Req, State) ->
    {[{binary() | {binary(), binary(), '*' | [{binary(), binary()}]}, atom()}], Req, State}
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([content_types_accepted/2]).

-callback content_types_provided(Req, State) ->
    {[{binary() | {binary(), binary(), '*' | [{binary(), binary()}]}, atom()}], Req, State}
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([content_types_provided/2]).

-callback charsets_provided(Req, State) ->
    {[binary()], Req, State}
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([charsets_provided/2]).

%%-callback resource_exists(Req, State) ->
%%    {boolean(), Req, State}
%%    | {stop, Req, State}
%%    when Req::cowboy_req:req(), State::map().
%%-optional_callbacks([resource_exists/2]).

-callback allow_missing_post(Req, State) ->
    {boolean(), Req, State}
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([allow_missing_post/2]).

-callback delete_resource(Req, State) ->
    {boolean(), Req, State}
    | {stop, Req, State}
    when Req::cowboy_req:req(), State::map().
-optional_callbacks([delete_resource/2]).

%% ====================================================================
%% REST generic implementation
%% ====================================================================

%% cowboy_sub_protocol callbacks
-export([upgrade/4, upgrade/5]).

%% cowboy_rest callbacks
-export([
    known_methods/2,
    allowed_methods/2,
    is_authorized/2,
    forbidden/2,
    valid_entity_length/2,
    content_types_accepted/2,
    content_types_provided/2,
    charsets_provided/2,
    %resource_exists/2,
    allow_missing_post/2,
    delete_resource/2
]).

%% callbacks returned by content_types_accepted()
-export([
    accept_json/2,
    accept_form/2,
    accept_multipart/2,
    accept_octet/2
]).

%% callbacks returned by content_types_provided()
-export([provide_json/2]). % in case ?H_BASE_BEHAVIOR:content_types_provided() is used
-export([get_file/2]). % in case cowboy_static:content_types_provided() is used

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("rest.hrl").

%% ====================================================================
%% Interface functions
%% ====================================================================

%% -----------------------------
-spec upgrade(Req, Env, module(), any()) -> {ok, Req, Env}
    when Req::cowboy_req:req(), Env::cowboy_middleware:env().
%% -----------------------------
upgrade(Req0, Env, Handler, HandlerState0) ->
    ?H_BASE_BEHAVIOR:upgrade(?MODULE, Req0, Env, Handler, HandlerState0).

%% -----------------------------
%% cowboy_rest takes no options.
%% -----------------------------
-spec upgrade(Req, Env, module(), any(), any()) -> {ok, Req, Env}
    when Req::cowboy_req:req(), Env::cowboy_middleware:env().
%% -----------------------------
upgrade(Req, Env, Handler, HandlerState, _Opts) ->
    ?H_BASE_BEHAVIOR:upgrade(?MODULE, Req, Env, Handler, HandlerState).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ----------------------------------
known_methods(Req, State) ->
    ?H_BASE_BEHAVIOR:known_methods(Req, State).

%% ----------------------------------
allowed_methods(Req, State) ->
    ?H_BASE_BEHAVIOR:allowed_methods(Req, State). % utility, will use endpoint callback.

%% ----------------------------------
is_authorized(Req, State) ->
    ?H_BASE_BEHAVIOR:is_authorized(Req, State). % IAM is not to be customized.

%% ----------------------------------
forbidden(Req, State) ->
    ?H_BASE_BEHAVIOR:forbidden(Req, State). % IAM is not to be customized.

%% ----------------------------------
valid_entity_length(Req, State) ->
    ?H_BASE_BEHAVIOR:valid_entity_length(Req, State, ?REST_MAX_CONTENT_LENGTH_JSON). % will try callback, then default itself.

%% ----------------------------------
content_types_accepted(Req, State) ->
    F = fun(ReqF, StateF) -> ?H_BASE_BEHAVIOR:content_types_accepted(ReqF, StateF) end,
    ?RESTU:call(Req, State, content_types_accepted, F).

%% ----------------------------------
content_types_provided(Req, State) ->
    F = fun(ReqF, StateF) -> ?H_BASE_BEHAVIOR:content_types_provided(ReqF, StateF) end,
    ?RESTU:call(Req, State, content_types_provided, F).

%% ----------------------------------
charsets_provided(Req, State) ->
    F = fun(ReqF, StateF) -> ?H_BASE_BEHAVIOR:charsets_provided(ReqF, StateF) end,
    ?RESTU:call(Req, State, charsets_provided, F).

%% ----------------------------------
%% resource_exists(Req, State) ->
%%    ?H_BASE_BEHAVIOR:resource_exists(Req, State). % will try callback, then default itself.

%% ----------------------------------
allow_missing_post(Req, State) ->
    ?RESTU:call(Req, State, charsets_provided, false).

%% ----------------------------------
delete_resource(Req, State) ->
    handle_method(Req, State).

%% ----------------------------------
accept_json(Req, State) ->
    accept_any(accept_json, Req, State).

accept_form(Req, State) ->
    accept_any(accept_form, Req, State).

accept_multipart(Req, State) ->
    accept_any(accept_multipart, Req, State).

accept_octet(Req, State) ->
    accept_any(accept_octet, Req, State).

accept_any(Callback, Req, State) ->
    case ?H_BASE_BEHAVIOR:Callback(Req, State) of
        {stop,_,_}=Stop -> Stop; % already replied, just terminate
        {true, Req2, State2} -> handle_method(Req2, State2);
        {false, Req2, State2} -> ?RESTU:send_error(400, Req2, State2)
    end.

%% --------------------------------------------
%% callback returned by ?H_BASE_BEHAVIOR:content_types_provided()
%% --------------------------------------------
provide_json(Req, State) ->
    case handle_method(Req, State) of
        {stop,_,_}=Stop -> Stop; % already replied, just terminate
        {{error,Reason}, Req2, State2} -> ?RESTU:send_error(Reason, Req2, State2);
        {Response, Req2, State2} -> ?RESTU:to_json({Response, Req2, State2})
    end.

%% --------------------------------------------
%% callback returned by cowboy_static:content_types_provided()
%% --------------------------------------------
get_file(Req, State) ->
    EHandler = maps:get(actual_handler, State, maps:get(endpoint_handler, State)),
    case {cowboy_req:method(Req), ?BU:function_exported(EHandler, head_file, 2)} of
        {<<"HEAD">>,true} -> ?RESTU:call(Req, State, head_file);
        _ -> ?RESTU:call(Req, State, get_file)
    end.

%% ----------------------------------
handle_method(Req, State) ->
    Method = ?RESTU:method(Req, State),
    Callback = ?RESTU:get_method_func(Method),
    Ferr = fun(ReqF, StateF) ->
                    {DMethods,_,_} = ?H_BASE_BEHAVIOR:declared_methods(Req, State),
                    case lists:member(Method, DMethods) of
                        true ->
                            Reason = ?BU:strbin("callback '~p' is not implemented", [Callback]),
                            ?RESTU:send_error(500, {internal_error,Reason}, ReqF, StateF);
                        false ->
                            ?RESTU:send_error(405, ReqF, StateF)
                    end end,
    ?RESTU:call(Req, State, Callback, Ferr).