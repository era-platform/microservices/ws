%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.06.2021
%%% @doc

-module(ws_rest_behavior_utils_msg).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_error_tuple/1]).

%% ====================================================================
%% Response messages
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Response messages
%% ====================================================================

get_error_tuple({limited,MaxSize}) ->
    {limit, <<"upload exceeds maximum size of ",(?BU:to_binary(MaxSize))/binary>>};

get_error_tuple({not_enough_disk_space}) ->
    {internal_error, <<"Not enough free disk space">>};

get_error_tuple({bad_option,Reason}) ->
    {internal_error, ?BU:strbin("bad option: ~s",[Reason])};

get_error_tuple({file_open,ReasonAtom}) ->
    {internal_error, ?BU:to_binary(ReasonAtom)};

get_error_tuple({file_write,ReasonAtom}) ->
    {internal_error, ?BU:to_binary(ReasonAtom)};

get_error_tuple({upload_timeout}) ->
    {internal_error, <<"upload takes too long without progress">>};

get_error_tuple({'catch',C,E}) ->
    {internal_error, ?BU:strbin("Catched ~p:~p",[C,E])}.