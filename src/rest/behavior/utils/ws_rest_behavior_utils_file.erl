%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Anton Makarov.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Anton Makarov <anton@mastermak.ru>
%%% @date 07.09.2019
%%% @doc REST handlers file utilities.

-module(ws_rest_behavior_utils_file).
-author('Anton Makarov <anton@mastermak.ru>').

-export([
    accept_upload_single/3,
    accept_upload_many/3,
    format_uploaded_statuses/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("rest.hrl").

%% ====================================================================
%% Utility functions
%% ====================================================================

%% ----------------------------------
%% Upload one file from single-part upload request or first file from multi-part upload.
%% The rest of files are not even read from client, we don't have to.
%% ----------------------------------
accept_upload_single(FilePath, Req, State) ->
    case ?RESTU_FS:accept_upload(Req, #{path => FilePath, single_file => true}) of
        {ok, _Req1, _Files}=Ok -> Ok;
        {error,Reason} -> ?RESTU:send_error(Reason, Req, State);
        {error,Reason,Req2} -> ?RESTU:send_error(Reason, Req2, State);
        {error,Code,Reason,Req2} -> ?RESTU:send_error(Code, Reason, Req2, State)
    end.

%% ----------------------------------
%% Upload all files from either single-part or multi-part upload.
%% ----------------------------------
accept_upload_many(DirPath, Req, State) ->
    case ?RESTU_FS:accept_upload(Req, #{path => DirPath, single_file => false}) of
        {ok, _Req1, _Files}=Ok -> Ok;
        {error,Reason} -> ?RESTU:send_error(Reason, Req, State);
        {error,Reason,Req2} -> ?RESTU:send_error(Reason, Req2, State);
        {error,Code,Reason,Req2} -> ?RESTU:send_error(Code, Reason, Req2, State)
    end.

%% ----------------------------------
%% Format upload result list into form similar to what GET /fs/collection returns, with added status fields.
%% ----------------------------------
-spec format_uploaded_statuses(Files::[{file,binary(),ok|{error,HTTPCode::integer(),Reason::binary()}}]) -> [map()].
%% ----------------------------------
format_uploaded_statuses(Files) ->
    lists:map(fun format_uploaded_status/1, Files).

%% @private
format_uploaded_status({file,FilePath,ok}) ->
    #{name => filename:basename(FilePath),
      size => filelib:file_size(FilePath),
      status => ok};
format_uploaded_status({file,FilePath,{error,HTTPCode,Reason}}) ->
    {Code, Msg, Details} = ?RESTU_ERR:parse_error_details(Reason),
    St = #{ name => filename:basename(FilePath),
            size => filelib:file_size(FilePath),
            status => error,
            error_http => HTTPCode,
            error_code => Code,
            error_message => Msg },
    case Details of
        [] -> St;
        _ -> St#{ error_details => Details }
    end.

