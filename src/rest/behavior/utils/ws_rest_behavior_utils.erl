%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.06.2021
%%% @doc

-module(ws_rest_behavior_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("rest.hrl").

%% ====================================================================
%% Utility functions
%% ====================================================================

%% Return method() that would handle given binary() HTTP verb.
get_method_func(<<"GET">>) -> get;
get_method_func(<<"HEAD">>) -> head;
get_method_func(<<"OPTIONS">>) -> options;
get_method_func(<<"POST">>) -> post;
get_method_func(<<"PATCH">>) -> patch;
get_method_func(<<"PUT">>) -> put;
get_method_func(<<"DELETE">>) -> delete;
get_method_func(<<"LOOKUP">>) -> lookup;
get_method_func(Method) -> ?BU:to_atom_new(?BU:to_lower(Method)).

get_bindings(Bindings, Req, State) -> get_bindings(Bindings, Req, State, []).
get_bindings([], _Req, _State, Acc) -> {ok,lists:reverse(Acc)};
get_bindings([Binding|Rest], Req, State, Acc) ->
    case cowboy_req:binding(Binding, Req) of
        undefined ->
            Msg = ?BU:strbin("Route is misconfigured: '~p' binding is not found.", [Binding]),
            send_error(500, {internal_error,Msg}, Req, State);
        Val ->
            get_bindings(Rest, Req, State, [Val|Acc])
    end.

%% ==================================
%% Encoding response
%% ==================================

to_json({stop, _Req, _State}=Stop) -> Stop;
to_json({Body, _Req, _State}=Res) when is_binary(Body) -> Res;
to_json({Term, Req, State}) ->
    Body = case catch jsx:encode(Term, [{space, 1}, {indent, 2}]) of
               {'EXIT',_} ->
                   send_error(500, {internal_error, <<"Could not encode result to JSON">>}, Req, State);
               B -> B
           end,
    Body1 = <<Body/binary,"\n">>,
    {Body1, Req, State}.

%% ==================================
%% Sending response
%% ==================================

%% ----------------------------------
%% Send Response encoded to json binary in body and return true value.
%% ----------------------------------
send_response(Data, Req, State) ->
    {Body, Req1, State1} = to_json({Data, Req, State}),
    Req2 = cowboy_req:set_resp_body(Body, Req1),
    {true, Req2, State1}.

%% ----------------------------------
%% Send error Reason in body and reply with HTTP error code derived from Reason, return stop.
%% ----------------------------------
send_error(Reason, Req, State) ->
    HttpCode = get_code_from_reason(Reason),
    send_error(HttpCode, Reason, Req, State).

%% @private
get_code_from_reason(HttpCode) when is_integer(HttpCode) andalso HttpCode < 1000 -> HttpCode;
get_code_from_reason({access_denied,_}) -> 403;
get_code_from_reason({already_exist,_}) -> 403;
get_code_from_reason({already_exists,_}) -> 403;
get_code_from_reason({method_not_allowed,_}) -> 405;
get_code_from_reason({exception,_}) -> 500;
get_code_from_reason({internal_error,_}) -> 500;
get_code_from_reason({service_unavailable,_}) -> 503;
get_code_from_reason({invalid_operation,_}) -> 409;
get_code_from_reason({invalid_params,_}) -> 422;
get_code_from_reason({invalid_request,_}) -> 422; % XXX in DC terms it means 422 here; but in http context 'invalid_request' would be 400.
get_code_from_reason({limited,_}) -> 413; % Payload Too Large
get_code_from_reason({not_connected,_}) -> 503;
get_code_from_reason({not_found,_}) -> 404;
get_code_from_reason({not_loaded,_}) -> 503;
get_code_from_reason(_) -> 500.

%% ----------------------------------
%% Send error Reason in body and reply with specified HTTP error code, return stop.
%% ----------------------------------
send_error(HttpCode, Reason, Req, State) ->
    Body = ?RESTU_ERR:convert_error(Reason),
    Req2 = cowboy_req:set_resp_header(<<"content-type">>, <<"application/json; charset=utf-8">>, Req),
    Req3 = case cowboy_req:method(Req) of
               <<"HEAD">> -> cowboy_req:set_resp_header(<<"content-length">>, ?BU:to_binary(size(Body)), Req2);
               _ -> cowboy_req:set_resp_body(Body, Req2)
           end,
    Req4 = cowboy_req:reply(HttpCode, Req3),
    {stop, Req4, State}.

%% ----------------------------------
%% Send 405 Method Not Allowed response, setting 'allow' header to currently allowed methods list.
%% ----------------------------------
method_not_allowed(Req, State) ->
    {Methods, Req1, State1} = callb(Req, State, allowed_methods),
    << ", ", Allow/binary >> = << << ", ", M/binary >> || M <- Methods >>,
    Req2 = cowboy_req:set_resp_header(<<"allow">>, Allow, Req1),
    send_error(405, <<"Method Not Allowed">>, Req2, State1).

%% ----------------------------------
%% Return POST for custom http methods but keep methods known to cowboy as-is.
%% ----------------------------------

to_known_cowboy_method(Method,_) when
    Method =:= <<"GET">>; Method =:= <<"HEAD">>; Method =:= <<"POST">>; Method =:= <<"PATCH">>;
    Method =:= <<"PUT">>; Method =:= <<"DELETE">>; Method =:= <<"OPTIONS">> -> Method; % known methods do not change.
to_known_cowboy_method(_,false) -> <<"GET">>; % avoid cowboy_rest:accept_resource() when there is no content.
to_known_cowboy_method(_,_) -> <<"POST">>.

%% ==================================
%% Return fallback request method from last path part capitalized or actual request method if fallback is not found.
%% ==================================

method(Req, State) when is_map(State) ->
    case maps:get(method, State, undefined) of
        undefined ->
            Method = cowboy_req:method(Req),
            Path = cowboy_req:path(Req),
            %PathInfo = cowboy_req:path_info(Req),
            Parts = binary:split(Path, <<"/">>, [global]),
            % retrieve custom method (if specified)
            LastPart = lists:last(Parts),
            case binary:matches(LastPart, ?FB_METHOD_SEP) of
                [] -> Method;
                Matches ->
                    {LastMatchStart,_} = lists:last(Matches),
                    case binary:part(LastPart, LastMatchStart+1, size(LastPart)-LastMatchStart-1) of
                        <<>> -> Method;
                        CM -> ?BU:to_upper(CM)
                    end end;
        Method -> Method
    end;
method(Req, _) -> cowboy_req:method(Req).

%% ==================================
%% State helpers
%% ==================================

is_collection(State) -> maps:get('is_collection', State, false).

get_route_domain_types(State) -> maps:get('domain_types', State, []). % specified in r_ws_routes_rest module

%% ====================================================================
%% Check URL route options and reply with error if parameters do not match.
%% ====================================================================

%% ----------------------------------
%% Check request (domain type), check State#{iam} is logged in and call NextFun if checks validate.
%% Otherwise an error is sent in response and {stop,_,_} is returned to the caller.
%% ----------------------------------
check_req_get_domain(_Req, State) ->
    case maps:get(iam, State, undefined) of
        IAM when is_map(IAM) ->
            Domain = maps:get('domain', IAM),
            IsRoot = case maps:get('domain_type', IAM) of
                           'root' -> true;
                           _ -> false
                       end,
            {ok, Domain, IsRoot}; % success
        _ ->
            {ok, ?NoAuthDomain, false} % emulate domain handler for public endpoints
    end.

%% ==================================
%% Call functions from callback modules or terminate request in case of crash.
%% ==================================

%% -------------------------------------
%% Run the Callback function in 'actual_handler' or 'endpoint_handler' module with [Req, State] args or return 'no_call' atom.
%% Actual handler is set in fallback case (tail url!method using POST).
%% -------------------------------------
call(Req, State, Callback) ->
    EHandler = maps:get(actual_handler, State, maps:get(endpoint_handler, State)),
    callx(Req, State, EHandler, Callback).

%% -------------------------------------
%% Run the Callback function in 'behavior_handler' module with [Req, State] args or return 'no_call' atom.
%% -------------------------------------
callb(Req, State, Callback) ->
    BHandler = maps:get(behavior_handler, State),
    callx(Req, State, BHandler, Callback).

%% -------------------------------------
%% Run the Callback function in Module with [Req, State] args or return 'no_call' atom.
%% In case function crashes, reply to client and terminate the request. Never returns in that case.
%% -------------------------------------
callx(Req, State, Module, Callback) ->
    case ?BU:function_exported(Module, Callback, 2) of
        false -> no_call;
        true ->
            try Module:Callback(Req, State)
            catch Class:Reason:StackTrace ->
                error_terminate(Req, State, Class, Reason, StackTrace, Callback)
            end end.

%% -------------------------------------
%% A variant of call/3 with Default value or function/2 that is used in case of no_call.
%% -------------------------------------
call(Req, State, Callback, Default) ->
    case call(Req, State, Callback) of
        no_call ->
            case erlang:is_function(Default) of
                true -> Default(Req, State);
                false -> {Default, Req, State}
            end;
        force_no_call -> no_call;
        Resp -> Resp % {Value, Req, State} | {stop, Req, State}
    end.

%% -------------------------------------
%% Run the Callback function and save returned value to State under the same key.
%% If returned value is specially-formatted {error,_} tuple, send_error() right away that returns {stop,Req,State}.
%% For other unrecognized values just return {error,Req,State} losing the returned value.
%% -------------------------------------
-spec save_cb_val_or_fail(Req, State, Callback) -> {ok|error|stop,Req,State} when Req::cowboy_req:req(), State::map(), Callback::atom().
%% -------------------------------------
save_cb_val_or_fail(Req, State, Callback) ->
    case catch call(Req, State, Callback) of
        {ok, Val} -> {ok, Req, State#{Callback => Val}};
        no_call -> send_error(500, {internal_error,<<"Endpoint Handler:",(?BU:to_binary(Callback))/binary,"/2 is not implemented">>}, Req, State);
        {'EXIT',Exit} ->
            ?LOG('$crash', "Endpoint Handler:~ts/2 crashed: ~120tp", [Callback, Exit]),
            send_error(500, {internal_error,?BU:strbin("Endpoint Handler:~p/2 crashed",[Callback])}, Req, State);
        {error,{ReasonAtom,ReasonMsg}=Reason} when is_atom(ReasonAtom), is_binary(ReasonMsg) ->
            send_error(Reason, Req, State);
        {error,Code,{ReasonAtom,ReasonMsg}=Reason} when is_integer(Code), is_atom(ReasonAtom), is_binary(ReasonMsg) ->
            send_error(Code, Reason, Req, State);
        _ -> {error,Req,State} % 403 Forbidden - cannot build path to file (e.g. session/domain is missing).
    end.

%% -------------------------------------
%% A variant of call/3 when callback function accepts 0 arguments.
%% -------------------------------------
call0(Req, State, Callback) ->
    Handler = maps:get(actual_handler, State, maps:get(endpoint_handler, State)),
    case ?BU:function_exported(Handler, Callback, 0) of
        false -> no_call;
        true ->
            try Handler:Callback()
            catch Class:Reason:StackTrace ->
                error_terminate(Req, State, Class, Reason, StackTrace, Callback)
            end end.

%% -------------------------------------
%% A variant of call/3 when callback function accepts 1 argument that is passed directly.
%% -------------------------------------
call1(Req, State, Callback, Arg) ->
    Handler = maps:get(actual_handler, State, maps:get(endpoint_handler, State)),
    case ?BU:function_exported(Handler, Callback, 1) of
        false -> no_call;
        true ->
            try Handler:Callback(Arg)
            catch Class:Reason:StackTrace ->
                error_terminate(Req, State, Class, Reason, StackTrace, Callback)
            end end.

%% -------------------------------------
%% A variant of call/3 when callback function accepts 2 arguments that are passed directly.
%% -------------------------------------
call2(Req, State, Callback, Arg1, Arg2) ->
    Handler = maps:get(actual_handler, State, maps:get(endpoint_handler, State)),
    case ?BU:function_exported(Handler, Callback, 2) of
        false -> no_call;
        true ->
            try Handler:Callback(Arg1, Arg2)
            catch Class:Reason:StackTrace ->
                error_terminate(Req, State, Class, Reason, StackTrace, Callback)
            end end.

%% -------------------------------------
%% A variant of call/3 when callback function accepts 3 arguments of which 2 are passed directly and 3rd is State.
%% -------------------------------------
call2st(Req, State, Callback, Arg1, Arg2) ->
    Handler = maps:get(actual_handler, State, maps:get(endpoint_handler, State)),
    case ?BU:function_exported(Handler, Callback, 3) of
        false -> no_call;
        true ->
            try Handler:Callback(Arg1, Arg2, State)
            catch Class:Reason:StackTrace ->
                error_terminate(Req, State, Class, Reason, StackTrace, Callback)
            end end.

%% -------------------------------------
%% A copy of cowboy_rest:error_terminate() modified for using REST State map.
%% -------------------------------------
-spec error_terminate(cowboy_req:req(), map(), atom(), any(), any(), atom()) -> no_return().
%% -------------------------------------
error_terminate(Req, State, Class, Reason, StackTrace, _Callback) ->
    Handler = maps:get(actual_handler, State, maps:get(endpoint_handler, State)),
    cowboy_handler:terminate({crash, Class, Reason}, Req, State, Handler),
    erlang:raise(Class, Reason, StackTrace).

%% ==================================
%% EnvCollSelect arguments preparation.
%% ==================================

parse_collselect_params(QsVals) ->
    Fields = [<<"filter">>,<<"order">>,<<"mask">>,<<"flat">>,<<"countonly">>,<<"offset">>,<<"limit">>],
    lists:filter(fun({K,_}) -> lists:member(K,Fields) end, QsVals).

%% -------------------------------------
%% apply EnvCollSelect to collection and return 3-tuple (useful for GET).
%% -------------------------------------
apply_collselect_filters(Items, Req, State) ->
    AfterFun = fun(L,_,_) -> L end,
    apply_collselect_filters(Items, AfterFun, Req, State).

%% -------------------------------------
%% apply EnvCollSelect to pre-filtered collection and return 3-tuple (useful for GET).
%% -------------------------------------
apply_collselect_filters_without_offset(Items, Req, State) ->
    AfterFun = fun(L,_,_) -> L end,
    ParamsFilterFun = fun({<<"offset">>,_}) -> false; (_) -> true end,
    apply_collselect_filters(Items, ParamsFilterFun, AfterFun, Req, State).

%% -------------------------------------
%% apply EnvCollSelect to collection, apply AfterFun to the resulting list and return 3-tuple (useful for GET).
%% -------------------------------------
apply_collselect_filters(Items, AfterFun, Req, State) ->
    ParamsFilterFun = fun(_) -> true end,
    apply_collselect_filters(Items, ParamsFilterFun, AfterFun, Req, State).

%% -------------------------------------
%% apply EnvCollSelect common implementation with all parameters as above.
%% -------------------------------------
apply_collselect_filters(Items, ParamsFilterFun, AfterFun, Req, State) ->
    QsVals = cowboy_req:parse_qs(Req),
    SelectOpts0 = parse_collselect_params(QsVals),
    SelectOpts1 = lists:filter(ParamsFilterFun, SelectOpts0),
    SelectOpts = [{format,map} | SelectOpts1],
    Res = case ?BLselect:select(Items, SelectOpts) of
              {ok,C} when is_integer(C) -> [{count,C}];
              {ok,L} when is_list(L) -> AfterFun(L, Req, State)
          end,
    {Res, Req, State}.

%% ==================================
%% other arguments preparations.
%% ==================================
parse_timeout_val(Timeout0) ->
    case ?BU:is_int_val(Timeout0) of
        false -> {error, {invalid_params,<<"Timeout must be integer.">>}};
        true ->
            Timeout = ?BU:to_integer(Timeout0),
            case Timeout > 0 of
                false -> {error, {invalid_params,<<"Timeout must be greater than 0.">>}};
                true -> {ok, Timeout}
            end end.

%% --------------------------------------------------------------------
%% Hyper-links
%% --------------------------------------------------------------------

%% ----------------------------------
%% Make list of hyper links from list of elements appended as the last url part.
%% ----------------------------------
make_hyper_links(Elements, Req) ->
    Path = cowboy_req:path(Req),
    lists:map(fun(Element) when is_binary(Element) ->
                    Href = <<Path/binary,"/",Element/binary>>,
                    Links = [{<<"self">>, [{<<"href">>, Href}]}],
                    [{<<"name">>,Element},{<<"_links">>,Links}];
                 ({Element,Params}) when is_binary(Element), is_list(Params) ->
                    Href = <<Path/binary,"/",Element/binary>>,
                    Links = [{<<"self">>, [{<<"href">>, Href},{<<"params">>,Params}]}],
                    [{<<"name">>,Element},{<<"_links">>,Links}]
              end, Elements).
