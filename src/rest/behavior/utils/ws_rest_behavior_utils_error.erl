%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.06.2021
%%% @doc

-module(ws_rest_behavior_utils_error).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([convert_error/1,
         parse_error_details/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-type error_code_int() :: non_neg_integer().
-type error_reason_msg() :: binary() | string().

-type error_input() :: error_code_int()
    | atom()
    | {error_code_int(), error_reason_msg()}
    | {Code :: atom()|binary(), error_reason_msg()}
    | term(). % will be ~tp formatted.

%% ====================================================================
%% Public functions
%% ====================================================================

%% -----------------------------------------------
-spec convert_error(Error::error_input()) -> JSON::binary().
%% -----------------------------------------------
convert_error(Error) ->
    {Code,Msg,Details} = parse_error_details(Error),
    R = build_error(Code,Msg,Details),
    jsx:encode(R).

%% -----------------------------------------------
-spec parse_error_details(Error::error_input()) -> { Code :: integer(),
    Msg :: binary(),
    Details :: [{binary(),binary()}] }.
%% -----------------------------------------------
parse_error_details(Error) ->
    {Code, Reason} = parse_error(Error),
    {ok,Msg,Details} = parse_reason_msg(Reason),
    {Code,Msg,Details}.

%% @private
%% message could contain extra fields inside text. ex. wrong field names in crud
parse_reason_msg(<<>>) -> {ok, <<>>, []};
parse_reason_msg(Reason) when is_binary(Reason) ->
    RE = <<"([A-Za-z0-9_-]+)=([^\\|]+)\\|">>,
    case re:run(Reason,RE,[global]) of
        nomatch -> {ok, Reason, []};
        {match,List} ->
            Opts = lists:foldl(fun([_,{KS,KL},{VS,VL}],Acc) ->
                [{binary_part(Reason,KS,KL), binary_part(Reason,VS,VL)} | Acc]
                               end, [], List),
            {ok, re:replace(Reason,RE,<<>>,[global,{return,binary}]), Opts}
    end.

%% @private
build_error(Code,Msg,Details) ->
    case Details of
        [] ->
            [{<<"error_code">>, Code},
                {<<"error_message">>, Msg}];
        _ ->
            [{<<"error_code">>, Code},
                {<<"error_message">>, Msg},
                {<<"error_details">>, Details}]
    end.

%% ====================================================================
%% Interface functions
%% ====================================================================

%% -----------------------------------------------
-spec parse_error(Input::error_input()) -> {Code::integer(), MsgWithDetails::binary()}.
%% -----------------------------------------------
parse_error(Code) when is_integer(Code) -> {Code, ?BU:to_binary(Code)};
parse_error(Code) when is_atom(Code); is_binary(Code) -> {parse_error_code(?BU:to_binary(Code)), ?BU:to_binary(Code)};
parse_error({Code,Reason}) when is_integer(Code), is_binary(Reason) -> {Code, Reason};
parse_error({Code,Reason}) when is_integer(Code), is_list(Reason) -> {Code, ?BU:to_binary(Reason)};
parse_error({Code,Reason}) when is_atom(Code) orelse is_binary(Code) andalso is_binary(Reason) -> {parse_error_code(?BU:to_binary(Code)), Reason};
parse_error({Code,Reason}) when is_atom(Code) orelse is_binary(Code) andalso is_list(Reason) -> {parse_error_code(?BU:to_binary(Code)), ?BU:to_binary(Reason)};
parse_error(Error) -> {1500, ?BU:strbin("~tp", [Error])}.

%% @private
parse_error_code(<<"access_denied">>) -> 1401;
parse_error_code(<<"already_exists">>) -> 1402;
parse_error_code(<<"not_implemented">>) -> 1403;
parse_error_code(<<"not_found">>) -> 1404;
parse_error_code(<<"invalid_request">>) -> 1405;
parse_error_code(<<"invalid_operation">>) -> 1406;
parse_error_code(<<"unauthorized">>) -> 1407;
parse_error_code(<<"not_loaded">>) -> 1408;
parse_error_code(<<"operation_failed">>) -> 1409;
parse_error_code(<<"limited">>) -> 1410;
parse_error_code(<<"license_restriction">>) -> 1411;
parse_error_code(<<"config_restriction">>) -> 1412;
parse_error_code(<<"invalid_params">>) -> 1413;
parse_error_code(<<"internal_error">>) -> 1501;
parse_error_code(<<"exception">>) -> 1502;
parse_error_code(<<"throw">>) -> 1503;
parse_error_code(<<"exit">>) -> 1504;
parse_error_code(A) when is_atom(A) -> 1505;
parse_error_code(_) -> 1506.