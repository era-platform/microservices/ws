%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.06.2021
%%% @doc

-module(ws_rest_behavior_utils_iam).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    authenticate/2,
    check_access/2, check_access/4
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ------------------------------------------
%% Authenticate REST request
%% Put 'iam' object into state
%% ------------------------------------------
-spec authenticate(Req::cowboy_req:req(), State::map()) -> {Req1::cowboy_req:req(), State1::map()}.
%% ------------------------------------------
authenticate(Req,State) ->
    TokenRes = extract_token(Req),
    case get_domain(TokenRes) of
        undefined -> {Req,State}; % unauthorized (no domain cookie when dc)
        Domain ->
            IAM = #{'domain' => Domain,
                    'domain_type' => case Domain of 'root' -> 'root'; _ -> 'sub' end,
                    'user_id' => undefined},
            case ?WsUtils:has_iam() of
                false -> {Req,State#{iam => IAM}};
                true ->
                    case get_session(TokenRes) of
                        false ->
                            {Req,State}; % unauthorized (no session cookie/token when iam)
                        {wrong,_SessionId} ->
                            Req1 = ?WsUtils:erase_cookie(?CookieSession,Req),
                            {Req1,State}; % erase wrong auth cookies
                        {ok,SessionItem} ->
                            Req1 = ?WsUtils:renew_session(SessionItem,Req), % renew cookies and sessions?
                            IAM1 = IAM#{'user_id' => maps:get(<<"userid">>,SessionItem),
                                        'session_id' => maps:get(<<"id">>,SessionItem)},
                            {Req1,State#{iam => IAM1}}
                    end end end.

%% ------------------------------------------
%% Authorize REST request
%% Check if route permitted
%% ------------------------------------------
-spec check_access(Req::cowboy_req:req(), State::map()) -> ok | {error,Reason::term()}.
%% ------------------------------------------
check_access(Req,State) ->
    {Domain,UserId} = extract_domain_user(State),
    Method = cowboy_req:method(Req),
    Path = ?BU:urldecode(cowboy_req:path(Req)),
    check_access(Domain,UserId,Method,Path).

%% ------------------------------------------
%% Authorize REST request
%% Check if route permitted
%% ------------------------------------------
-spec check_access(Domain::binary()|atom(), UserId::binary(), Method::binary(), Path::binary()) -> ok | {error,Reason::term()}.
%% ------------------------------------------
check_access(Domain,UserId,Method,Path) ->
    case ?WsUtils:has_iam() of
        false -> ok;
        true ->
            IamAuthArgs = [{'endpoint',Path,Method},{'user',UserId}],
            GlobalName = ?GN_NAMING:get_globalname(?MsvcIam,Domain),
            case catch ?GLOBAL:gen_server_call(GlobalName,{'authorize',IamAuthArgs}) of
                {'EXIT',Reason} ->
                    ?LOG('$crash',"IAM call crashed: ~120tp",[Reason]),
                    {error,{internal_error,<<"IAM is not accessible">>}};
                {error,_}=Err -> Err;
                ok -> ok
            end end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ------------------------------------------
%% Return session/domain token from request or false
%% First from Authorization header when bearer, if not then from cookie
%% ------------------------------------------
-spec extract_token(Req::cowboy_req:req()) -> {bearer,Value::binary()} | {cookie,Value::binary()} | false.
%% ------------------------------------------
extract_token(Req) ->
    case ?WsUtils:get_authorization(Req) of
        {ok,{<<"bearer">>,Value}} -> {bearer,Value};
        false ->
            case ?WsUtils:get_cookie(?CookieSession,Req) of
                {ok,Value} -> {cookie,Value};
                false -> false
            end end.

%% ------------------------------------------
%% Return domain by request
%% ------------------------------------------
-spec get_domain(TokenRes::{bearer,binary()} | {cookie,binary()} | false)
      -> undefined | 'root' | binary().
%% ------------------------------------------
get_domain(TokenRes) ->
    case ?WsUtils:has_dc() of
        false -> 'root';
        true ->
            F = fun(Token) ->
                    case ?ParseToken(Token) of
                        {ok,_SessionId,<<"root">>} -> 'root';
                        {ok,_SessionId,Domain} -> Domain;
                        error -> undefined
                    end end,
            case TokenRes of
                {bearer,Token} -> F(Token);
                {cookie,Token} -> F(Token);
                false -> undefined
            end end.

%% -------------------------------------------------------
%% Return session by request
%% ------------------------------------------
-spec get_session(TokenRes::{bearer,binary()} | {cookie,binary()} | false)
      -> {ok,SessionItem::map()} | false.
%% -------------------------------------------------------
get_session(false) -> false;
get_session({_,Token}) ->
    case ?ParseToken(Token) of
        {ok,SessionId,<<"root">>} -> ?WsUtils:find_session('root',SessionId);
        {ok,SessionId,Domain} -> ?WsUtils:find_session(Domain,SessionId);
        error -> false
    end.

%% -------------------------------------------------------
%% Extract Domain, UserId from authenticated state for authorization in IAM
%% If not authenticated, then try Domain='root', User=undefined for general public endpoints
%% -------------------------------------------------------
extract_domain_user(State) ->
    case maps:get(iam,State,undefined) of
        undefined -> {'root',undefined};
        Iam -> {maps:get('domain',Iam), maps:get('user_id',Iam)}
    end.