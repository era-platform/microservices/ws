%%% @author Pavel Abramov
%%% @date 08.08.2019
%%% @doc REST handlers file utilities.

-module(ws_rest_behavior_utils_fs).
-author('Pavel Abramov').

-export([
    accept_upload/2,
    download/2,
    list/1,
    delete/1,
    meta/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("rest.hrl").

%% ====================================================================
%% Interface functions
%% ====================================================================

%% ---------------------------
%% Accept incoming upload of single or multiple parts and return uploaded files listing with statuses.
%% When there is at least one successfully uploaded part, an {ok,Req,FNL} is returned, with possible error statuses inside FNL.
%% Single-part uploads return either {ok,_,_} which means the status is also ok in the listing, or natural error with possible HTTP Code.
%% Multi-part uploads can also return error when it comes before first successfully uploaded part.
%% ---------------------------
-spec accept_upload(Req, Params::map()) -> {ok, Req, FileNameList::list()} |
                                           {error, Reason} |
                                           {error, Reason, Req} |
                                           {error, Code::integer(), Reason, Req}
                                             when Req::cowboy_req:req(), Reason::{atom(),binary()}.
%% ---------------------------
accept_upload(Req, Params) when is_map(Params) ->
    case check_and_default_params(Params) of
        {ok, ParamsU} ->
            case cowboy_req:parse_header(<<"content-type">>, Req) of
                {<<"multipart">>, <<"form-data">>, _} -> receive_multipart(Req, ParamsU, []);
                _ -> receive_any(Req, ParamsU, [])
            end;
        {error, _}=Error -> Error
    end.

%% ---------------------------
%-spec download(Params::map(), Req) -> {... TODO
%% ---------------------------
download(Params, Req) when is_map(Params) ->
    case check_and_default_params(Params) of
        {ok, ParamsU} ->
            Path = ?FILEPATHS:get_local_path(maps:get(path, ParamsU)),
            case ?FILER:check_file_existing(Path) of
                true -> send_file(Path, Req);
                false ->
                    {error, {not_found, <<"File not found (D3) ">>}}
            end;
        {error, _}=Error -> Error
    end.

%% ---------------------------
list(Params) ->
    case check_and_default_params(Params) of
        {ok, ParamsU} ->
            Path = ?FILEPATHS:get_local_path(maps:get(path, ParamsU)),
            case ?FILESVC:check_dir_exists(Path) of
                true ->
                    LS = maps:get(ls, ParamsU, true),
                    SubDir = false, % never recursive
                    {ok, [{<<"files">>,FList}]} = ?FILER:get_file_list(Path, "*", LS, SubDir),
                    FList2 = lists:filtermap(fun([{<<"dir">>,_}]) -> false;
                                                ([{<<"file">>,FInfo}]) ->
                                                     FInfo2 = lists:map(fun({<<"size">>=K,BinSize}) -> {K,?BU:to_int(BinSize)};
                                                                           (T) -> T
                                                                        end, FInfo),
                                                     {true, FInfo2};
                                                (_) -> false
                                             end, FList),
                    {ok, FList2};
                false ->
                    {error, {not_found, <<"Directory not found on server">>}}
            end;
        {error, _}=Error -> Error
    end.

%% ---------------------------
delete(Params) ->
    case check_and_default_params(Params) of
        {ok, ParamsU} ->
            Path = ?FILEPATHS:get_local_path(maps:get(path, ParamsU)),
            case ?FILER:check_file_existing(Path) of
                true -> ?FILER:delete_file(?BU:to_list(Path));
                false -> {error, {not_found, <<"File not found (D2)">>}}
            end;
        {error, _}=Error -> Error
    end.

%% ---------------------------
meta(Params) ->
    case check_and_default_params(Params) of
        {ok, ParamsU} ->
            Path = ?FILEPATHS:get_local_path(maps:get(path, ParamsU)),
            case ?FILER:check_file_existing(Path) of
                true -> ?BLfile:read_file_info(Path);
                false -> {error, {not_found, <<"File not found (M)">>}}
            end;
        {error, _}=Error -> Error
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----------------------------------------------
check_and_default_params(Params) ->
    case maps:is_key(path, Params) of
        false -> {error, {internal_error,<<"key 'path' not found in Params">>}};
        true -> {ok, maps:update_with(maxcontentlength, fun(V) -> V end, ?REST_MAX_CONTENT_LENGTH_FILE, Params)}
    end.

%% -----------------------------------------------
%% Handle non-multipart upload of single body.
%% -----------------------------------------------
receive_any(Req, Params, FileNameList) ->
    case check_diskspace() of
        true ->
            case check_content_length(Req, Params) of
                true -> do_receive_any(Req, Params, FileNameList);
                {false,MaxSize} ->
                    {error, 413, ?RESTU_MSG:get_error_tuple({limited,MaxSize}), Req}
            end;
        false -> {error, ?RESTU_MSG:get_error_tuple({not_enough_disk_space}), Req}
    end.

%% @private
do_receive_any(Req, Params, FileNameList) ->
    % CASE
    {Req1, UpFileNameList} = receive_file(Params, Req, 'field-name-is-ignored', octet, FileNameList),
    {ok, Req1, UpFileNameList}.

%% -----------------------------------------------
%% Handle multipart upload, receiving multiple files.
%% -----------------------------------------------
receive_multipart(Req, #{single_file:=true}, [_|_]=FileNameList) -> {ok, Req, FileNameList}; % stop after first file is received
receive_multipart(Req, Params, FileNameList) ->
    case check_diskspace() of
        true ->
            case check_content_length(Req, Params) of
                true -> do_receive_multipart(Req, Params, FileNameList);
                {false,MaxSize} ->
                    % FileNameList can only be empty because content-length is only defined on whole request, not on parts.
                    {error, 413, ?RESTU_MSG:get_error_tuple({limited,MaxSize}), Req}
            end;
        false -> {error, ?RESTU_MSG:get_error_tuple({not_enough_disk_space}), Req}
    end.

%% @private
do_receive_multipart(Req, Params, FileNameList) ->
    case cowboy_req:read_part(Req) of
        {ok, Headers, Req1} ->
            % CASE
            {Req2, NewFileNameList} = do_receive_one_part(Req1, Params, Headers, FileNameList),
            receive_multipart(Req2, Params, NewFileNameList); % mutual recursion
        {done, Req3} -> {ok, Req3, FileNameList}
    end.

%% @private
do_receive_one_part(Req, Params, Headers, FileNameList) ->
    SingleFile = maps:get(single_file, Params, false),
    case cow_multipart:form_data(Headers) of
        {data, FieldName} -> receive_file(Params, Req, FieldName, octet, FileNameList); % it is a strange case to save field value to a file..
        {file, FieldName, FileName, _CType} when SingleFile==false -> receive_file(Params, Req, FieldName, FileName, FileNameList);
        {file, FieldName, _FileName, _CType} when SingleFile==true -> receive_file(Params, Req, FieldName, octet, FileNameList);
        % support older cowlib versions that still passed Content-Transfer-Encoding header
        {file, FieldName, FileName, _CType, _CTE} when SingleFile==false -> receive_file(Params, Req, FieldName, FileName, FileNameList);
        {file, FieldName, _FileName, _CType, _CTE} when SingleFile==true -> receive_file(Params, Req, FieldName, octet, FileNameList)
    end.

%% -----------------------------------------------
%% Receive one file by Params=#{path and maxcontentlength} from Req and return Acc proplist with appended [{FieldName,FileName,Status}].
%% -----------------------------------------------
%-spec receive_file(Params::map(), Req, FieldName::binary(), FileName::binary()|octet, Acc) -> {Req, Acc}
%      when Req::cowboy_req:req(), Acc::[{file,binary(),ok|{error,Reason::binary()}}], Err::
%% -----------------------------------------------
%receive_file(_Params, Req, _FieldName, _FileName, {error,_}=Err) -> {Req, Err}; % stop after first error and return it
receive_file(Params, Req, FieldName, <<>>, Acc) -> receive_unnamed_file(Params, Req, FieldName, <<>>, Acc);
receive_file(Params, Req, FieldName, FileName, Acc) ->
    case receive_file_upload(Params, Req, FileName) of
        {ok, NewFilePath, _FileSize, Req1} ->
            NewFileNameList = lists:append(Acc, [{file, NewFilePath, ok}]),
            {Req1, NewFileNameList};
        {limit, NewFilePath, Reason, Req2} ->
            ?LOG('$warning', "Upload limit detected! Type: ~p; FieldName: '~ts'; Filename: '~ts'; Req: ~p", [Reason, FieldName, FileName, Req2]),
            ?BLfile:delete(NewFilePath), % ignore delete errors
            NewFileNameList = lists:append(Acc, [{file, NewFilePath, {error,413,<<"File size limit reached!">>}}]),
            {Req2, NewFileNameList};
        {error, NewFilePath, Code, ReasonT} ->
            ?LOG('$error', "Upload file op error! FileName: '~ts'; error code ~p reason: ~120tp", [NewFilePath, Code, ReasonT]),
            ?BLfile:delete(NewFilePath), % ignore delete errors
            NewFileNameList = lists:append(Acc, [{file, NewFilePath, {error,Code,ReasonT}}]),
            {Req, NewFileNameList}
    end.

receive_unnamed_file(_Opts, Req, FieldName, <<>>, Acc) ->
    ?LOG('$info', "Empty file name -> ignored, FieldName ~p", [FieldName]),
    NewFileNameList = lists:append(Acc, [{file, FieldName, {error, <<"Empty file name!">>}}]),
    {Req, NewFileNameList}.

%% -----------------------------------------------
%% Receive body stream and save it under specified filename.
%%   returns tuple {ok | limit, NewFilePath, FileSize, Request} | {error, Reason}
%% -----------------------------------------------
%-spec receive_file_upload(Params::map(), Req, FileName::binary()|octet) ->
%% -----------------------------------------------
receive_file_upload(Params, Req, FileName) ->
    Path = ?FILEPATHS:get_local_path(maps:get(path, Params)),
    MaxFileSizeLimit = maps:get(maxcontentlength, Params),
    NewFilePath = make_ensure_file_path(Path, FileName),
    try ?BLfile:open(NewFilePath, [raw, write]) of
        {ok, IoDevice} ->
            try
                case stream_file(Req, IoDevice, 0, MaxFileSizeLimit) of
                    {ok, FileSize, Req4} -> {ok, NewFilePath, FileSize, Req4};
                    {limit, Reason, Req4} -> {limit, NewFilePath, Reason, Req4};
                    {error, Code, Reason} -> {error, NewFilePath, Code, Reason}
                end
            after
                ?BLfile:close(IoDevice)
            end;
        {error, IOReason} ->
            {error, NewFilePath, 500, ?RESTU_MSG:get_error_tuple({file_open,IOReason})}
    catch
        C:E:StackTrace ->
            ?LOG('$crash', "Catched ~p:~p:~nStacktrace: ~tp", [C, E, StackTrace]),
            {error, NewFilePath, 500, ?RESTU_MSG:get_error_tuple({'catch',C,E})}
    end.

%% @private
%% This first part implements single-file endpoints when Path is the final path including filename.
%% It splits the path into containing folder and filename, then running the ensure part.
make_ensure_file_path(Path, octet) ->
    Name = filename:basename(Path),
    Dir = filename:dirname(Path),
    make_ensure_file_path(Dir, Name);
%% This second part implements creating containing folder and returns complete path with filename.
make_ensure_file_path(Path, FileName) ->
    ?BLfilelib:ensure_dir(Path),
    ?BLfile:make_dir(Path),
    filename:join(Path, FileName).

%% @private
%% returns tuple {ok, NewFileSize, Request} | {limit, file_size, Request} | {error, Code::integer(), Reason::binary()}.
stream_file(Req, IoDevice, FileSize, MaxFileSizeLimit) ->
    stream_file(Req, IoDevice, FileSize, MaxFileSizeLimit, 0). % #419
stream_file(Req, IoDevice, FileSize, MaxFileSizeLimit, Cnt) ->
    {OkMore, Data, Req2} = stream_part_body(Req),
    case write_to_file(IoDevice, Data, MaxFileSizeLimit, FileSize) of
        {ok, NewFileSize} ->
            case OkMore of
                ok -> {ok, NewFileSize, Req2};
                more ->
                    NewCnt = case NewFileSize==FileSize of true -> timer:sleep(20), Cnt+1; false -> 0 end, %% #419
                    case NewCnt > 250 of
                        true -> {error, 408, ?RESTU_MSG:get_error_tuple({upload_timeout})};
                        false -> stream_file(Req2, IoDevice, NewFileSize, MaxFileSizeLimit, NewCnt)
                    end
            end;
        {{error,ReasonAtom}, _NewFileSize} ->
            {error, 500, ?RESTU_MSG:get_error_tuple({file_write,ReasonAtom})};
        {limit, file_size} -> {limit, file_size, Req2}
    end.

%% @private
stream_part_body(Req) ->
    Opts = #{length => 1048576},
    case cowboy_req:parse_header(<<"content-type">>, Req) of
        {<<"multipart">>, <<"form-data">>, _} -> cowboy_req:read_part_body(Req, Opts);
        _ -> cowboy_req:read_body(Req, Opts)
    end.

%% @private
write_to_file(IoDevice, Data, MaxFileSizeLimit, FileSize) ->
    NewFileSize = erlang:byte_size(Data) + FileSize,
    case NewFileSize > MaxFileSizeLimit of
        true -> {limit, file_size};
        false -> {?BLfile:write(IoDevice, Data), NewFileSize}
    end.

%% -----------------------------------------------
send_file(Path, Req) when is_list(Path); is_binary(Path) ->
    [BFileName|_] = lists:reverse(binary:split(?BU:to_binary(Path), <<"/">>, [global])),
    send_file({Path, BFileName}, Req);
send_file({Path, Filename}, Req) when (is_list(Path) orelse is_binary(Path)) andalso is_list(Filename) ->
    send_file({Path, ?BU:to_binary(Filename)}, Req);
send_file({Path, BFileName0}, Req) when (is_list(Path) orelse is_binary(Path)) andalso is_binary(BFileName0) ->
    Req0 = allow_origin(Req),
    Req1 = cowboy_req:set_resp_header(<<"server">>, ?ServerName, Req0), % Rostell
    Req4 = set_nocache_headers(Req1),
    FileSize = ?BLfilelib:file_size(Path),
    Req5 = cowboy_req:set_resp_body({sendfile, 0, FileSize, Path}, Req4),
    cowboy_req:reply(200, Req5),
    {ok, Req5}.

%% @private
allow_origin(Req) ->
    Host = cowboy_req:header(<<"host">>, Req, <<>>),
    Origin = cowboy_req:header(<<"origin">>, Req, Host),
    case lists:last(binary:split(Origin, <<"://">>, [])) of
        Host -> Req;
        _ ->
            Req1 = cowboy_req:set_resp_header(<<"access-control-allow-origin">>, Origin, Req),
            _Req2 = cowboy_req:set_resp_header(<<"access-control-allow-credentials">>, <<"true">>, Req1)
    end.

%% @private RP-194
set_nocache_headers(Req) ->
    NoCacheHeaders = [{<<"cache-control">>,<<"no-cache, no-store, must-revalidate">>},
                      {<<"pragma">>,<<"no-cache">>},
                      {<<"expires">>,<<"0">>}],
    F = fun({HeaderName,HeaderVal},ReqX) ->
                case cowboy_req:has_resp_header(HeaderName,Req) of
                    true -> ReqX;
                    false -> cowboy_req:set_resp_header(HeaderName,HeaderVal,ReqX)
                end end,
    lists:foldl(F,Req,NoCacheHeaders).

%% -----------------------------------------------
%% check if free disk space is larger than 3GB
check_diskspace() ->
    D = ?FILEPATHS:get_node_temp_path(), % XXX only in 1 case uploads go to that folder; lots of sync_path cases.
    case ?BU:get_disk_space(D) of
        undefined -> true;
        {_,Cap,Perc} -> Cap - (Cap div 100 * Perc) > 500000 % 500 MB
    end.

%% -----------------------------------------------
%% check request content length does not exceed limit.
check_content_length(Req, Params) ->
    case cowboy_req:header(<<"content-length">>, Req) of
        undefined -> true;
        CLen ->
            MaxContentSize = maps:get(maxcontentlength, Params),
            case ?BU:to_int(CLen) > MaxContentSize of
                true -> {false, MaxContentSize};
                false -> true
            end end.

