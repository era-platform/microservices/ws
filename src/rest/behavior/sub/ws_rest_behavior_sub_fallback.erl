%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.06.2021
%%% @doc

-module(ws_rest_behavior_sub_fallback).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([init/2]).

-export([convert_fallback_verb_req_var/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("rest.hrl").

%% ====================================================================
%% Fallback handler
%% ====================================================================

init(Req0, Opts) ->
    Req = ?HTTPU:allow_origin(Req0, undefined),
    case cowboy_req:method(Req) of
        <<"POST">> ->
            case convert_fallback_verb_req(Req, Opts) of
                {ok, Req1, Opts1} ->
                    case lists:keyfind(actual_handler, 1, Opts1) of
                        {_,Handler} ->
                            Handler:init(Req1, Opts1);
                        false ->
                            not_found(Req, nullstate)
                    end;
                not_found ->
                    not_found(Req, nullstate)
            end;
        <<"OPTIONS">> -> {?H_GEN, Req, Opts};
        _Method -> method_not_allowed(Req, nullstate)
    end.

%% @private
convert_fallback_verb_req(Req, Opts) ->
    case get_fallback_verb_path(Req) of
        not_found -> not_found;
        {Method, Path} ->
            HasBody = cowboy_req:has_body(Req),
            CowboyMethod = to_known_safe_cowboy_method(Method, HasBody),
            {ok, ?COWBU:update_request([{method, CowboyMethod},
                                        {path, Path}], Req), [{method,Method}|Opts]}
    end.

%% @private
get_fallback_verb_path(Req) ->
    Path = cowboy_req:path(Req),
    %PathInfo = cowboy_req:path_info(Req), % must be empty
    Parts = binary:split(Path, <<"/">>, [global]),
    [LastPart|RevParts] = lists:reverse(Parts),
    % retrieve custom method (if specified)
    case split_part_method(LastPart) of
        not_found -> not_found;
        {Method, PartPrefix} ->
            CParts = lists:reverse([PartPrefix | RevParts]),
            CPath = ?BU:join_binary(CParts, <<"/">>),
            {Method, CPath}
    end.

%% @private
to_known_safe_cowboy_method(NewMethod, HasBody) ->
    case ?RESTU:to_known_cowboy_method(NewMethod, HasBody) of
        M when (M == <<"GET">> orelse M == <<"HEAD">>) andalso HasBody ->
            % make sure the body is handled by cowboy_rest if it exists in request,
            <<"POST">>; % even in case of !get or !head (because the real method is POST).
        CowboyMethod -> CowboyMethod
    end.

%% ==================================
%% Sending response
%% ==================================

not_found(Req, State) ->
    Req1 = cowboy_req:reply(404, [], <<>>, Req),
    {ok, Req1, State}.

%% ----------------------------------

method_not_allowed(Req, State) ->
    Req1 = cowboy_req:reply(405, [], <<>>, Req),
    {ok, Req1, State}.

%% ====================================================================
%% Fallback utility
%% ====================================================================

%% ----------------------------------
%% Return converted req with custom method taken from binding or path_info, and remove that suffix from both binding/path_info and path.
%% Also, if neither binding nor path_info is found, convert custom method to POST in Req and save the original method in State.
%% When nothing is changed, return atom 'not_found'.
%% ----------------------------------
-spec convert_fallback_verb_req_var(Req, State) -> not_found | {ok, Req, State}
    when Req::cowboy_req:req(), State::map().
%% ----------------------------------
convert_fallback_verb_req_var(Req, State) ->
    case maps:get(binding_method, State, undefined) of
        undefined ->
            case maps:get(pathinfo_method, State, undefined) of
                undefined ->
                    Method = cowboy_req:method(Req),
                    HasBody = cowboy_req:has_body(Req),
                    case ?RESTU:to_known_cowboy_method(Method, HasBody) of
                        Same when Same =:= Method -> not_found;
                        CowboyMethod ->
                            {ok, ?COWBU:update_request([{method, CowboyMethod}], Req), State#{method => Method}}
                    end;
                _ -> convert_fallback_verb_req_pathinfo(Req, State)
            end;
        Binding -> convert_fallback_verb_req_binding(Req, Binding, State)
    end.

%% @private
convert_fallback_verb_req_binding(Req, Binding, State) ->
    case cowboy_req:binding(Binding, Req) of
        undefined -> not_found;
        Value ->
            case split_part_method(Value) of
                not_found -> not_found;
                {Method, BindingPrefix} ->
                    Bindings = cowboy_req:bindings(Req),
                    TruncBindings = lists:keyreplace(Binding, 1, Bindings, {Binding, BindingPrefix}),
                    Path = cowboy_req:path(Req),
                    TruncPath = binary:part(Path, 0, size(Path)-size(Method)-1),
                    HasBody = cowboy_req:has_body(Req),
                    CowboyMethod = ?RESTU:to_known_cowboy_method(Method, HasBody),
                    {ok, ?COWBU:update_request([{method, CowboyMethod},
                                                {bindings, TruncBindings},
                                                {path, TruncPath}], Req), State#{method => Method}}
            end end.

%% @private
convert_fallback_verb_req_pathinfo(Req, State) ->
    case cowboy_req:path_info(Req) of
        undefined -> not_found;
        [] -> not_found;
        Parts ->
            [LastPart|RevParts] = lists:reverse(Parts),
            case split_part_method(LastPart) of
                not_found -> not_found;
                {Method, PartPrefix} ->
                    CParts = lists:reverse([PartPrefix | RevParts]),
                    Path = cowboy_req:path(Req),
                    TruncPath = binary:part(Path, 0, size(Path)-size(Method)-1),
                    HasBody = cowboy_req:has_body(Req),
                    CowboyMethod = ?RESTU:to_known_cowboy_method(Method, HasBody),
                    {ok, ?COWBU:update_request([{method, CowboyMethod},
                                                {path_info, CParts},
                                                {path, TruncPath}], Req), State#{method => Method}}
            end end.

%% ====================================================================
%% Internal functions
%% ====================================================================

split_part_method(Part) ->
    case binary:matches(Part, ?FB_METHOD_SEP) of
        [] -> not_found;
        Matches ->
            {LastMatchStart,_} = lists:last(Matches),
            case binary:part(Part, LastMatchStart+1, size(Part)-LastMatchStart-1) of
                <<>> -> not_found;
                Method ->
                    PartPrefix = binary:part(Part, 0, LastMatchStart),
                    {?BU:to_upper(Method), PartPrefix}
            end end.
