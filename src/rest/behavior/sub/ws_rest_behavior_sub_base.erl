%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.06.2021
%%% @doc

-module(ws_rest_behavior_sub_base).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([upgrade/5]). % helper

% common implementations for different endpoint types.

-export([
    known_methods/2,
    allowed_methods/2,
    is_authorized/2,
    forbidden/2,
    valid_entity_length/3,
    content_types_accepted/2,
    content_types_provided/2,
    charsets_provided/2,
    resource_exists/2,
    %previously_existed/2,
    last_modified/2%,
    %expires/2,
    %moved_permanently/2,
    %moved_temporarily/2,
    %delete_resource/2,
    %delete_completed/2
]).

-export([content_types_accepted/3]). % helper

-export([
    accept_json/2,
    accept_form/2,
    accept_multipart/2,
    accept_octet/2
]).

-export([read_body_stream/2]).

-export([
    declared_methods/2,
    allowed_methods_auto/1
]).

%% ====================================================================
%% Define
%% ====================================================================

-define(MAX_CONTENT_LENGTH_DEFAULT, 1024*1024*2). % 2 Mb

-include("app.hrl").
-include("rest.hrl").

%% ====================================================================
%% Utility functions for specific REST handlers implementations
%% ====================================================================

%% ----------------------------------
%% Upgrade to sub-protocol (common implementation, helper)
%% ----------------------------------
-spec upgrade(module(), Req, Env, Handler::module(), HandlerState::list()) % timeout(), run|hibernate
      -> {ok, Req, Env} when Req::cowboy_req:req(), Env::cowboy_middleware:env().
%% ----------------------------------
upgrade(Module, Req0, Env, Handler, HandlerState) when is_map(Env), is_list(HandlerState) ->
    Req = ?HTTPU:allow_origin(Req0, undefined),
    case ?BAN:is_banned(element(1,cowboy_req:peer(Req))) of
        true -> extend_ban_reply_forbidden(Req, Env);
        false -> do_upgrade(Module, Req, Env, Handler, HandlerState)
    end.

%% @private
do_upgrade(Module, Req, Env, Handler, HandlerState) when is_map(Env), is_list(HandlerState) ->
    % accept only proplists handler Opts, convert them to map and use as initial request state.
    State = maps:from_list(proplists:unfold(HandlerState)),
    State1 = State#{ endpoint_handler => Handler, behavior_handler => Module },
    % tell cowboy to use the provided behavior handler module for rest callbacks.
    UpEnv = Env#{handler := Module},
    % convert fallback URL to canonical URL + custom method.
    {Req2, State2} = case ?H_FALLBACK:convert_fallback_verb_req_var(Req, State1) of
                         not_found -> {Req, State1};
                         {ok, ReqC, StateC} -> {ReqC, StateC}
                     end,
    % replace incoming custom method with POST so cowboy_rest can do the right job for us
    {Req3, State3} = case convert_http_verb(Req2, State2) of
                         not_found -> {Req2, State2};
                         {ok, ReqC2, StateC2} -> {ReqC2, StateC2}
                     end,
    % re-use the cowboy_rest implementation of a RESTful request handler
    %  and implement a number of behavior handlers with help of its callbacks.
    cowboy_rest:upgrade(Req3, UpEnv, Module, State3).

%% @private
convert_http_verb(Req, State) ->
    Method = cowboy_req:method(Req),
    HasBody = cowboy_req:has_body(Req),
    case ?RESTU:to_known_cowboy_method(Method, HasBody) of
        M when M==Method -> not_found;
        CowboyMethod -> {ok, ?COWBU:update_request([{method, CowboyMethod}], Req), State#{method => Method}}
    end.

%% @private
extend_ban_reply_forbidden(Req, Env) ->
    IpAddr = element(1,cowboy_req:peer(Req)),
    ?BAN:request_failed_with(IpAddr, IpAddr), % extend ban, with Data==IpAddr (will create one more entry in banned state)
    % .. because in banned state we don't want to spend any more time figuring out SessionId or whatever.
    timer:sleep(1000+rand:uniform(1000)), % anti brute-force pause
    {stop, Req2, nullstate} = ?RESTU:send_error(403, access_denied, Req, nullstate),
    {ok, Req2, Env}.

%% ----------------------------------
%% Return list of known_methods to cowboy, consulting behavior handler allowed_methods() callback and
%%  merging it with default known methods. It is required to support custom methods (like LOOKUP)
%%  when they are included in allowed_methods.
%% ----------------------------------
-spec known_methods(Req, State) -> {Value, Req, State}
    when Req::cowboy_req:req(), State::map(), Value::[binary()].
%% ----------------------------------
known_methods(Req, State) ->
    {AM, Req1, State1} = ?RESTU:callb(Req, State, allowed_methods),
    KMdef = ordsets:from_list([<<"HEAD">>,<<"GET">>,<<"POST">>,<<"PUT">>,<<"PATCH">>,<<"DELETE">>,<<"LOOKUP">>,<<"CLEAR">>,<<"OPTIONS">>]),
    KnownMethods = ordsets:to_list(ordsets:union(ordsets:from_list(AM),KMdef)),
    {KnownMethods, Req1, State1}.

%% ----------------------------------
%% Return list of allowed_methods to cowboy, consulting endpoint handler allowed_methods() callback and
%%  adding always-supported OPTIONS method, as well as HEAD if GET is in the list, and
%%  adding POST when any non-basic method is in the list (basic are GET HEAD OPTIONS POST).
%% ----------------------------------
-spec allowed_methods(Req, State) -> {Value, Req, State}
    when Req::cowboy_req:req(), State::map(), Value::[binary()].
%% ----------------------------------
allowed_methods(Req, State) -> % this utility function is called by some behavior_handlers (atm. generic).
    {AM, Req2, State2} = declared_methods(Req, State),
    AMA = allowed_methods_auto(AM),
    {AMA, Req2, State2}.

%% ----------------------------------
%% Return list of allowed_methods as provided by endpoint handler module, without adding anything.
%% ----------------------------------
-spec declared_methods(Req, State) -> {Value, Req, State}
    when Req::cowboy_req:req(), State::map(), Value::[binary()].
%% ----------------------------------
declared_methods(Req, State) ->
    F = fun(ReqF, StateF) -> {allowed_methods_default(), ReqF, StateF} end,
    ?RESTU:call(Req, State, allowed_methods, F).

%% @private
allowed_methods_default() -> [<<"GET">>,<<"HEAD">>,<<"OPTIONS">>].

%% ----------------------------------
%% Return list of allowed methods extended with automatic methods:
%% - OPTIONS is always added;
%% - HEAD is added when GET is present;
%% - POST is added when custom methods are present (except basic methods: GET HEAD OPTIONS POST)
%% ----------------------------------
-spec allowed_methods_auto(AM) -> AM when AM::[binary()].
%% ----------------------------------
allowed_methods_auto(AM) ->
    AMO = case lists:member(<<"OPTIONS">>, AM) of
              true -> AM;
              false -> AM ++ [<<"OPTIONS">>]
          end,
    AMH = case {lists:member(<<"GET">>, AMO), lists:member(<<"HEAD">>, AMO)}  of
              {true, true} -> AMO;
              {true, false} -> AMO ++ [<<"HEAD">>];
              {false, _} -> AMO
          end,
    AMP = case AMH -- ?BASIC_METHODS of
              [] -> AMH;
              [_|_] -> % custom methods are substituted with POST in upgrade() so it must be allowed
                  case lists:member(<<"POST">>, AMH) of
                      true -> AMH;
                      false -> AMH ++ [<<"POST">>]
                  end
          end,
    AMP.

%% ----------------------------------
%% Authenticate request (fill State#{iam} by http session cookie) and
%%  in case of no session check for anonymous access to request URL (return 401 when not allowed).
%% ----------------------------------
-spec is_authorized(Req, State) -> {true|{false,WWWAuthenticate::binary()}, Req, State}
    when Req::cowboy_req:req(), State::map().
%% ----------------------------------
is_authorized(Req, State) ->
    case cowboy_req:method(Req) of
        <<"OPTIONS">> -> {true, Req, State};
        _ -> is_authorized1(Req, State)
    end.

%% @private
is_authorized1(Req0, State0) ->
    {Req, State} = ?RESTU_IAM:authenticate(Req0, State0), % can add {IpAddr,SessionId} to ban list attempts if cookie is set but session does not exist.
    IsAuthen = case maps:get(iam, State, undefined) of
                   A when is_map(A) -> true; % either session with user roles or token without roles
                   _ -> false
               end,
    case IsAuthen of
        true -> {true, Req, State}; % let it through now (don't fail with 401 here yet)
        false ->
            case ?RESTU_IAM:check_access(Req, State) of
                ok -> {true, Req, State}; % URL allows for anonymous access - let it through
                {error,_} ->
                    {{false, ?BU:strbin("Bearer realm=\"Era.API\"",[])}, Req, State}
            end end.

%% ----------------------------------
%% Authorize access to request URL using IAM service.
%% ----------------------------------
-spec forbidden(Req, State) -> {boolean(), Req, State}
    when Req::cowboy_req:req(), State::map().
%% ----------------------------------
forbidden(Req, State) ->
    case ?RESTU_IAM:check_access(Req, State) of
        ok ->
            {false, Req, State}; % not forbidden
        % send some Reason content instead of empty body (if {true,Req,State} was returned (in is_authorized())).
        {error,{Code,Reason}} when is_binary(Reason) ->
            ?RESTU:send_error({Code,Reason}, Req, State);
        {error,Reason} ->
            ?RESTU:send_error(403, {access_denied,?BU:strbin("IAM: ~120tp",[Reason])}, Req, State)
    end.

%% ----------------------------------

valid_entity_length(Req, State, MaxContentLength) when is_integer(MaxContentLength) ->
    F = fun(Req1, State1) ->
                ContentLength = ?BU:to_int(cowboy_req:header(<<"content-length">>, Req1, <<"0">>)),
                {ContentLength =< MaxContentLength, Req1, State1}
        end,
    ?RESTU:call(Req, State, valid_entity_length, F).

%% ----------------------------------

%% Return list of accepted content-types and their respective handler functions.
%% Only specific known content-types are handled.
%% No callback function is provided for endpoint module because AcceptCallback is always called in behavior module.
content_types_accepted(Req, State) ->
    Method = ?RESTU:method(Req, State),
    CTAM = content_types_accepted(Method),
    Handler = maps:get(behavior_handler, State),
    CTAMF = lists:filter(fun({_,CB}) -> ?BU:function_exported(Handler,CB,2) end, CTAM),
    {CTAMF, Req, State}.

content_types_accepted(Method) when Method==<<"GET">>; Method==<<"HEAD">>; Method==<<"OPTIONS">> -> []; % no content is accepted

content_types_accepted(Method) when Method==<<"POST">> ->
    [get_cta_json(), get_cta_form(), get_cta_multipart(), get_cta_octet(), get_cta_any()];

content_types_accepted(Method) when Method==<<"PATCH">>; Method==<<"DELETE">> ->
    [get_cta_json(), get_cta_form()];

content_types_accepted(Method) when Method==<<"PUT">> ->
    [get_cta_json(), get_cta_multipart(), get_cta_octet(), get_cta_any()];

content_types_accepted(_Method) ->
    [get_cta_json(), get_cta_form(), get_cta_multipart(), get_cta_octet(), get_cta_any()]. % custom methods can accept any supported content-type.
% If custom methods need ability to only accept specific content types,
% it is possible to add new callback in behavior that would somehow limit content_types_accepted selection.

get_cta_json() -> {{<<"application">>,<<"json">>,'*'}, accept_json}.
get_cta_form() -> {{<<"application">>,<<"x-www-form-urlencoded">>,'*'}, accept_form}.
get_cta_multipart() -> {{<<"multipart">>,<<"form-data">>,'*'}, accept_multipart}.
get_cta_octet() -> {{<<"application">>,<<"octet-stream">>,'*'}, accept_octet}. % direct put of content as-is without multipart
get_cta_any() -> {'*', accept_any}.

%% ----------------------------------
%% Helper function to additionally limit/filter the accepted content-types list.
%% ----------------------------------
content_types_accepted(Req, State, Accepted) ->
    {CTA,Req1,State1} = content_types_accepted(Req, State),
    CTAM = lists:filter(fun({_,FunAtom}) -> lists:member(FunAtom,Accepted) end, CTA),
    {CTAM,Req1,State1}.

%% ----------------------------------

accept_json(Req, State) ->
    case read_body_stream(Req, ?REST_MAX_CONTENT_LENGTH_JSON) of
        {ok, Body, Req1} -> decode_json_data(Body, <<"body">>, Body, Req1, State);
        {error,Reason,Req2} -> ?RESTU:send_error(Reason, Req2, State)
    end.

read_body_stream(Req, MaxContentLength) -> read_x_body_stream(Req, fun cowboy_req:read_body/1, MaxContentLength).

read_x_body_stream(Req, ReadBodyFun, MaxContentLength) ->
    case check_content_length(Req) of
        true -> read_x_body_stream(Req, ReadBodyFun, [], 0, MaxContentLength);
        {false,MaxSize} ->
            {error, ?RESTU_MSG:get_error_tuple({limited,MaxSize}), Req}
    end.

read_x_body_stream(Req, ReadBodyFun, Received, ReceivedBytes, MaxContentLength) when is_integer(MaxContentLength) ->
    case ReadBodyFun(Req) of
        {ok, Body, Req1} ->
            Complete = erlang:iolist_to_binary([Received,Body]),
            {ok, Complete, Req1};
        {more, Data, Req2} ->
            NewReceivedBytes = ReceivedBytes + length(Data),
            MaxSize = MaxContentLength,
            case NewReceivedBytes < MaxSize of
                true -> read_x_body_stream(Req2, ReadBodyFun, [Received,Data], NewReceivedBytes, MaxContentLength);
                false -> {error, ?RESTU_MSG:get_error_tuple({limited,MaxSize}), Req2}
            end
    end.

decode_json_data(Data, DataName, FullBody, Req, State) ->
    case catch jsx:decode(Data, [return_maps]) of
        Map when is_map(Map) ->
            State1 = State#{ input_type => object, input_map => Map, input => jsx:decode(Data) },
            State2 = maybe_save_body_in_state(State1, FullBody),
            {true, Req, State2};
        List when is_list(List) ->
            State1 = State#{ input_type => array, input_array => List, input => jsx:decode(Data) },
            State2 = maybe_save_body_in_state(State1, FullBody),
            {true, Req, State2};
        String when is_binary(String) ->
            State1 = State#{ input_type => string, input_string => String },
            State2 = maybe_save_body_in_state(State1, FullBody),
            {true, Req, State2};
        Num when is_number(Num) ->
            State1 = State#{ input_type => number, input_number => Num },
            State2 = maybe_save_body_in_state(State1, FullBody),
            {true, Req, State2};
        Bool when is_boolean(Bool) ->
            State1 = State#{ input_type => boolean, input_boolean => Bool },
            State2 = maybe_save_body_in_state(State1, FullBody),
            {true, Req, State2};
        {'EXIT',_} ->
            ?RESTU:send_error(400, {invalid_request,<<"invalid json in ",DataName/binary>>}, Req, State);
        _ ->
            ?RESTU:send_error(422, {invalid_params,<<"json object or string is expected in ",DataName/binary>>}, Req, State)
    end.

%% ----------------------------------

accept_form(Req, State) ->
    case read_body_stream(Req, ?REST_MAX_CONTENT_LENGTH_JSON) of
        {ok, Body, Req1} ->
            Vars = urldecode(Body),
            State1 = State#{ form => Vars },
            case lists:keyfind(<<"data">>, 1, Vars) of
                false ->
                    State2 = maybe_save_body_in_state(State1, Body),
                    {true, Req1, State2};
                {_,Data} -> decode_json_data(Data, <<"'data' form field">>, Body, Req1, State1)
            end;
        {error,Reason,Req2} ->
            ?RESTU:send_error(Reason, Req2, State)
    end.

urldecode(Body) when is_binary(Body) ->
    Parts = binary:split(Body, <<"&">>, [global]),
    lists:foldr(fun(Part, Acc) ->
                        case binary:split(Part, <<"=">>) of
                            [_] -> [{Part,<<>>} | Acc];
                            [K,V] -> [{K,?BU:urldecode(V)} | Acc]
                        end end, [], Parts).

%% ----------------------------------

accept_multipart(Req, State) ->
    case check_content_length(Req) of
        true ->
            case read_body_parts(Req, ?REST_MAX_CONTENT_LENGTH_FILE) of
                {ok, Parts, Req1} ->
                    State1 = State#{ body_parts => Parts },
                    {true, Req1, State1};
                {error, Reason, Req1} ->
                    ?RESTU:send_error(Reason, Req1, State)
            end;
        {false,MaxSize} ->
            ?RESTU:send_error(413, ?RESTU_MSG:get_error_tuple({limited,MaxSize}), Req, State)
    end.

read_body_parts(Req, MaxContentLength) ->
    read_body_parts(Req, [], MaxContentLength).

read_body_parts(Req, AccParts, MaxContentLength) ->
    case cowboy_req:read_part(Req) of
        {ok, _Headers, Req1} ->
            case read_part_body_stream(Req1, MaxContentLength) of
                {ok, Body, Req2} -> read_body_parts(Req2, [Body|AccParts], MaxContentLength); % recurse
                {error,_,_}=Err -> Err
            end;
        {done, Req4} ->
            {ok, lists:reverse(AccParts), Req4}
    end.

%% @private
read_part_body_stream(Req, MaxContentLength) ->
    read_x_body_stream(Req, fun cowboy_req:read_part_body/1, MaxContentLength).

%% ----------------------------------

accept_octet(Req, State) ->
    case cowboy_req:read_body(Req) of
        {ok, Body, Req1} ->
            State1 = State#{ body => Body }, % always save body (whether asked or not) because it's not decoded.
            {true, Req1, State1};
        {more, _Data, Req3} ->
            Resp = <<"accepting overlong octet-stream body is not implemented.. file uploads are done elsewhere.">>,
            Req4 = cowboy_req:set_resp_body(Resp, Req3),
            {false, Req4, State}
    end.

%% ==================================

content_types_provided(Req, State) ->
    CTP = [{{<<"application">>,<<"json">>,'*'}, provide_json}],
    {CTP, Req, State}.

charsets_provided(Req, State) ->
    CSP = [<<"utf-8">>],
    {CSP, Req, State}.

%% ----------------------------------

resource_exists(Req, State) ->
    ?RESTU:call(Req, State, resource_exists, true).

%% ----------------------------------

%previously_existed(Req, State) ->
%    ?RESTU:call(Req, State, previously_existed, true).

%% ----------------------------------

last_modified(Req, State) ->
    ?RESTU:call(Req, State, last_modified, undefined).

%% ----------------------------------

%expires(Req, State) ->
%    ?RESTU:call(Req, State, expires, undefined).

%% ----------------------------------

%moved_permanently(Req, State) ->
%    ?RESTU:call(Req, State, moved_permanently, true).

%% ----------------------------------

%moved_temporarily(Req, State) ->
%    ?RESTU:call(Req, State, moved_temporarily, true).

%% --------------------------------------------------------------------
%% DELETE
%% H:delete_resource/2 -> false => 500 Internal Server Error (request is good, but delete failed)
%% H:delete_resource/2 -> true => 204 No Content
%% H:delete_resource/2 -> map() => 200 OK + json
%% --------------------------------------------------------------------

%delete_resource(Req, State) ->
%    ?RESTU:call(Req, State, delete_resource, true).

%delete_completed(Req, State) ->
%    ?RESTU:call(Req, State, delete_completed, true). % return 204 or 200 (not 202) by default

%% ====================================================================
%% Internal functions
%% ====================================================================

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% % start - all methods --------
%%
%% service_available() -> 503 service not available.           % LATER
%% not known_methods() -> 501 method not implemented.          % not needed
%% not uri_too_long() -> 414 request URI too long.             % not needed
%% allowed_methods() -> 405 methods not allowed.               % IMPLEMENTED
%% not malformed_request() -> 400 bad request.                 % not needed
%% is_authorized() -> 401 unauthorized www-authenticate.       % IMPLEMENTED
%% forbidden() -> 403 forbidden.                               % IMPLEMENTED
%% not valid_content_headers() -> 501 (what?) not implemented. % not needed
%% valid_entity_length() -> 413 request entity too large.      % IMPLEMENTED
%%
%% % OPTIONS (after start) --------
%%
%% options() -> 200 OK.                                        % auto
%%
%% % content negitiation (after start) - all methods but OPTIONS. --------
%%
%% content_types_provided() -> (400 bad request); 406 not acceptable.  % IMPLEMENTED
%% languages_provided() -> (400 bad request); 406 not acceptable.      % not needed
%% charsets_provided() -> (400 bad request); 406 not acceptable.       % IMPLEMENTED
%%
%% % GET, HEAD --------
%%
%% resource_exists() when has If-Match -> 412 precondition failed.     % IMPLEMENTED
%% last_modified() -> 304 Not Modified.                                % IMPLEMENTED
%% expires() -> ...                                                    % IMPLEMENTED
%% ProvideCallback() -> ...
%% previously_existed() -> 404 not found.                              % IMPLEMENTED
%% moved_permanently() -> 301 moved permanently.                       % IMPLEMENTED
%% moved_temporarily() -> 307 moved temporarily;                       % IMPLEMENTED
%%                        410 gone. % not in the scheme (maybe missing, it's REST!)
%%
%% % POST, PUT, PATCH --------
%%
%% resource_exists() when has If-Match -> 412 precondition failed.     % IMPLEMENTED
%% previously_existed() -> 404 not found.                              % IMPLEMENTED
%% moved_permanently() -> 301 moved permanently.                       % IMPLEMENTED
%% moved_temporarily() -> 307 moved temporarily;                       % IMPLEMENTED
%% allow_missing_post() -> 404 not found; % PUT                        % IMPLEMENTED in behavior, not needed in base.
%%                         410 gone. % POST
%% is_conflict() -> 409 conflict.                                      % not needed
%% content_types_accepted() -> AcceptCallbacks; 415 Unsupported Media Type.  % IMPLEMENTED
%% AcceptCallback() -> 400 bad request;
%%    new_resource? -> 204 no content;
%%                     201 created;
%%                     303 see other. % when {true, Location} is returned (and resource_exists() was true).
%% multiple_choices() -> 200 OK;                                       % not needed
%%                       300 multiple choices.
%%
%% % DELETE --------
%%
%% resource_exists() when has If-Match -> 412 precondition failed.     % IMPLEMENTED
%% delete_resource() -> 500 internal server error. % why 500 ? because nothing more specific in range 5xx (server side error) suits.  % IMPLEMENTED
%% delete_completed() -> 202 accepted. % a promise to delete the resource asynchronously.                                             % IMPLEMENTED
%%                       204 no content.
%% multiple_choices() -> 200 OK;                                       % not needed
%%                       300 multiple choices.
%% previously_existed() -> 404 not found.                              % IMPLEMENTED
%% moved_permanently() -> 301 moved permanently.                       % IMPLEMENTED
%% moved_temporarily() -> 307 moved temporarily;                       % IMPLEMENTED
%%                        410 gone. % not in the scheme (maybe missing, it's REST!)
%%
%% % conditional requests (after resource_exists() == true.) - all methods but OPTIONS. --------
%%
%% generate_etag() -> EntityTag.                                       % LATER
%% last_modified() -> 412 precondition failed.                         % IMPLEMENTED
%% expires() -> 304 not modified.                                      % IMPLEMENTED

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% -----------------------------------------------
%% check request content length does not exceed limit.
check_content_length(Req) ->
    case cowboy_req:header(<<"content-length">>, Req) of
        undefined -> true;
        CLen ->
            MaxContentSize = ?MAX_CONTENT_LENGTH_DEFAULT,
            case ?BU:to_int(CLen) > MaxContentSize of
                true -> {false, MaxContentSize};
                false -> true
            end
    end.

%% ----------------------------------
%% Save body in state if handler or url_route asked for it.
%% ----------------------------------
-spec maybe_save_body_in_state(State, Body) -> State when State::map(), Body::binary().
%% ----------------------------------
maybe_save_body_in_state(State, Body) ->
    case maps:get(save_body_in_state, State, false) of
        true -> State#{body => Body};
        _ -> State
    end.
