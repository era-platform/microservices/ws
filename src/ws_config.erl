%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 18.05.2021
%%% @doc Configuration functions

-module(ws_config).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    log_destination/1,
    http_port/0,
    https_port/0,
    http_iface/0,
    https_iface/0,
    temp_path/0,
    websocket_log_level/0,
    http_log_level/0
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% return log destination for loggerlib, ex. {app,prefix}
log_destination(Default) ->
    ?BU:get_env(?APP,'log_destination',Default).

%% return http_port
http_port() ->
    ?BU:get_env(?APP,'http_port',80).

https_port() ->
    ?BU:get_env(?APP,'https_port',443).

http_iface() ->
    ?BU:get_env(?APP,'http_iface',"0.0.0.0").

https_iface() ->
    ?BU:get_env(?APP,'https_iface',"0.0.0.0").

temp_path() ->
    ?BU:get_env(?APP,'temp_path',"/tmp").

websocket_log_level() ->
    ?BU:get_env(?APP,'websocket_log_level',0).

http_log_level() ->
    ?BU:get_env(?APP,'websocket_log_level',0).

%% ====================================================================
%% Internal functions
%% ====================================================================