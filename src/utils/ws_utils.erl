%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 26.06.2021
%%% @doc

-module(ws_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([has_iam/0,
         has_dc/0]).

-export([get_cookie/2,
         get_authorization/1]).

-export([set_cookie/4,
         erase_cookie/2]).

-export([find_session/2,
         renew_session/2,renew_session/1]).

-export([allow_origin/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ------------------------------
%% Return true if iam installed and users authentication should be used
%% ------------------------------
has_iam() ->
    ?PCFG:get_nodes_by_msvc(?MsvcIam) /= [].

%% ------------------------------
%% Return true if dc installed and domain authentication should be used
%% ------------------------------
has_dc() ->
    ?PCFG:get_nodes_by_msvc(?MsvcDc) /= [].

%% ====================================================================
%% Get headers functions
%% ====================================================================

%% -------------------------------------------------------
%% Return cookie value by name.
%% -------------------------------------------------------
-spec get_cookie(Name::binary(), Req::cowboy_req:req()) -> {ok,Cookie::binary()} | false.
%% -------------------------------------------------------
get_cookie(Name, Req) ->
    Cookies = cowboy_req:parse_cookies(Req),
    case lists:keyfind(Name, 1, Cookies) of
        false -> false;
        {_,Value} -> {ok,Value}
    end.

%% -------------------------------------------------------
%% Return parsed Authorization header
%% -------------------------------------------------------
-spec get_authorization(Req::cowboy_req:req()) -> {ok, {Type::binary() | undefined,Value::binary()}} | false.
%% -------------------------------------------------------
get_authorization(Req) ->
    case cowboy_req:header(<<"authorization">>, Req) of
        undefined -> false;
        Auth ->
            case binary:split(Auth, <<" ">>) of
                [Invalid] -> {ok,{undefined,Invalid}}; % without token type
                [TokenType,Token] -> {ok,{?BU:to_lower(TokenType),Token}}
            end end.

%% ====================================================================
%% Set-cookie functions
%% ====================================================================

%% -------------------------------------------------------
%% Set cookie to response
%% -------------------------------------------------------
-spec set_cookie(Name::binary(), Value::binary(), TTL::non_neg_integer(), Req::cowboy_req:req()) -> Req1::cowboy_req:req().
%% -------------------------------------------------------
set_cookie(Name,Value,TTL,Req) ->
    Opts = #{same_site => strict, path => "/", max_age => TTL}, % domain, http_only, secure
    cowboy_req:set_resp_cookie(Name,Value,Req,Opts).

%% -------------------------------------------------------
%% Set erase-cookie header into response
%% -------------------------------------------------------
-spec erase_cookie(Name::binary(), Req::cowboy_req:req()) -> Req1::cowboy_req:req().
%% -------------------------------------------------------
erase_cookie(Name,Req) ->
    Opts = #{same_site => strict, path => "/", max_age => 0}, % domain, http_only, secure
    cowboy_req:set_resp_cookie(Name,<<>>,Req,Opts).

%% ====================================================================
%% Session functions
%% ====================================================================

%% -------------------------------------------------------
%% Find session in domain
%% -------------------------------------------------------
-spec find_session(Domain::binary() | atom(), SessionId::binary())
      -> {ok,SessionItem::map()} | {wrong, Id::binary()} | false.
%% -------------------------------------------------------
find_session(Domain,SessionId) ->
    GlobalName = ?GN_NAMING:get_globalname(?MsvcType,Domain),
    case catch ?GLOBAL:gen_server_call(GlobalName,{'find_session',[SessionId]}) of
        {'EXIT',Reason} ->
            % TODO: try again
            ?LOG('$crash',"WS call crashed: ~120tp",[Reason]),
            false;
        false -> {wrong,SessionId};
        {ok,SessionItem} -> {ok,SessionItem}
    end.

%% -------------------------------------------------------
%% Check and renew session TTL
%% -------------------------------------------------------
-spec renew_session(SessionItem::map(), Req::cowboy_req:req()) -> Req1::cowboy_req:req().
%% -------------------------------------------------------
renew_session(SessionItem,Req) ->
    case renew_session(SessionItem) of
        false -> Req;
        true ->
            SessionId = maps:get(<<"id">>,SessionItem),
            Domain = maps:get(<<"domain">>,SessionItem),
            set_cookie(?CookieSession,?MakeToken(SessionId,Domain),?SessionTTL,Req)
    end.
%% -------------------------------------------------------
%% Check and renew session TTL and cookies
%% -------------------------------------------------------
-spec renew_session(SessionItem::map()) -> true | false.
%% -------------------------------------------------------
renew_session(SessionItem) ->
    TS = maps:get(<<"timestamp">>,SessionItem),
    case ?BU:timestamp() of
        NowTS when NowTS - TS < 4 * 3600 * 1000 -> false;
        _ ->
            SessionId = maps:get(<<"id">>,SessionItem),
            Domain = maps:get(<<"domain">>,SessionItem),
            UserId = maps:get(<<"userid">>,SessionItem),
            spawn(fun() ->
                        ClassItem = ?FixturerClasses:sessions(Domain),
                        Map = #{'object' => {'entity',SessionId,[]},
                                'initiator' => {'user',UserId},
                                'qs' => [],
                                'content' => SessionItem},
                        ?DMS_CRUD:crud({Domain,'update',ClassItem,?SessionsCN,Map,[]},fun(_) -> ok end)
                  end),
            true
    end.

%% ====================================================================
%% Allow-Origin functions
%% ====================================================================

%% -------------------------------------------------------
%% Setup access-control-* headers to oncoming response
%% -------------------------------------------------------
-spec allow_origin(Req::cowboy_req:req(), State::term()) -> Req1::cowboy_req:req().
%% -------------------------------------------------------
allow_origin(Req, _State) ->
    Host = cowboy_req:header(<<"host">>, Req, <<>>),
    Origin = cowboy_req:header(<<"origin">>, Req, Host),
    case lists:last(binary:split(Origin, <<"://">>, [])) of
        Host -> Req;
        _ ->
            Req1 = case cowboy_req:header(<<"access-control-request-method">>, Req, undefined) of
                       undefined -> Req;
                       M -> cowboy_req:set_resp_header(<<"access-control-allow-methods">>, M, Req)
                   end,
            Req2 = case cowboy_req:header(<<"access-control-request-headers">>, Req1, undefined) of
                       undefined -> Req1;
                       H-> cowboy_req:set_resp_header(<<"access-control-allow-headers">>, H, Req1)
                   end,
            Req3 = cowboy_req:set_resp_header(<<"access-control-allow-origin">>, Origin, Req2),
            _Req4 = cowboy_req:set_resp_header(<<"access-control-allow-credentials">>, <<"true">>, Req3)
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================