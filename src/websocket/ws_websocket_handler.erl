%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.06.2021
%%% @doc WebSocket connection cb module.
%%%      Works as FSM. requires authentication to work fully
%%%      Protocol:
%%%        Requests (in any way) from server by cap's API [Method, {qid, ...}] should be responsed by [Method_result, {qid, ...}]
%%%        Events from server by cap's API [Method, {...}] doesn't contain qid and should not be responsed
%%%        Connection setups required capabilities. Capabilities are implemented as plugins, and has unique methods and events exported.
%%%      Messages:
%%%        * 'authenticate',
%%%        * 'setup' capabilities (methods from cap's apis are become available, names of methods are strongly unique and different over all caps, registered togather).
%%%        * send requests to available APIs.
%%%        * get events and requests from server by cap's API.
%%%

-module(ws_websocket_handler).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([init/2,
         websocket_handle/2,
         websocket_info/2,
         terminate/2]).

-export([apply_msg/2]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").
-include("websocket.hrl").

-define(TIMERTOUT,5000).
-define(TIMERMSG,{timer}).

%% ====================================================================
%% Callbacks
%% ====================================================================

%% -------------------------------------------------------
%% init of websocket connection and state
%% -------------------------------------------------------
init(Req, Opts) ->
    case ?BAN:is_banned(element(1,cowboy_req:peer(Req))) of
        true ->
            ?BAN:login_failed(element(1,cowboy_req:peer(Req))),
            timer:sleep(1000 + rand:uniform(1000)), % anti brute-force pause
            stop_upgrade_reply({stop, Req, undefined}, 403);
        false ->
            do_init(Req, Opts)
    end.

%% @private next
do_init(Req0, Opts) ->
    Req = ?WsUtils:allow_origin(Req0, undefined),
    case ?BU:get_by_key(mode,Opts) of
        <<"user">> -> init_user(Req, Opts)
    end.

%% -----------
%% @private
%% User Api
%% -----------
init_user(Req, Opts) ->
    DebugLevel = undefined,
    Pid = maps:get(pid,Req), % self()
    State = #wsstate{state = ?Inited,
                     pid = Pid,
                     connid = ?BU:newid(),
                     api_modules = [?FsmInited],
                     initial_req = Req,
                     peer = cowboy_req:peer(Req),
                     path = ?BU:urldecode(cowboy_req:path(Req)),
                     opts = Opts,
                     timerref = erlang:send_after(10000,Pid,?TIMERMSG),
                     data = #{},
                     debug_level = DebugLevel},
    WebsockOpts = #{idle_timeout => 3600000,
                    compress => true,
                    validate_utf8 => true},
    debug(1, fun() -> Self = self(),
                      debug(1, {"WSconn ~p inited", [Self]}, DebugLevel),
                      ?BLmonitor:append_fun(Self, "WebSocket", fun(normal) -> ok;
                                                                  (R) -> ?LOG('$crash',"Websock connection ~p down (reason=~120p)", [Self,R]) end) end, State),
    case cowboy_req:parse_header(<<"sec-websocket-protocol">>, Req) of
        undefined ->
            {cowboy_websocket, Req, State, WebsockOpts};
        Subprotocols ->
            case lists:member(<<"json">>, Subprotocols) of
                true ->
                    Req2 = cowboy_req:set_resp_header(<<"sec-websocket-protocol">>, <<"json">>, Req),
                    {cowboy_websocket, Req2, State, WebsockOpts};
                false ->
                    debug(1, {"WSconn ~p closing ('json' subprotocol not found in requested: ~120p)", [Pid, Subprotocols]}, State),
                    stop_upgrade_reply({stop, Req, State}, 501)
            end end.

%% -------------------------------------------------------
%% handle websocket messages
%% -------------------------------------------------------
%{text|binary|ping|pong, binary()}, State}
%{ok, State}
%{ok, State, hibernate}
%{reply, {text, Data}|[{text, Data1}, {binary, Data2}], State}
%{stop, State}
websocket_handle({text, Data}, State) -> ?MODULE:apply_msg({text, Data}, State);
websocket_handle({binary, Data}, State) -> ?MODULE:apply_msg({binary, Data}, State);
websocket_handle({ping, _Data}, State) -> {ok, State};
websocket_handle({pong, _Data}, State) -> {ok, State}.

%% -------------------------------------------------------
%% handling process events
%% -------------------------------------------------------
websocket_info(stop, State) ->
    debug(1, {"WSconn ~p closing", [self()]}, State),
    {stop, State};

%%
websocket_info({send,{_,Data}=X}, #wsstate{debug_level=DL}=State) ->
    case DL of
        N when N<4 -> ok;
        4 -> debug(4, {"WSconn ~p send ~p bytes",[self(),byte_size(?BU:to_binary(Data))]}, State);
        5 -> debug(5, {"WSconn ~p send ~n  ~ts",[self(),take(1024,Data)]}, State);
        N when N>=6 -> debug(6, {"WSconn ~p send ~n  ~ts",[self(),Data]}, State);
        _ -> ok
    end,
    {reply, X, State};

%%
websocket_info({send,Data}, #wsstate{debug_level=DL}=State)
  when is_map(Data); is_list(Data); is_binary(Data) ->
    case DL of
        4 -> debug(4, {"WSconn ~p send ~p bytes",[self(),byte_size(?BU:to_binary(Data))]}, State);
        5 -> debug(5, {"WSconn ~p send ~n  ~ts",[self(),take(1024,Data)]}, State);
        N when N>=6 -> debug(6, {"WSconn ~p send ~n  ~ts",[self(),Data]}, State);
        _ -> ok
    end,
    {reply, {text,Data}, State};

%%
websocket_info({apply,F}, #wsstate{}=State) when is_function(F,1) ->
    case F(State) of
        {reply,{text,_},State1}=Res -> log_send(Res, State1), Res;
        Res -> Res
    end;
websocket_info({apply,{M,F,A1}}, #wsstate{}=State) when is_atom(M),is_atom(F) ->
    case erlang:apply(M,F,[A1,State]) of
        {reply,{text,_},State1}=Res -> log_send(Res, State1), Res;
        Res -> Res
    end;
websocket_info(?TIMERMSG, State) -> {ok, timer_expired(State)};
websocket_info(Info, State) ->
    ?MODULE:apply_msg({info, Info}, State).

%% @private
log_send(Res,#wsstate{debug_level=DL}=State) ->
    case Res of
        {reply,{text,Data},_} ->
            case DL of
                4 -> debug(4, {"WSconn ~p send ~p bytes",[self(),byte_size(?BU:to_binary(Data))]}, State);
                5 -> debug(5, {"WSconn ~p send ~n  ~ts",[self(),take(1024,Data)]}, State);
                N when N>=6 -> debug(6, {"WSconn ~p send ~n  ~ts",[self(),Data]}, State);
                _ -> ok
            end;
        _ -> ok
    end.

%% -------------------------------------------------------
%% on connection termination
%% -------------------------------------------------------
terminate(Reason, #wsstate{timerref=TRef}=State) ->
    debug(1, {"WSconn ~p terminated", [self()]}, State),
    ?BU:cancel_timer(TRef),
    {ok,State1} = ?WsReg:unregister(State),
    ?MODULE:apply_msg({terminate,Reason}, State1),
    clean_temporary(State1),
    ok.

%% ====================================================================
%% FSM
%% ====================================================================

apply_msg(Msg, #wsstate{state=S}=State)
  when S==?Inited; S==?Authenticated ->
    do_apply_msg(S, Msg, State).

% ----
do_apply_msg(_S, {text,Data}, State) ->
    apply_api(Data, State);
do_apply_msg(S, {binary,Data}, State) ->
    debug(2, {"WSconn '~s' ~p got binary ~n\t~120tp", [S,self(),Data]}, State),
    {ok, State};
do_apply_msg(_S, {info,Info}, State) ->
    apply_info(Info,State);
do_apply_msg(_S, {terminate,_Reason}, State) ->
    {ok, State}.

%% ====================================================================
%% Internal functions.
%% ====================================================================

%% ----------------------------
%% @private
%% Abort upgrading to websocket
stop_upgrade_reply({stop, Req, undefined}, HTTPCode) ->
    DebugLevel = ?APP:get_opt_value(<<"websock_debug_level">>, fun() -> 0 end),
    % generate minimal state so terminate() does not crash on function clause
    State = #wsstate{state=?Inited,
                     pid=self(),
                     debug_level=DebugLevel},
    stop_upgrade_reply({stop, Req, State}, HTTPCode);
%%
stop_upgrade_reply({stop, Req, State}, HTTPCode) ->
    Req2 = cowboy_req:reply(HTTPCode, Req),
    {ok, Req2, State}.

%% ----------------------------
%% @private
%% Clean temporary files and folders
clean_temporary(#wsstate{sessionid=SessionId}) ->
    clean_websocktemp_dir(SessionId).

%% @private
clean_websocktemp_dir(undefined) -> ok; % ignore missing session
clean_websocktemp_dir(SessionId) ->
    case ?FILEU:get_file_path_websocktemp(SessionId) of
        false -> ok; % ignore path not found
        StdPath ->
            Fdel = fun() -> ?BU:directory_delete(?BU:to_list(StdPath)) end,
            F = fun F(0) -> ok;
                    F(Attempts) ->
                        timer:sleep(30000),
                        case ?WsSess:get_connections_of_session_node_active(SessionId,node()) of
                            {ok,[_|_]} -> ok;
                            {ok,[]} -> Fdel();
                            {error, {not_found, <<"Session not found">>}} -> Fdel();
                            {error,_} -> F(Attempts-1)
                        end
                end,
            spawn(fun() -> F(30) end)
    end.

%% ====================================================================
%% Internal functions. Apply received info (from other pids)
%% ====================================================================

%% find modules where handle/3 exported. Apply there until non false result
apply_info(Info,#wsstate{api_modules=ApiModules}=State) ->
    F = fun(Module,false) ->
                case ?BU:function_exported(Module,handle,3) of
                    false -> false;
                    true ->
                        try Module:handle(info,Info,State)
                        catch _C:E:ST ->
                            debug(2, {"WSconn ~p info error (~s:handle/3): ~120p,~n\tStacktrace: ~120tp", [self(),Module,{_C,E},ST]}, State),
                            {ok,State}
                        end end;
                (_,Acc) -> Acc
        end,
    case lists:foldl(F, false, ApiModules) of
        false ->
            debug(2, {"WSconn '~s' ~p got info ~n\t~120tp", [State#wsstate.state,self(),Info]}, State),
            {ok, State};
        Reply -> Reply
    end.

%% ====================================================================
%% Internal functions. Apply received message (from websocket)
%% ====================================================================

%% check size, decode
apply_api(Msg, State) when byte_size(Msg) > 10485760 ->
    debug(3, {"WSconn ~p found too large message: ~p bytes", [self(), byte_size(Msg)]}, State),
    ?WsReply:reply({error, {invalid_params, <<"Too large json. Must be less than 10MB">>}}, State);
apply_api(Msg, State) ->
    try jsx:decode(Msg,[return_maps]) of
        [RMethod,RData] when is_map(RData) -> apply_api_0(Msg,RMethod,RData,State);
        _ ->
            debug(3, {"WSconn ~p found invalid json message: ~n  ~ts", [self(), Msg]}, State),
            ?WsReply:reply({error, {invalid_params, <<"Invalid json format">>}}, State)
    catch
        _:_ ->
            debug(3, {"WSconn ~p found non json message: ~n  ~ts", [self(), Msg]}, State),
            ?WsReply:reply({error, {invalid_params, <<"Invalid json">>}}, State)
    end.

%% @private Next
%%  debug log
apply_api_0(_Msg,RMethod,RData,#wsstate{debug_level=DL}=State) when DL<4 ->
    apply_api_1(RMethod,RData,State);
apply_api_0(_Msg,RMethod,RData,#wsstate{debug_level=DL}=State) when DL==4 ->
    debug(4, {"WSconn ~p recv '~ts'",[self(),RMethod]}, State),
    case apply_api_1(RMethod,RData,State) of
        {ok,_}=Ok -> Ok;
        {reply,_,_}=Reply ->
            debug(4, {"WSconn ~p send sync reply",[self()]}, State),
            Reply
    end;
apply_api_0(Msg,RMethod,RData,#wsstate{debug_level=DL}=State) when DL==5 ->
    debug(5, {"WSconn ~p recv ~n  ~ts",[self(),take(1024,Msg)]}, State),
    case apply_api_1(RMethod,RData,State) of
        {ok,_}=Ok -> Ok;
        {reply,{_,MsgOut},_}=Reply ->
            debug(5, {"WSconn ~p send ~n  ~ts",[self(),take(1024,MsgOut)]}, State),
            Reply
    end;
apply_api_0(Msg,RMethod,RData,#wsstate{debug_level=DL}=State) when DL>=6 ->
    debug(6, {"WSconn ~p recv ~n  ~ts",[self(),Msg]}, State),
    case apply_api_1(RMethod,RData,State) of
        {ok,_}=Ok -> Ok;
        {reply,{_,MsgOut},_}=Reply ->
            debug(6, {"WSconn ~p send ~n  ~ts",[self(),MsgOut]}, State),
            Reply
    end.

%% @private Next
%%  check if response, call waiting pid
apply_api_1(RMethod,RData,State) ->
    case ?BU:get_by_key(<<"qid">>,RData,undefined) of
        undefined -> apply_message(RMethod,RData,State);
        Qid ->
            case ?WsWaiter:take(Qid,State) of
                false -> apply_message(RMethod,RData,State);
                {value,Pid,State1} ->
                    Pid ! {response,Qid,RData},
                    {ok, State1}
            end end.

%% -----------------------
%% @private Next
%%  ensure method atom exists
apply_message(RMethod,RData,State) ->
    % check method, find module (api module' atoms already loaded on setup capabilities)
    case catch ?BU:to_atom(RMethod) of
        {'EXIT',_} ->
            debug(3, {"WSconn ~p got invalid request method ~ts", [self(), RMethod]}, State),
            check_late_response({error, {invalid_params, <<"Unknown method">>}}, RMethod,RData,State);
        RMethodAtom -> apply_message_1([RMethodAtom,RData], State)
    end.

%% @private Next
%%  select and call api module
apply_message_1([RMA,RData], #wsstate{api_modules=ApiModules}=State) ->
    F = fun(X,undefined) ->
                case ?BU:function_exported(X,RMA,2) of
                    true -> X;
                    false -> undefined
                end;
           (_,Acc) -> Acc
        end,
    case lists:foldl(F, undefined, ApiModules) of
        undefined -> check_late_response({error,{invalid_params,<<"Unknown/restricted method">>}},RMA,RData,State);
        Module ->
            try Module:RMA([RMA,RData],State)
            catch _C:E:StackTrace ->
                      debug(2, {"WSconn ~p api error (~s:~s): ~120p,~n\tStacktrace: ~120tp", [self(),Module,RMA,{_C,E},StackTrace]}, State),
                      Err = {error, {internal_error, ?BU:strbin("Websock API exec error, method '~ts'", [RMA])}},
                      ?WsReply:reply(Err, [RMA,RData], State)
            end end.

%% @private
%%  if method not found and qid present in data, check if method ends with "_result", then it's late response
check_late_response(_,_,undefined,State) -> {ok,State};
check_late_response(Err,RMethod,RData,State) ->
    case ?BU:get_by_key(<<"qid">>,RData,undefined) of
        undefined ->
            debug(3, {"WSconn ~p got non qid request", [self()]}, State),
            ?WsReply:reply(Err, [RMethod,RData], State);
        _Qid ->
            case lists:suffix("_result", ?BU:to_list(RMethod)) of
                true -> {ok,State};
                _ ->
                    debug(3, {"WSconn ~p got unknown/restricted method ~ts", [self(),RMethod]}, State),
                    ?WsReply:reply(Err, [RMethod,RData], State)
            end end.

%% ====================================================================
%% Internal functions. Apply received message
%% ====================================================================

%% ------------------------------
%% @private
%% Tick on every 5-10 sec
timer_expired(#wsstate{state=?Inited}=State) -> ?WsProc:stop(State);
timer_expired(#wsstate{state=?Authenticated}=State) ->
    State1 = ?WsWaiter:filter_expired_waiters(State),
    State2 = check_renew_session(State1),
    State2#wsstate{timerref=erlang:send_after(?TIMERTOUT+?BU:random(5000),self(),?TIMERMSG)}.

%% ------------------------------
%% @private
%% refresh session
check_renew_session(#wsstate{state=?Authenticated}=State) ->
    Key = 'last_refresh_session_ts',
    NowTS = ?BU:timestamp(),
    SkipTimeout = 300000, % check every 5 minutes
    case ?WsStore:find_data(Key,State,undefined) of
        TS when is_integer(TS), NowTS-TS<SkipTimeout -> State;
        _ -> ?WsStore:store_data(Key,NowTS,renew_session(State))
    end.

%% @private
renew_session(#wsstate{sessionid=SessionId,userid=UserId,domain=Domain}=State) ->
    FStop = fun(Reason) ->
                    Self = self(),
                    debug(2, {"WSconn ~p ~ts. Closing...", [Self,Reason]}, State),
                    Self ! stop
            end,
    case ?WsUtils:find_session(Domain,SessionId) of
        {ok,#{<<"userid">>:=UserId}=SessionItem} -> ?WsUtils:renew_session(SessionItem);
        {ok,_SessionItem} -> FStop("found invalid user in session");
        {wrong,_} -> FStop("found invalid domain in session");
        false -> FStop("session not found")
    end,
    State.

%% ====================================================================
%% Internal functions. Debug logging
%% ====================================================================

%% @private
debug(Level,Msg) -> debug(Level,Msg,?CFG:websocket_log_level()).
%% @private
debug(Level,Msg,#wsstate{debug_level=DL}) -> debug(Level,Msg,DL);
debug(Level,Msg,DL) when not is_integer(DL) -> debug(Level,Msg);
debug(Level,_,DL) when Level>DL -> ok;
debug(_,Fun,_) when is_function(Fun,0) -> Fun();
debug(_,{Fmt,Args},_) when is_list(Fmt), is_list(Args) -> ?LOG('$force',Fmt,Args);
debug(_,Text,_) when is_list(Text) -> ?LOG('$force',Text);
debug(_,_,_) -> ok.

%% @private
take(Count,T) when is_list(T), Count>=length(T) -> T;
take(Count,T) when is_list(T) -> {R,_} = lists:split(Count,T), R ++ ?BU:str("...[~p bytes]",[length(T)]);
take(Count,T) when is_binary(T), Count>=byte_size(T) -> T;
take(Count,T) when is_binary(T) -> <<(binary:part(T, 0, Count))/binary,(?BU:strbin("...[~p bytes]",[byte_size(T)]))/binary>>.

