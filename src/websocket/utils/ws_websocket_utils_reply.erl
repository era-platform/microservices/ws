%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.06.2021
%%% @doc Send reply routines

-module(ws_websocket_utils_reply).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    reply/2, reply/3,
    reply_async/2, reply_async/3
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("websocket.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ----------------------------------------
%% sync reply to websocket connection
%% ----------------------------------------
reply({error,_}=Err, State) -> reply(Err, undefined, State).
%%
reply({error,{_,Reason}}, undefined, State) ->
    OutData = jsx:encode([<<"error">>,
                         [{<<"result">>,<<"error">>},
                          {<<"errormsg">>,Reason}]]),
    {reply, {text,OutData}, State};
%%
reply({error,{_,Reason}}, [Method,Data], State) ->
    reply({error,Reason}, [Method,Data], State);
%%
reply({error,Reason}, [Method,Data], State)
  when (is_map(Data) orelse is_list(Data))
  andalso (is_binary(Reason) orelse is_list(Reason)) ->
    Qid = ?BU:get_by_key(<<"qid">>,Data,undefined),
    OutData = jsx:encode([?BU:join_binary([?BU:to_binary(Method),<<"_result">>]),
                          [{<<"qid">>,Qid},
                           {<<"result">>,<<"error">>},
                           {<<"errormsg">>,Reason}]]),
    {reply, {text,OutData}, State};
%%
reply(ok, [Method,Data], State) ->
    Qid = ?BU:get_by_key(<<"qid">>,Data,undefined),
    OutData = jsx:encode([?BU:join_binary([?BU:to_binary(Method),<<"_result">>]),
                          [{<<"qid">>,Qid},
                           {<<"result">>,<<"ok">>}]]),
    {reply, {text,OutData}, State};
%%
reply({ok,Name}, [_,_]=Msg, State) when is_atom(Name); is_binary(Name) ->
    reply({ok,{Name,[]}}, Msg, State);
%%
reply({ok,{Name,OutData}}, [_,Data], State)
  when (is_atom(Name) orelse is_list(Name) orelse is_binary(Name))
  andalso (is_map(OutData) orelse is_list(OutData)) ->
    Qid = ?BU:get_by_key(<<"qid">>,Data,undefined),
    OutData0 = case OutData of _ when is_list(OutData) -> OutData; _ when is_map(OutData) -> maps:to_list(OutData) end,
    OutData1 = jsx:encode([?BU:to_binary(Name), [{<<"qid">>,Qid}|OutData0]]),
    {reply, {text,OutData1}, State};
%%
reply(OutData, [Method,Data], State)
  when is_map(OutData); is_list(OutData) ->
    Qid = ?BU:get_by_key(<<"qid">>,Data,undefined),
    OutData0 = case OutData of _ when is_list(OutData) -> OutData; _ when is_map(OutData) -> maps:to_list(OutData) end,
    OutData1 = try jsx:encode([?BU:join_binary([?BU:to_binary(Method),<<"_result">>]), [{<<"qid">>,Qid}|OutData0]])
               catch E:R:ST ->
                    ?LOG("Websock reply crashed: ~120tp ~n\tOutData: ~120tp~n\tStack: ~120tp",[{E,R},OutData,ST]),
                    [{<<"qid">>,Qid},
                     {<<"result">>,<<"false">>},
                     {<<"errormsg">>,?BU:strbin("Internal error. Wrong response. See logs of '~tp'",[node()])}]
               end,
    {reply, {text,OutData1}, State}.

%% ----------------------------------------
%% async reply to websocket connection
%% ----------------------------------------
reply_async(T, State) -> reply_async(T, undefined, State).
reply_async(T, Msg, #wsstate{pid=Pid}=State) ->
    {_,Resp,_,_} = reply(T, Msg, State),
    Pid ! {send,Resp},
    ok.

%% ====================================================================
%% Internal functions
%% ====================================================================