%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.06.2021
%%% @doc Interaction with connection pid routines

-module(ws_websocket_utils_proc).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    reply/2, reply/3,
    reply_async/2, reply_async/3,
    stop/1,
    apply/2,
    send_event/2]).

-compile({no_auto_import,[apply/2]}).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("websocket.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================


%% ----------------------------------------
%% stop websocket connection
%% ----------------------------------------
reply(Reply,State) -> ?WsReply:reply(Reply,State).
reply(Reply,Msg,State) -> ?WsReply:reply(Reply,Msg,State).
reply_async(Reply,State) -> ?WsReply:reply_async(Reply,State).
reply_async(Reply,Msg,State) -> ?WsReply:reply_async(Reply,Msg,State).

%% ----------------------------------------
%% stop websocket connection
%% ----------------------------------------
stop(#wsstate{pid=Pid}=_State) ->
    stop(Pid);
%%
stop(HandlerPid) when is_pid(HandlerPid) ->
    HandlerPid ! stop.

%% ---------------------------------------------------
%% Apply to client connection (update state)
%% ---------------------------------------------------
-spec apply(Conn::conn_data(), FState::function()) -> ok | {error,Reason::term()}.
%% ---------------------------------------------------
apply(Conn,FState) ->
    WsPid = ?WsReg:get_connection_pid(Conn),
    F = fun(State) -> FState(State) end,
    case process_info(WsPid,status) of
        undefined -> {error,{invalid_request,<<"Connection not found">>}};
        _ ->
            WsPid ! {apply,F},
            ok
    end.

%% ---------------------------------------------------
%% Make event to client connection, no wait
%% ---------------------------------------------------
-spec send_event(Conn::conn_data(), Msg :: {EvM::binary(), Data::json_term()} | binary()) -> ok.
%% ----------------------------------------
send_event(Conn, OutData) when is_binary(OutData) ->
    % OutData should be jsx:encode([EvM,Data])
    WsPid = ?WsReg:get_connection_pid(Conn),
    case process_info(WsPid,status) of
        undefined -> {error,{invalid_request,<<"Connection not found">>}};
        _ ->
            WsPid ! {send,{text,OutData}},
            ok
    end;
%%
send_event(Conn, {EvM,Data}) when is_map(Data); is_list(Data) ->
    WsPid = ?WsReg:get_connection_pid(Conn),
    OutData = jsx:encode([EvM,Data]),
    case process_info(WsPid,status) of
        undefined -> {error,{invalid_request,<<"Connection not found">>}};
        _ ->
            WsPid ! {send,{text,OutData}},
            ok
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================