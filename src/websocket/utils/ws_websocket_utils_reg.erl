%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.06.2021
%%% @doc

-module(ws_websocket_utils_reg).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    register/2,
    unregister/1,
    get_domain_conns/1]).

-export([get_connection_pid/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("websocket.hrl").

-define(RefreshTimeoutMs, 25000).

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------------------------------
%% send register request to user center, wait for result and confirm capabilities
%% returns result for json answer and full list of capabilities, where some {Cap,{ok,_}} could be replaced by {Cap,{error,_}}.
%% ---------------------------------------------
-spec register(Caps::[{Cap::binary(), Res::{ok, Module::atom()} | {error,Reason::binary()}}], State::#wsstate{})
      -> {ok, CapsRes::[{Cap::binary(), Res::{ok,Module::atom(), State::#wsstate{}} | {error,Reason::binary()}}]}.
%% ---------------------------------------------
register(Caps,State) ->
    #wsstate{connid=ConnId, domain=Domain, userid=UserId, sessionid=SessionId,
             peer={Ip,Port}, path=Endpoint,
             appPath=AppPath, setup_params=SetupParams}=State,
    Caps1 = lists:filtermap(fun({Cap,{ok,_}}) -> {true,Cap}; (_) -> false end, Caps),
    Conn = #{<<"type">> => <<"websocket">>,
             <<"domain">> => Domain,
             <<"userid">> => UserId,
             <<"session_id">> => SessionId,
             <<"capabilities">> => Caps1,
             <<"remote_ip">> => ?BU:strbin("~ts:~p", [inet:ntoa(Ip),Port]),
             <<"dt_start">> => ?BU:strdatetime3339(?BU:utcdatetime_ms()),
             <<"info">> => #{
                 pid => self(),
                 node => node(),
                 site => ?PCFG:get_current_site(),
                 endpoint => ?BU:to_binary(Endpoint)},
             <<"setup_info">> => #{
                 app_path => AppPath,
                 params => SetupParams},
             <<"expires">> => 300},
    Map = build_crud_map(ConnId,UserId,Conn),
    ClassItem = ?FixturerClasses:connections(Domain),
    case catch ?DMS_CRUD:crud({Domain,'replace',ClassItem,?ConnectionsCN,Map,[]},fun(Reply) -> Reply end) of
        {'EXIT',Reason} ->
            ?LOG('$error',"refresh connection registration '~ts' crash: ~120tp",[ConnId,Reason]),
            {error,Reason};
        {error,Reason}=Err ->
            ?LOG('$error',"refresh connection registration '~ts' error: ~120tp",[ConnId,Reason]),
            Err; % {invalid,Caps}
        {ok,Entity} ->
            State1 = stop_register(State),
            State2 = start_register(Entity,State1),
            {ok,Caps,State2}
    end.

%% ---------------------------------------------
%% send unregister request to user center
%% ---------------------------------------------
-spec unregister(State::#wsstate{}) -> {ok, State1::#wsstate{}} | {error,Reason::term()}.
%% ----------------------------------------
unregister(#wsstate{domain=undefined,userid=undefined}=State) -> {ok,State};
unregister(#wsstate{connid=ConnId,domain=Domain,userid=UserId}=State) ->
    State1 = stop_register(State),
    Map = build_crud_map(ConnId,UserId),
    ClassItem = ?FixturerClasses:connections(Domain),
    case catch ?DMS_CRUD:crud({Domain,'delete',ClassItem,?ConnectionsCN,Map,[]}, fun(Reply) -> Reply end) of
        {'EXIT',Reason} -> {error,Reason};
        {error,_}=Err -> Err;
        ok ->
            {ok, State1#wsstate{userid=undefined,sessionid=undefined,domain=undefined}}
    end.

%% ---------------------------------------------
%% return active loggedin websock connections by query from usercenter on its restart
%% @todo
%% ---------------------------------------------
-spec get_domain_conns(Domain::binary()) -> [Map::map()].
%% ---------------------------------------------
get_domain_conns(Domain) ->
    case ?DMS_CACHE:read_direct(Domain,?ConnectionsCN,#{}) of
        {error,_}=Err -> throw(Err);
        {ok,Items} -> Items
    end.

%% ---------------------------------------------
%% return connection pid
%% ---------------------------------------------
-spec get_connection_pid(Conn::conn_data()) -> pid() | undefined.
%% ---------------------------------------------
get_connection_pid(Conn) when is_map(Conn) -> maps:get(pid,maps:get(<<"info">>,Conn,#{}),undefined);
get_connection_pid(Pid) when is_pid(Pid) -> Pid.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---------------------
%% @private
start_register(Entity,#wsstate{pid=Self}=State) ->
    NewPid = spawn(fun() -> refresh_registration(?RefreshTimeoutMs,Entity) end),
    ?BLmonitor:append_fun(NewPid,?MODULE,fun(normal) -> ok; (_) -> erlang:exit(Self,<<"Register pid is down">>) end),
    ?BLmonitor:append_fun(Self,?MODULE,fun(normal) -> ok; (_) -> erlang:exit(NewPid,<<"Connection pid is down">>) end),
    ?WsStore:store_values(#{register_pid => NewPid,
                            connection_item => Entity},State).

%% @private
stop_register(#wsstate{pid=Self}=State) ->
    case ?WsStore:find_data(register_pid,State,undefined) of
        PrevPid when is_pid(PrevPid) ->
            ?BLmonitor:drop_fun(Self,?MODULE),
            ?BLmonitor:drop_fun(PrevPid,?MODULE),
            PrevPid ! stop,
            ?WsStore:delete_keys([register_pid,connection_item],State);
        _ -> State
    end.

%% ---------------------
%% @private
refresh_registration(SleepMs,Entity) ->
    receive stop -> ok
    after SleepMs ->
        ConnId = maps:get(<<"id">>,Entity),
        Domain = maps:get(<<"domain">>,Entity),
        UserId = maps:get(<<"userid">>,Entity),
        {Operation,NewEntity} = {'replace',Entity}, % {'update',#{}}
        Map = build_crud_map(ConnId,UserId,NewEntity),
        ClassItem = ?FixturerClasses:connections(Domain),
        case catch ?DMS_CRUD:crud({Domain,Operation,ClassItem,?ConnectionsCN,Map,[]}, fun(Reply) -> Reply end) of
            {'EXIT',Reason} ->
                ?LOG('$error',"refresh connection registration '~ts' crash: ~120tp",[ConnId,Reason]),
                refresh_registration(timeout(SleepMs,{error,Reason}),Entity);
            {error,Reason}=Err ->
                ?LOG('$error',"refresh connection registration '~ts' error: ~120tp",[ConnId,Reason]),
                refresh_registration(timeout(SleepMs,Err),Entity);
            {ok,Entity1} ->
                refresh_registration(?RefreshTimeoutMs,Entity1)
        end end.

%% @private
timeout(?RefreshTimeoutMs,_) -> 1000;
timeout(1000,_) -> 2000;
timeout(2000,_) -> 4000;
timeout(4000,_) -> 8000;
timeout(8000,_) -> 16000;
timeout(16000,{error,Reason}) -> exit(Reason).

%% @private
build_crud_map(ConnId,UserId,Content) ->
    (build_crud_map(ConnId,UserId))#{content => Content}.
build_crud_map(ConnId,UserId) ->
    #{'object' => {'entity',ConnId,[]},
      'initiator' => {'user',UserId},
      'qs' => []}.