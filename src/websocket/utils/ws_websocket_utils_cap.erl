%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.06.2021
%%% @doc Capabilities, access to API modules. Routines

-module(ws_websocket_utils_cap).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    check_capabilities/1,
    check_capabilities_roles/2
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("websocket.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% -------------------------------------------------------
%% Map capabilities from 'setup' request into {Cap::binary(),Result::ok|{error,Reason::binary()}
%% -------------------------------------------------------
-spec check_capabilities(Caps::[map()|proplist()])
      -> [{Key::binary(), Result::ok | {error,Reason::binary()}}].
%% -------------------------------------------------------
check_capabilities(Caps) ->
    lists:foldr(fun(Cap,Acc) when is_list(Cap); is_map(Cap) ->
                        case ?BU:get_by_key(<<"key">>,Cap,undefined) of
                            undefined -> Acc;
                            Key ->
                                case capability(Key) of
                                    undefined -> [{Key,{error,<<"Unknown capability">>}} | Acc];
                                    _ -> [{Key,ok} | Acc]
                                end end end, [], Caps).

%% -------------------------------------------------------
%% Split capabilities by user roles.
%% Check if user can use capability.
%% -------------------------------------------------------
-spec check_capabilities_roles(Caps::[Cap|{Cap,ok}|{Cap,Err}], State::#wsstate{})
      -> [{Cap, {ok,Module::atom()} | Err}]
    when Cap::binary(), Err::{error,Reason::binary()}.
%% -------------------------------------------------------
check_capabilities_roles(Caps,#wsstate{domain=Domain,userid=UserId}=State) ->
    F = fun(Cap,Acc) ->
                {Module} = capability(Cap),
                case check_roles(State,Domain,UserId,Cap) of
                    ok -> [{Cap,{ok,Module}}|Acc];
                    {error,_}=Err -> [{Cap,Err}|Acc]
                end end,
    lists:foldr(fun({_,{error,_}}=C,Acc) -> [C|Acc];
                   ({Cap,ok},Acc) -> F(Cap,Acc);
                   (Cap,Acc) -> F(Cap,Acc)
                end, [], Caps).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
%% capability user roles in domain
check_roles(State,Domain,UserId,Cap) ->
    URL = make_iam_websocket_url(State,Cap),
    Method = <<"WEBSOCKET">>,
    IamAuthArgs = [{'endpoint',URL,Method},{'user',UserId}],
    GlobalName = ?GN_NAMING:get_globalname(?MsvcIam,Domain),
    case catch ?GLOBAL:gen_server_call(GlobalName,{'authorize',IamAuthArgs}) of
        {'EXIT',Reason} ->
            ?LOG('$crash',"IAM call crashed: ~120tp",[Reason]),
            {error,<<"IAM is not accessible">>};
        {error,{_,Reason}} when is_binary(Reason) -> {error,Reason};
        {error,Reason}=Err when is_binary(Reason) -> Err;
        ok -> ok
    end.

%% @private
make_iam_websocket_url(#wsstate{path=Path}, Cap) ->
    <<Path/binary,"#",Cap/binary>>.

%% ------------------------------------
%% ------------------------------------
%% @private
%% Known capability modules. Returns {Module::atom()}
%% Plugins supported by module name and function test() inside.
capability(<<"rest">>) -> {ws_websocket_capabilities_general_rest};
capability(<<"subscr">>) -> {ws_websocket_capabilities_general_subscr};
capability(Cap) ->
    Module = ?BU:to_atom_new(?BU:strbin("~s~s", [<<"ws_websocket_capabilities_plugin_">>,Cap])),
    case ?BU:function_exported(Module,test,0) of
        true -> {Module};
        false -> undefined
    end.
