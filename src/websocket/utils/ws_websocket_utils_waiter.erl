%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.06.2021
%%% @doc Response-waiter routines

-module(ws_websocket_utils_waiter).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([take/2]).

-export([filter_expired_waiters/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("websocket.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ----------------------------------------
%% Returns pid of waiter, taken from connection state's storage
%% ----------------------------------------
take(Qid, State) ->
    WR = ?WsStore:find_data(?WAITREQS,State,[]),
    case lists:keytake(Qid,#resp_waiter.qid,WR) of
        {value,#resp_waiter{pid=Pid},WR1} ->
            {ok,Pid,?WsStore:store_data(?WAITREQS,WR1,State)};
        false -> false
    end.

%% ----------------------------------------
%% filter expired waiters
%% ----------------------------------------
filter_expired_waiters(State) ->
    % ?OUT("State: ~120p", [State]),
    case ?WsStore:find_data(?WAITREQS,State,[]) of
        [] -> State;
        WR ->
            Now = ?BU:timestamp(),
            WR1 = lists:filter(fun(#resp_waiter{expire_ts=E}) -> E>Now end, WR),
            ?WsStore:store_data(?WAITREQS,WR1,State)
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================