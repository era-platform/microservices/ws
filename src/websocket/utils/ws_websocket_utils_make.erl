%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.06.2021
%%% @doc Make request routines

-module(ws_websocket_utils_make).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([make_request/3, make_request/4, make_request/5]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("websocket.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---------------------------------------------------
%% Make request to client connection, register qid in state and wait response till timeout
%% ---------------------------------------------------
-spec make_request(Conn::conn_data(), Msg::{EvM::binary(), Opts::json_term()}, FState::function())
      -> {ok,ResponseData::list()} | {error,R::{timeout,Msg::binary()}}.
%% ----------------------------------------
make_request(Conn, {EvM,Opts}, FState) ->
    make_request(Conn, {EvM,Opts}, FState, 5000).

%% ----------------------------------------
-spec make_request(Conn::conn_data(), {EvM::binary(), Opts::json_term()}, FState::function(), Timeout::integer())
      -> {ok,ResponseData::list()} | {error,R::{timeout,Msg::binary()}}.
%% ----------------------------------------
make_request(Conn, {EvM,Opts}, undefined, Timeout) ->
    make_request(Conn, {EvM,Opts}, fun(_) -> ok end, fun(S) -> S end, Timeout);
make_request(Conn, {EvM,Opts}, FState, Timeout) ->
    make_request(Conn, {EvM,Opts}, fun(_) -> ok end, FState, Timeout).

%% ----------------------------------------
-spec make_request(Conn::conn_data(), {EvM::binary(), Opts::json_term()}, FCheck::function(), FState::function(), Timeout::integer())
      -> {ok,ResponseData::list()} | {error,R::{timeout,Msg::binary()}}.
%% ----------------------------------------
make_request(Conn, {EvM,Opts}, FCheck, undefined, Timeout) ->
    make_request(Conn, {EvM,Opts}, FCheck, fun(S) -> S end, Timeout);
make_request(Conn, {EvM,Opts}, FCheck, FState, Timeout) ->
    Self = self(),
    % update request by qid
    Qid = ?BU:newid(),
    Opts1 = case Opts of
                M when is_map(M) -> M#{<<"qid">> => Qid};
                L when is_list(L) -> lists:keystore(<<"qid">>,1,L,{<<"qid">>,Qid})
            end,
    OutData = jsx:encode([EvM,Opts1]),
    Now = ?BU:timestamp(),
    RW = #resp_waiter{qid=Qid,pid=self(),ts=Now,expire_ts=Now+Timeout},
    % send to connection pid
    F = fun(State) ->
                case FCheck(State) of
                    ok ->
                        W = ?WsStore:find_data(?WAITREQS,State,[]),
                        W1 = lists:keystore(Qid,#resp_waiter.qid,W,RW),
                        State1 = ?WsStore:store_data(?WAITREQS,W1,FState(State)),
                        {reply, {text,OutData}, State1};
                    {error,_}=Err ->
                        Self ! Err,
                        {ok, State}
                end end,
    WsPid = ?WsReg:get_connection_pid(Conn),
    case process_info(WsPid,status) of
        undefined -> {error,{invalid_request,<<"Connection not found">>}};
        _ ->
            WsPid ! {apply,F},
            receive
                {response,Qid,ResponseData} -> {ok,ResponseData};
                {error,_}=Err -> Err
            after Timeout -> {error,{timeout,<<"Wait response timeout">>}}
            end
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================