%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.06.2021
%%% @doc

-module(ws_websocket_utils_session).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    get_connections_of_session_node_active/3, get_connections_of_session_node_active/4,
    get_connections_of_session_node/3, get_connections_of_session_node/4,
    get_connections_of_token/1,
    get_connections_of_session/2, get_connections_of_session/3,
    get_connections_of_user/2, get_connections_of_user/3
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("websocket.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------------------
%% Return connections items list (websocket on selected node, domain, sessionid).
%% Filters by alive pid
%%   also can filter by capabilities
%% ------------------------------------------
get_connections_of_session_node_active(Domain,SessionId,Node) -> get_connections_of_session_node_active(Domain,SessionId,Node,[]).
get_connections_of_session_node_active(Domain,SessionId,Node,Caps) ->
    case get_connections_of_session(Domain,SessionId,Caps) of
        {ok,Conns} ->
            {ok,lists:filter(fun(Conn) ->
                                    Info = maps:get(<<"info">>,Conn),
                                    case {maps:get('node',Info,undefined),maps:get('pid',Info,undefined)} of
                                        {Node,Pid} when is_pid(Pid) -> check_is_process_alive(Pid);
                                        _ -> false
                                    end
                             end, Conns)};
        Err -> Err
    end.

%% ------------------------------------------
%% Return connections items list (websocket on selected node, domain, sessionid)
%%   also can filter by capabilities
%% ------------------------------------------
get_connections_of_session_node(Domain,SessionId,Node) -> get_connections_of_session_node(Domain,SessionId,Node,[]).
get_connections_of_session_node(Domain,SessionId,Node,Caps) ->
    case get_connections_of_session(Domain,SessionId,Caps) of
        {ok,Conns} ->
            {ok,lists:filter(fun(Conn) ->
                                    Info = maps:get(<<"info">>,Conn),
                                    maps:get('node',Info,undefined) == Node
                             end, Conns)};
        Err -> Err
    end.

%% ------------------------------------------
%% Return connections items list by session token (websocket in selected domain, sessionid)
%%   also can filter by capabilities
%% ------------------------------------------
get_connections_of_token(Token) ->
    case ?ParseToken(Token) of
        {ok,SessionId,Domain} -> get_connections_of_session(Domain,SessionId);
        error -> {error, {invalid_params, <<"Invalid session token">>}}
    end.

%% ------------------------------------------
%% Return connections items list (websocket in selected domain, sessionid)
%%   also filter by capabilities if non-empty
%% ------------------------------------------
get_connections_of_session(Domain,SessionId,Caps) ->
    case get_connections_of_session(Domain,SessionId) of
        {ok,Conns} -> {ok, filter_by_caps(Conns, Caps)};
        Err -> Err
    end.

%% ------------------------------------------
%% Return connections items list (websocket in selected domain, sessionid)
%% ------------------------------------------
get_connections_of_session(Domain,SessionId) ->
    Filter = [<<"==">>,[<<"property">>,<<"session_id">>],SessionId],
    read_connections(Domain,Filter).

%% ------------------------------------------
%% Return connections items list (websocket in selected domain, sessionid)
%%   also filter by capabilities if non-empty
%% ------------------------------------------
get_connections_of_user(Domain, UserId, Caps) ->
    case get_connections_of_user(Domain, UserId) of
        {ok,Conns} -> {ok, filter_by_caps(Conns, Caps)};
        Err -> Err
    end.

%% ------------------------------------------
%% Return connections items list (websocket in selected domain, userid)
%% ------------------------------------------
get_connections_of_user(Domain, UserId) ->
    Filter = [<<"==">>,[<<"property">>,<<"userid">>],UserId],
    read_connections(Domain,Filter).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -------------------------------------------------------
%% Read connections by filter in dms
%% -------------------------------------------------------
-spec read_connections(Domain::binary() | atom(), Filter::list()) -> {ok,Conns::[map()]} | {error,Reason::term()}.
%% -------------------------------------------------------
read_connections(Domain,Filter) ->
    CurSite = ?PCFG:get_current_site(),
    case ?DMS_CACHE:read_direct(Domain,?ConnectionsCN,#{<<"filter">> => Filter},auto) of
        {ok,Conns} ->
            {ok,lists:filtermap(fun(Conn) ->
                case {maps:get(<<"type">>,Conn),maps:get(<<"info">>,Conn)} of
                    {<<"websocket">>,#{site:=CurSite}} -> {true,Conn};
                    _ -> false
                end end, Conns)};
        {error,_} ->
            {error, {not_found, <<"Websocket connection pid not found (dms unavailable)">>}}
    end.

%% -------------------------------------------------------
%% Filter connections items by selected capabilities
%%   Non-empty intersection or requested capabilities empty list
%% -------------------------------------------------------
-spec filter_by_caps(Conns::[map()],Caps::[binary()]) -> Conns1::[map()].
%% -------------------------------------------------------
filter_by_caps(Conns, []) -> Conns;
filter_by_caps(Conns, Caps) ->
    F = fun(Conn) ->
                case Caps of
                    [Cap] ->
                        case lists:member(Cap,maps:get(<<"capabilities">>,Conn,[])) of
                            false -> false;
                            true -> {true,Conn}
                        end;
                    [_|_] ->
                        case ordsets:intersection(ordsets:from_list(maps:get(<<"capabilities">>,Conn,[])), ordsets:from_list(Caps)) of
                            [] -> false;
                            _ -> {true,Conn}
                        end;
                    _ -> false
                end end,
    lists:filtermap(F, Conns).

%% -------------------------------------------------------
%% Check if pid is alive (on current or other node)
%% -------------------------------------------------------
-spec check_is_process_alive(Pid::pid()) -> boolean() | unavailable.
%% -------------------------------------------------------
check_is_process_alive(Pid) ->
    Node = node(),
    case erlang:node(Pid) of
        Node -> erlang:is_process_alive(Pid);
        RemoteNode ->
            case ?BLrpc:call(RemoteNode, erlang, is_process_alive, [Pid], 1000) of
                {'EXIT',_} -> unavailable;
                {badrpc,_} -> unavailable;
                {error,_} -> unavailable;
                T -> T
            end end.