%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.06.2021
%%% @doc Operations in connection storage routines

-module(ws_websocket_utils_store).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    store_data/3,
    store_values/2,
    find_data/3,
    delete_data/2,
    delete_keys/2
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("websocket.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------
%% Store Key/Value in connection's bag
%% --------------
store_data(Key,Value,#wsstate{data=Map}=State) ->
    State#wsstate{data=Map#{Key => Value}}.

%% --------------
%% Store multiple keys/values in connection's bag
%% --------------
store_values(AddMap,#wsstate{data=Map}=State) when is_map(AddMap) ->
    State#wsstate{data=maps:merge(Map,AddMap)}.

%% --------------
%% Find by key in connection's bag, return default if not found.
%% --------------
find_data(Key,#wsstate{data=Map},Default) ->
    case maps:get(Key,Map,undefined) of
        undefined -> Default;
        T -> T
    end.

%% --------------
%% Delete key from connection's bag
%% --------------
delete_data(Key,#wsstate{data=Map}=State) ->
    State#wsstate{data=maps:without([Key],Map)}.

%% --------------
%% Delete keys from connection's bag
%% --------------
delete_keys(Keys,#wsstate{data=Map}=State) when is_list(Keys) ->
    State#wsstate{data=maps:without(Keys,Map)}.

%% ====================================================================
%% Internal functions
%% ====================================================================