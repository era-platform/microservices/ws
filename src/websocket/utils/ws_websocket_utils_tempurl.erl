%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.06.2021
%%% @doc

-module(ws_websocket_utils_tempurl).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    move_file_to_temp_url/2,
    copy_file_to_temp_url/3,
    store_temp_file/1, store_temp_file/2
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("websocket.hrl").

-define(TempTTL, 180).

%% ====================================================================
%% Public functions
%% ====================================================================

%% ------------------------------------
%% moves file to make it available from from public temp url for 5 minutes
%% ------------------------------------
move_file_to_temp_url(SrcPath,PidAutoDelete) ->
    TmpPath = ?FILEU:get_file_path_statictemp(),
    LUID = string:to_lower(?BU:to_list(?BU:luid())),
    Extension = filename:extension(SrcPath),
    Filename = ?BU:str("~s~s", [LUID, Extension]),
    DestPath = filename:join([TmpPath,Filename]),
    ?BLfile:rename(SrcPath,DestPath),
    ?WsTmp:store_temp_file(DestPath,PidAutoDelete),
    {ok, ?BU:strbin("/temp/~s",[Filename])}.

%% ------------------------------------
%% copies file from another node and make it available from public temp url for 5 minutes
%% ------------------------------------
copy_file_to_temp_url(FromSN,FromSrcPath,PidAutoDelete) ->
    TmpPath = ?FILEU:get_file_path_statictemp(),
    LUID = string:to_lower(?BU:to_list(?BU:luid())),
    Extension = filename:extension(FromSrcPath),
    Filename = ?BU:str("~s~s", [LUID, Extension]),
    DestPath = filename:join([TmpPath,Filename]),
    case ?COPIER:copy_file(FromSN,FromSrcPath,DestPath) of
        ok ->
            ?WsTmp:store_temp_file(DestPath,PidAutoDelete),
            {ok,?BU:strbin("/temp/~s",[Filename])};
        {error,R} ->
            {error,?BU:strbin("~120tp",[R])}
    end.

%% ------------------------------------
%% store file for max 5 minutes until pid is alive
%% ------------------------------------
store_temp_file(Path) -> store_temp_file(Path, self()).
store_temp_file(Path,OwnerPid) ->
    spawn(fun() when is_pid(OwnerPid) ->
                MonRef = erlang:monitor(process,OwnerPid),
                receive {'DOWN',MonRef,process,OwnerPid,_} -> ok
                after ?TempTTL*1000 -> ok
                end,
                ?BLfile:delete(Path);
             () ->
                timer:sleep(?TempTTL*1000),
                ?BLfile:delete(Path)
          end).

%% ====================================================================
%% Internal functions
%% ====================================================================