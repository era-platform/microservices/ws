%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 01.07.2021
%%% @doc Sample plugin capability module.

-module(ws_websocket_capabilities_plugin_sample).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([test/0]).

-export([pingplugin/2]).

-export([event/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("websocket.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

test() -> ok.

%% -------------------------------------
%% @Method
%% business logic function
%% -------------------------------------
pingplugin([_,_Data]=Msg, State) ->
    OutData = [{<<"result">>,<<"ok">>}],
    ?WsReply:reply(OutData, Msg, State).

%% -------------------------------------
%% when notified events (cdr and other)
%% -------------------------------------
-spec event({{Event::binary(), Opts::json_term()}, Conn::conn_data(), Timeout::integer}) -> ok.
%% -------------------------------------
event({{<<"eventplugin">>=EvM,Opts}, Conn, _Timeout}) ->
    case catch jsx:encode(Opts) of
        {'EXIT',_} -> ?WsProc:send_event(Conn, {EvM,[{<<"invalid_data">>,?BU:to_binary(lists:flatten(io_lib:format("~tp",[Opts])))}]});
        _ -> ?WsProc:send_event(Conn, {EvM,Opts})
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================