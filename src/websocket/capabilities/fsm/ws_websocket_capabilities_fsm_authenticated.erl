%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 29.06.2021
%%% @doc FSM module of state Authenticated. Common methods implementation

-module(ws_websocket_capabilities_fsm_authenticated).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    ping/2,
    authenticate/2,
    stop/2,
    setup/2,
    connection_info/2]).

-export([event/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("websocket.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% @Method
ping(Msg, State) ->
    ?WsReply:reply({ok,<<"pong">>}, Msg, State).

%% @Method
authenticate([_Method,_Data]=Msg, State) ->
    ?WsReply:reply({error,{invalid_request,<<"Already authenticated">>}}, Msg, State).

%% @Method
stop(Msg, #wsstate{}=State) ->
    ?WsReg:unregister(State),
    ?WsProc:stop(State),
    ?WsReply:reply(ok, Msg, State).

%% @Method
setup([_,Data]=Msg, State) ->
    case ?BU:extract_required_props([<<"capabilities">>], Data) of
        {error,_}=Err -> ?WsUtils:reply(Err, Msg, State);
        [NewCaps] ->
            % check capabilities (if known versions)
            Caps1 = ?WsCap:check_capabilities(NewCaps),
            % check user roles for capabilities
            Caps2 = ?WsCap:check_capabilities_roles(Caps1,State),
            % to register app and params
            State1 = State#wsstate{'appPath' = ?BU:get_by_key(<<"appPath">>,Data,null),
                                   'setup_params' = ?BU:get_by_key(<<"params">>,Data,#{})},
            % register over usercenter, filter caps
            case ?WsReg:register(Caps2,State1) of
                {error,_}=Err -> ?WsReply:reply(Err, Msg, State1);
                {ok,CapsReg,State2} ->
                    % new state
                    ApiModules = [?FsmAuthenticated | lists:filtermap(fun({_,{ok,Module}})-> {true,Module}; (_) -> false end, CapsReg)],
                    State3 = State2#wsstate{'capabilities' = lists:filtermap(fun({Cap,{ok,_}})-> {true,Cap}; (_) -> false end, CapsReg),
                                            'api_modules' = ApiModules},
                    % forcely load atoms of api modules (not cached, cause of dynamic load modules)
                    lists:foreach(fun(Module) -> ?BU:function_exported(Module,module_info,0) end, ApiModules),
                    % response
                    OutData = [{<<"result">>,<<"ok">>},
                               {<<"capabilities">>,lists:map(fun({Cap,{ok,_}}) -> [{<<"key">>,Cap},{<<"result">>,<<"ok">>}];
                                                                ({Cap,{error,R}}) -> [{<<"key">>,Cap},{<<"result">>,<<"error">>},{<<"errormsg">>,R}]
                                                             end, CapsReg)}],
                    ?WsReply:reply(OutData, Msg, State3)
            end
    end.

%% @Method
connection_info([_,_Data]=Msg, #wsstate{domain=Domain,userid=UserId,connid=ConnId,sessionid=SessionId,capabilities=Caps}=State) ->
    UserData = case ?WsUtils:has_iam() of
                   false -> #{};
                   true ->
                       case ?DMS_CACHE:read_cache(Domain,?UsersCN,#{object => {'entity',UserId,[]}},auto) of
                           {ok,UserItem,_} ->
                               #{<<"userlogin">> => maps:get(<<"login">>,UserItem),
                                 <<"username">> => maps:get(<<"name">>,UserItem),
                                 <<"timezone">> => maps:get(<<"timezone">>,UserItem,<<"default">>),
                                 <<"roles">> => maps:get(<<"roles">>,UserItem)};
                           {error,Reason} -> #{<<"error">> => ?BU:strbin("~tp",[Reason])}
                       end end,
    ENowTS = os:timestamp(),
    OutData = [{<<"result">>,<<"ok">>},
               {<<"site">>,?PCFG:get_current_site()},
               {<<"ownertype">>,<<"user">>},
               {<<"domain">>,Domain},
               {<<"userid">>,UserId},
               {<<"session_id">>,SessionId},
               {<<"session">>,?MakeToken(SessionId,Domain)},
               {<<"connection_id">>,ConnId},
               {<<"capabilities">>,Caps},
               {<<"datetime">>,?BU:strdatetime3339(?BU:utcdatetime_ms(ENowTS))},
               {<<"timestamp">>,?BU:timestamp(ENowTS)}
               | maps:to_list(UserData)],
    ?WsReply:reply(OutData, Msg, State).

%% ---------------------------
%% From system
%% ---------------------------

-spec event({{Event::binary(), Opts::json_term()}, Conn::conn_data(), Timeout::integer()}) -> ok.

event({{<<"user_state_changed">>=EvM,Opts}, Conn, _Timeout}) ->
    ?WsProc:send_event(Conn, {EvM,Opts});

event({{EvM,Opts}, Conn, _Timeout}) when is_binary(EvM) ->
    ?WsProc:send_event(Conn, {EvM,Opts}).

%% ====================================================================
%% Internal functions
%% ====================================================================