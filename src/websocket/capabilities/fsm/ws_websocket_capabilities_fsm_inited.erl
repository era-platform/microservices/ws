%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 29.06.2021
%%% @doc FSM module of state Inited (no authentication). Common methods implementation.

-module(ws_websocket_capabilities_fsm_inited).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    ping/2,
    setup/2,
    authenticate/2,
    stop/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("websocket.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% @Method
ping(Msg, State) ->
    ?WsReply:reply({ok,<<"pong">>}, Msg, State).

%% @Method
setup([_,Data]=Msg, #wsstate{}=State) ->
    case ?BU:extract_required_props([<<"capabilities">>], Data) of
        {error,_}=Err -> ?WsReply:reply(Err, Msg, State);
        [NewCaps] ->
            % check capabilities (if known versions)
            Caps1 = ?WsCap:check_capabilities(NewCaps),
            % new state, store only capabilities
            State1 = State#wsstate{'capabilities' = lists:filtermap(fun({Cap,ok})-> {true,Cap}; (_) -> false end, Caps1),
                                   'setup_params' = ?BU:get_by_key(<<"params">>,Data,#{}),
                                   'appPath' = ?BU:get_by_key(<<"appPath">>,Data,null)},
            % response
            OutCaps = lists:map(fun({Cap,ok}) -> [{<<"key">>,Cap},{<<"result">>,<<"ok">>}];
                                   ({Cap,{error,R}}) -> [{<<"key">>,Cap},{<<"result">>,<<"error">>},{<<"errormsg">>,R}]
                                end, Caps1),
            OutData = [{<<"result">>,<<"ok">>},
                       {<<"capabilities">>,OutCaps}],
            ?WsReply:reply(OutData, Msg, State1)
    end.

%% @Method
authenticate([_Method,_Data]=Msg, #wsstate{peer={Ip,_}}=State) ->
    case ?BAN:is_banned(Ip) of
        true -> auth_failed_reply(Msg, State); % extend ban when already banned
        false -> do_authenticate(internal, Msg, State)
    end.

%% @Method
stop([_Method,_Data]=Msg, State) ->
    ?WsProc:stop(State),
    ?WsReply:reply(ok, Msg, State).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---------------------------------
%% LOGIN routines
%% ---------------------------------
do_authenticate(internal,[_Method,Data]=Msg, State) ->
    case ?BU:get_by_key(<<"session">>,Data,undefined) of
        undefined ->
            ?WsReply:reply({error,{invalid_paramse,<<"Expected 'session'. Make 'POST /rest/v1/sessions' or 'GET /rest/v1/sessions/current'">>}}, Msg, State);
        Token ->
            case ?ParseToken(Token) of
                error ->
                    ?WsReply:reply({error,{invalid_params,<<"Wrong session token'">>}}, Msg, State);
                {ok,SessionId,Domain0} ->
                    Domain = case Domain0 of
                                 <<"root">> -> 'root';
                                 _ -> Domain0
                             end,
                    case ?WsUtils:find_session(Domain,SessionId) of
                        false -> ?WsReply:reply({error,{not_found,<<"Session not found'">>}}, Msg, State);
                        {wrong,_} -> ?WsReply:reply({error,{not_found,<<"Wrong session'">>}}, Msg, State);
                        {ok,SessionItem} ->
                            M = #{sessionid => SessionId,
                                  domain => Domain,
                                  userid => maps:get(<<"userid">>,SessionItem)},
                            auth_ok(M,Msg,State)
                    end end end.

%% @private
auth_ok(M, Msg, State) ->
    [Domain,UserId,SessionId] = ?BU:maps_get([domain,userid,sessionid], M),
    % new state 1
    State1 = State#wsstate{state=?Authenticated,
                           domain=Domain,
                           userid=UserId,
                           sessionid=SessionId},
    % check user roles for capabilities
    Caps2 = ?WsCap:check_capabilities_roles(State1#wsstate.capabilities,State1),
    % register over usercenter, filter capabilities
    case ?WsReg:register(Caps2,State1) of
        {error,_}=Err -> ?WsReply:reply(Err, Msg, State1);
        {ok,CapsReg,State2} ->
            % new state 2
            ApiModules=[?FsmAuthenticated | lists:filtermap(fun({_,{ok,Module}})-> {true,Module}; (_) -> false end, CapsReg)],
            State3 = State2#wsstate{'capabilities' = lists:filtermap(fun({Cap,{ok,_}})-> {true,Cap}; (_) -> false end, CapsReg),
                                    'api_modules' = ApiModules},
            % forcely load atoms of api modules (not cached, cause of dynamic load modules)
            lists:foreach(fun(Module) -> ?BU:function_exported(Module,module_info,0) end, ApiModules),
            % response
            OutData = [{<<"result">>,<<"ok">>},
                       {<<"domain">>,Domain},
                       {<<"userid">>,UserId},
                       {<<"session">>,?MakeToken(SessionId,Domain)},
                       {<<"capabilities">>,lists:map(fun({Cap,{ok,_}}) -> [{<<"key">>,Cap},{<<"result">>,<<"ok">>}];
                                                        ({Cap,{error,R}}) -> [{<<"key">>,Cap},{<<"result">>,<<"error">>},{<<"errormsg">>,R}]
                                                     end, CapsReg)}],
            ?WsReply:reply(OutData, Msg, State3)
    end.

%% @private
auth_failed_reply(Msg, #wsstate{peer={Ip,_}}=State) ->
    ?BAN:login_failed(Ip),
    timer:sleep(1000),
    ?WsProc:stop(State),
    ?WsReply:reply({error, {access_denied, <<"Invalid prerequisites">>}}, Msg, State).