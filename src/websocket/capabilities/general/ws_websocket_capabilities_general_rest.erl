%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.06.2021
%%% @doc General capability module. REST API implementation.

-module(ws_websocket_capabilities_general_rest).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([rest/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("websocket.hrl").

-define(RESTU, ws_rest_behavior_utils).
-define(RESTU_IAM, ws_rest_behavior_utils_iam).

-define(Timeout, 30000).

-record(caller,{
    domain :: binary(),
    wspid :: pid(),
    calls :: map() % ref => {Req,TS,TimerRef}
}).

%% ====================================================================
%% Public functions
%% ====================================================================

%% @Method
rest([_Method,_Data]=Msg, #wsstate{}=State) ->
    semi_sync(Msg,State).

%% ====================================================================
%% Internal functions
%% ====================================================================

%%%% synchronous implementation
%%sync(Msg,#wsstate{domain=Domain,userid=UserId}=State) ->
%%    FunReply = fun(Reply) -> ?WsReply:reply(Reply, Msg, State) end,
%%    do_rest(Msg, {Domain,UserId}, FunReply).
%%
%%%% asynchronous implementation
%%async(Msg,#wsstate{domain=Domain,userid=UserId,pid=Pid}=State) ->
%%    FunReply = fun(Reply) -> ?WsProc:apply(Pid,fun(StateF) -> ?WsReply:reply(Reply, Msg, StateF) end) end,
%%    F = fun() -> do_rest(Msg, {Domain,UserId}, FunReply) end,
%%    AsyncPid = spawn(F),
%%    Fmon = fun(normal) -> ok;
%%              (Reason) ->
%%                  ?LOG('$crash',"Websocket rest crashed: ~120tp",[Reason]),
%%                  ?LOG('$trace',"Crashed request: ~120tp",[Msg]),
%%                  FunReply({error,{internal_error,?BU:strbin("Operation crashed. See logs in '~ts'", [node()])}})
%%           end,
%%    ?BLmonitor:append_fun(AsyncPid,?MODULE,Fmon),
%%    {ok,State}.

%% semi-sync mode,
%% sync iam call, then cast from helper pid to dms in right sequence.
semi_sync(Msg,#wsstate{}=State) ->
    FunReply = fun(#wsstate{}=State1) -> {ok,State1};
                  (Reply) -> ?WsReply:reply(Reply, Msg, State) end,
    do_rest(Msg, State, FunReply).

%% ----------------------------
%% Rest query implementation
%% ----------------------------
do_rest(Msg, StateX, FunReply) ->
    case catch do_rest_1(Msg, StateX) of
        {'EXIT',Reason} ->
            ?LOG('$error',"websocket rest exception (a): ~120tp",[Reason]),
            PErrMsg = [{result,error},
                       {errormsg,?BU:strbin("Got exception. See logs of '~ts'", [node])}],
            FunReply(PErrMsg);
        {error,{Code,Reason}} ->
            PErrMsg = [{result,error},
                       {errorcode,?RESTU:get_code_from_reason({Code,Reason})},
                       {errormsg,Reason}],
            FunReply(PErrMsg);
        {ok,Reply} -> FunReply(Reply)
    end.

%% @private next
%% basic fields
do_rest_1([_,Data]=WsMsg, StateX) ->
    Operation = case ?BU:get_by_key(<<"operation">>, Data, undefined) of
                    <<"create">> -> 'create';
                    <<"replace">> -> 'replace';
                    <<"update">> -> 'update';
                    <<"delete">> -> 'delete';
                    <<"read">> -> 'read';
                    <<"lookup">> -> 'lookup';
                    <<"clear">> -> 'clear';
                    _ -> throw({error,{invalid_params,<<"Invalid operation. Expected: create,replace,update,delete,read,lookup,clear">>}})
                end,
    ClassPath = case ?BU:get_by_key(<<"classpath">>, Data, undefined) of
                    undefined -> throw({error,{invalid_params,<<"Expected 'classpath'">>}});
                    <<"/rest/v1/",_/binary>>=V -> V;
                    <<"/",_/binary>> -> throw({error,{invalid_params,<<"Invalid classpath. Should start with '/rest/v1/'">>}});
                    CN -> <<"/rest/v1/",CN/binary>>
                end,
    do_rest_2({Operation,ClassPath},WsMsg,StateX).

%% @private next
%% check IAM
do_rest_2({Operation,ClassPath},[_,Data]=WsMsg,StateX) ->
    {Domain,UserId} = extract([domain,userid],StateX),
    % IAM access
    Id = ?BU:get_by_key(<<"id">>, Data, undefined),
    Prepath = ?BU:get_by_key(<<"prepath">>, Data, undefined),
    IamUrl = case Operation of
                 O when O=='create'; O=='clear'; O=='lookup' -> ClassPath;
                 'read' when Id==undefined -> ClassPath;
                 _ when Prepath==undefined -> <<ClassPath/binary,"/",Id/binary>>;
                 _ -> <<ClassPath/binary,"/",Prepath/binary,"/",Id/binary>>
             end,
    case ?RESTU_IAM:check_access(Domain,UserId,rest_method(Operation),IamUrl) of
        {error,_}=Err -> throw(Err);
        ok -> do_rest_3({Operation,ClassPath,Id,Prepath},WsMsg,StateX)
    end.

%% @private next
%% build request to dms
do_rest_3({Operation,ClassPath,Id,Prepath},[_,Data]=WsMsg,StateX) ->
    {Domain,UserId} = extract([domain,userid],StateX),
    <<"/rest/v1/",CN/binary>> = ClassPath,
    IsColl = lists:member(Operation,['create','clear','lookup']),
    Object = if IsColl -> 'collection';
                Id==undefined andalso Operation=='read' -> 'collection';
                Id==undefined -> throw({error,{invalid_params,?BU:strbin("Expected 'id' when operation is '~ts'",[Operation])}});
                Prepath==undefined -> {'entity',Id,[]};
                true -> {'entity',Id,[{path,Prepath}]}
             end,
    Content = case maps:get(<<"content">>,Data,undefined) of
                  undefined when Operation=='read' -> #{};
                  undefined when not IsColl -> throw({error,{invalid_params,?BU:strbin("Expected 'content' when operation is '~ts'",[Operation])}});
                  C when Operation=='lookup' andalso (is_binary(C) orelse is_integer(C)) -> C;
                  C when is_map(C) -> C;
                  _ -> throw({error,{invalid_params,?BU:strbin("Invalid 'content'. Expected object",[Operation])}})
              end,
    Map0 = #{'object' => Object,
             'initiator' => {'user',UserId},
             'qs' => maps:without([<<"operation">>,<<"classpath">>,<<"id">>,<<"prepath">>,<<"content">>],Data),
             'content' => Content},
    ReadOpts = #{<<"filter">> => [<<"==">>,[<<"property">>,<<"classname">>],CN]},
    ClassItem = case ?DMS_CACHE:read_cache(Domain,?ClassesCN,ReadOpts,auto) of
                    {error,_} -> throw({error,{invalid_params,?BU:strbin("Class not found: '~ts'",[CN])}});
                    {ok,[Item],_} -> Item
                end,
    do_rest_4({Domain,Operation,ClassItem,CN,Map0,[]}, WsMsg, StateX).

%% @private next
%% to dms/appserver or helper pid
do_rest_4(DmsReq, WsMsg, #wsstate{domain=Domain}=StateX) ->
    case ?WsStore:find_data('rest_pid',StateX,undefined) of
        undefined ->
            Pid = start_caller(Domain),
            StateX1 = ?WsStore:store_data(rest_pid,Pid,StateX),
            Pid ! {call,WsMsg,DmsReq},
            {ok,StateX1};
        Pid when is_pid(Pid) ->
            Pid ! {call,WsMsg,DmsReq},
            {ok,StateX}
    end;
do_rest_4(DmsReq, _WsMsg, _StateX) ->
    case catch ?DMS_CRUD:crud(DmsReq, fun(Reply) -> Reply end) of
        {'EXIT',Reason} -> throw({error,Reason});
        {ok,Result} -> {ok,[{<<"result">>,<<"ok">>},
                            {<<"data">>, Result}]};
        ok -> {ok,[{<<"result">>,<<"ok">>}]}
    end.

%% --------
%% @private
rest_method('read') -> <<"GET">>;
rest_method('create') -> <<"POST">>;
rest_method('replace') -> <<"PUT">>;
rest_method('update') -> <<"PATCH">>;
rest_method('delete') -> <<"DELETE">>;
rest_method('clear') -> <<"CLEAR">>;
rest_method('lookup') -> <<"LOOKUP">>.

%% @private
extract([domain,userid],{Domain,UserId}) -> [Domain,UserId];
extract([domain,userid],#wsstate{domain=Domain,userid=UserId}) -> [Domain,UserId].

%% ----------------------------------------

%% @private
start_caller(Domain) ->
    Self = self(),
    spawn_link(fun() -> loop_caller(#caller{domain=Domain,wspid=Self,calls=#{}}) end).

%% @private
loop_caller(#caller{wspid=WsPid,calls=Calls}=State) ->
    receive
        {call,WsMsg,DmsReq} ->
            % DmsReq :: {Domain,Operation,ClassItem,CN,Map0,[]},
            Ref = make_ref(),
            case ?DMS_CRUD:crud_cast(DmsReq, {self(),Ref}, fun(Reply) -> Reply end) of
                ok ->
                    TimerRef = erlang:send_after(?Timeout,self(),{timeout,Ref}),
                    loop_caller(State#caller{calls=Calls#{Ref => {WsMsg,TimerRef}}});
                Reply ->
                    reply(WsPid,WsMsg,Reply),
                    loop_caller(State)
            end;
        {'$reply',Ref,Reply} ->
            case maps:get(Ref,Calls,undefined) of
                undefined -> loop_caller(State);
                {WsMsg,TimerRef} ->
                    ?BU:cancel_timer(TimerRef),
                    reply(WsPid,WsMsg,Reply),
                    loop_caller(State#caller{calls=maps:without([Ref],Calls)})
            end;
        {timeout,Ref} ->
            case maps:get(Ref,Calls,undefined) of
                undefined -> loop_caller(State);
                {WsMsg,_} ->
                    OutMsgData = [{result,error},{errormsg,<<"DMS server is not responding">>}],
                    Freply = fun(Req1,State1) -> ?WsReply:reply(OutMsgData, WsMsg, Req1, State1) end,
                    WsPid ! {apply,Freply},
                    loop_caller(State#caller{calls=maps:without([Ref],Calls)})
            end;
        _ ->
            loop_caller(State)
    end.

%% @private
reply(WsPid,WsMsg,Reply) ->
    OutMsgData = case Reply of
                     {'EXIT',Reason} ->
                         ?LOG('$error', "websocket rest dms. Exception: ~120tp",[Reason]),
                         [{result,error},{errormsg,<<"Server got exception">>}];
                     {error,{Code,Reason}} ->
                         [{result,error},{errorcode,?RESTU:get_code_from_reason({Code,Reason})},{errormsg,Reason}];
                     {error,Reason} when is_binary(Reason) ->
                         [{result,error},{errormsg,Reason}];
                     {error,Reason} ->
                         % Hide real reason, only in logs
                         ?LOG('$error', "websocket rest dms. Error: ~120tp",[Reason]),
                         [{result,error},{errorcode,500},{errormsg,?BU:strbin("See error reason in logs of '~ts'",[node()])}];
                     {ok,Result} ->
                         [{result,ok},{content,Result}];
                     ok ->
                         [{result,ok}]
                 end,
    Freply = fun(Req1,State1) -> ?WsReply:reply(OutMsgData, WsMsg, Req1, State1) end,
    WsPid ! {apply,Freply}.