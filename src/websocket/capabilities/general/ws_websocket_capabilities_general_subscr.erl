%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.06.2021
%%% @doc General capability module. Subscribe-Notify implementation

-module(ws_websocket_capabilities_general_subscr).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([subscribe/2]).

-export([handle/3]).

-export([event/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("websocket.hrl").

-define(RESTU, ws_rest_behavior_utils).
-define(RESTU_IAM, ws_rest_behavior_utils_iam).

%% ====================================================================
%% Public functions
%% ====================================================================

%% @Method
subscribe([_Method,_Data]=Msg, State) ->
    case ?PCFG:get_nodes_by_msvc(?MsvcSubscr) of
        [] -> ?WsReply:reply({error,{invalid_operation,<<"Subscriber microservice not found">>}},Msg,State);
        _ -> async(Msg,State)
    end.

%% ====================================================================
%% Handle (info) functions
%% ====================================================================
handle(info, {notify,SubscrId,EventData}, #wsstate{pid=Pid}=State) when is_binary(SubscrId), is_map(EventData) ->
    case catch jsx:encode(EventData) of
        {'EXIT',_} ->
            ?WsProc:send_event(Pid,{<<"notify">>,[{<<"invalid_data">>,?BU:to_binary(lists:flatten(io_lib:format("~tp",[EventData])))}]}),
            {ok,State};
        _ ->
            ?WsProc:send_event(Pid,{<<"notify">>,EventData}),
            {ok,State}
    end;
handle(_,_,_) -> false.

%% ====================================================================
%% Events
%% ====================================================================
%% -------------------------------------
%% when notified events (cdr and other)
%% -------------------------------------
-spec event({{Event::binary(), Opts::json_term()}, Conn::conn_data(), Timeout::integer}) -> ok.
%% -------------------------------------
event({{<<"notify">>=EvM,EventData}, Conn, _Timeout}) ->
    case catch jsx:encode(EventData) of
        {'EXIT',_} -> ?WsProc:send_event(Conn, {EvM,[{<<"invalid_data">>,?BU:to_binary(lists:flatten(io_lib:format("~tp",[EventData])))}]});
        _ -> ?WsProc:send_event(Conn, {EvM,EventData})
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% synchronous implementation
sync(Msg,#wsstate{domain=Domain,userid=UserId,pid=Pid}=State) ->
    FunReply = fun(Reply) -> ?WsReply:reply(Reply, Msg, State) end,
    StateX = #{domain=>Domain, userid=>UserId, pid=>Pid},
    do_subscribe(Msg, StateX, FunReply).

%% asynchronous implementation
async(Msg,#wsstate{domain=Domain,userid=UserId,pid=Pid}=State) ->
    FunReply = fun(Reply) -> ?WsProc:apply(Pid,fun(StateF) -> ?WsReply:reply(Reply, Msg, StateF) end) end,
    StateX = #{domain=>Domain, userid=>UserId, pid=>Pid},
    AsyncPid = spawn(fun() -> do_subscribe(Msg, StateX, FunReply) end),
    Fmon = fun(normal) -> ok;
              (Reason) ->
                    ?LOG('$crash',"Websocket subscribe crashed: ~120tp",[Reason]),
                    ?LOG('$trace',"Crashed request: ~120tp",[Msg]),
                    FunReply({error,{internal_error,?BU:strbin("Operation crashed (~tp). See logs in '~ts'", [Reason,node()])}})
           end,
    ?BLmonitor:append_fun(AsyncPid,?MODULE,Fmon),
    {ok,State}.

%% ----------------------------
%% Subscribe query implementation
%% ----------------------------
do_subscribe(Msg, StateX, FunReply) ->
    case catch do_subscribe_1(Msg, StateX) of
        {'EXIT',Reason} ->
            ?LOG('$error',"websocket subscribe exception (a): ~120tp",[Reason]),
            PErrMsg = [{result,error},
                       {errormsg,?BU:strbin("Got exception. See logs of '~ts'", [node])}],
            FunReply(PErrMsg);
        {error,{Code,Reason}} ->
            PErrMsg = [{result,error},
                       {errorcode,?RESTU:get_code_from_reason({Code,Reason})},
                       {errormsg,Reason}],
            FunReply(PErrMsg);
        {ok,Reply} -> FunReply(Reply)
    end.

%% @private next
%% events
do_subscribe_1([_,Data]=Msg, StateX) ->
    ModelEv = <<"modelevents.data_changed">>,
    case ?BU:get_by_key(<<"events">>,Data,undefined) of
        undefined -> throw({error,{invalid_params,<<"Expected 'events'">>}});
        [] -> throw({error,{invalid_params,<<"Invalid 'events'. Expected non-empty list">>}});
        ModelEv -> do_subscribe_modelevents({[ModelEv]},Msg,StateX);
        [ModelEv] -> do_subscribe_modelevents({[ModelEv]},Msg,StateX);
        [_,_]=Events ->
            case lists:member(ModelEv,Events) of
                true -> throw({error,{invalid_params,<<"Event type 'modelevents.data_changed' should be subscribed separately">>}});
                false -> do_subscribe_common({Events},Msg,StateX)
            end;
        _ -> throw({error,{invalid_params,<<"Invalid 'events'. Expected non-empty list">>}})
    end.

%% -------------------------
%% Common events
%% -------------------------
do_subscribe_common({_Events}, [_,_Data]=_Msg, _StateX) ->
    throw({error,{not_implemented,<<"Not implemented for selected events">>}}).

%% -------------------------
%% Model events
%% -------------------------
%% @private next
%% objects
do_subscribe_modelevents({Events}, [_,Data]=Msg, StateX) ->
    ClassPath = case ?BU:get_by_key(<<"objects">>,Data,undefined) of
                    undefined -> throw({error,{invalid_params,<<"Expected 'objects'">>}});
                    [] -> throw({error,{invalid_params,<<"Invalid 'objects'. Expected non-empty list or 'any'">>}});
                    [_,_] -> throw({error,{invalid_params,<<"Invalid 'objects'. Expected single classpath">>}});
                    <<"/rest/v1/",_/binary>>=V -> V;
                    <<"/",_/binary>> -> throw({error,{invalid_params,<<"Invalid 'objects'. ClassPath should start with '/rest/v1/'">>}});
                    CN when is_binary(CN) -> <<"/rest/v1/",CN/binary>>;
                    [<<"/rest/v1/",_/binary>>=V] -> V;
                    [<<"/",_/binary>>] -> throw({error,{invalid_params,<<"Invalid 'objects'. ClassPath should start with '/rest/v1/'">>}});
                    [CN] when is_binary(CN) -> <<"/rest/v1/",CN/binary>>
                end,
    do_subscribe_modelevents_1({Events,[ClassPath]}, Msg, StateX).

%% @private next
%% check IAM (for modelevents)
do_subscribe_modelevents_1({Events,[ClassPath]},Msg,StateX) ->
    [Domain,UserId] = ?BU:maps_get([domain,userid],StateX),
    <<"/rest/v1/",CN/binary>>=ClassPath,
    ReadOpts = #{<<"filter">> => [<<"==">>,[<<"property">>,<<"classname">>],CN]},
    ClassItem = case ?DMS_CACHE:read_cache(Domain,?ClassesCN,ReadOpts,auto) of
                    {error,_}=Err0 -> throw(Err0);
                    {ok,[],_} -> throw({error,{invalid_params,?BU:strbin("Class not found: '~ts'",[CN])}});
                    {ok,[Item],_} -> Item
                end,
    IamMethod = <<"GET">>,
    IamUrl = case maps:get(<<"storage_mode">>,ClassItem) of
                 SM when SM==<<"transaction_log">>; SM==<<"history">> ->
                     <<ClassPath/binary,"/2021-07-02T14:40:25.00+03:00/00000000-0000-0000-0000-000000000000">>;
                 _ ->
                    <<ClassPath/binary,"/00000000-0000-0000-0000-000000000000">>
             end,
    case ?RESTU_IAM:check_access(Domain,UserId,IamMethod,IamUrl) of
        {error,_}=Err -> throw(Err);
        ok -> do_subscribe_modelevents_2({Events,[CN]},Msg,StateX)
    end.

%% @private next
%% exargs (for modelevents)
do_subscribe_modelevents_2({Events,Objects},[_Method,Data]=Msg,StateX) ->
    ExArgs = case maps:get(<<"exargs">>,Data,#{}) of
                 ExArgs0 when is_map(ExArgs0) ->
                     FilterMap = maps:with([<<"filter">>],ExArgs0),
                     MaskMap = maps:with([<<"mask">>],ExArgs0),
                     maps:merge(FilterMap,MaskMap);
                 _ ->
                     throw({error,{invalid_params,?BU:strbin("Invalid 'exargs'. Expected object. Significant fields: 'filter', 'mask'",[])}})
             end,
    do_subscribe_modelevents_3({Events,Objects,ExArgs},Msg,StateX).

%% @private next
%% build subscription
do_subscribe_modelevents_3({Events,Objects,ExArgs}, [_Method,Data]=_Msg, StateX) ->
    [Domain,Pid] = ?BU:maps_get([domain,pid],StateX),
    TTL = min(max(?BU:to_int(?BU:get_by_key(<<"expires">>,Data,0),0),0),86400),
    SubscrId = case ?BU:get_by_key(<<"sid">>,Data,undefined) of
                   undefined -> ?BU:newid();
                   Sid -> Sid
               end,
    Value = build_subscription_value(Events,Objects,ExArgs,Pid),
    PutReq = {put, [SubscrId, Value, TTL]},
    GlobalName = ?GN_NAMING:get_globalname(?MsvcSubscr,Domain),
    case catch ?GLOBAL:gen_server_call(GlobalName, PutReq) of
        {'EXIT',Reason} ->
            ?LOG('$warning',"Subscribe in '~ts' crashed: ~120tp",[Domain,Reason]),
            {error,{internal_error,?BU:strbin("Subscriber call failed. See logs in '~ts'",[node()])}};
        {error,_}=Err ->
            ?LOG('$error',"Subscribe in '~ts' error: ~120tp",[Domain,Err]),
            Err;
        {retry_after,_,_}=Retry ->
            ?LOG('$info',"Subscribe in '~ts' got ~120tp", [Domain,Retry]),
            Retry;
        undefined ->
            ?LOG('$info',"Subscribe in '~ts' failed", [Domain]),
            {error,{internal_error,<<"Subscriber call failed">>}};
        {ok,SubscrId} -> % subscribed
            {ok,[{<<"result">>,<<"ok">>},
                 {<<"sid">>,SubscrId}]};
        ok -> % unsubscribed
            {ok,[{<<"result">>,<<"ok">>}]}
    end.

%% @private
build_subscription_value(Events,Objects,ExArgs,FromPid) ->
    #{
        <<"subscriber">> => #{
            <<"type">> => 'service',
            <<"site">> => ?PCFG:get_current_site(),
            <<"node">> => node(),
            <<"pid">> => FromPid,
            <<"ts">> => ?BU:timestamp(),
            <<"opts">> => []
        },
        <<"events">> => Events,
        <<"objects">> => Objects,
        <<"exargs">> => ExArgs
    }.