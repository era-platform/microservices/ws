%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.06.2021
%%% @doc Routes of websocket API for cowboy

-module(ws_websocket_routes).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_routes/0]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

get_routes() ->
    r("/ws", ws_websocket_handler, [{mode,<<"user">>}]).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
%% Automatically add url route to Opts so it can be found by IAM.
r(URLRoute,Handler,Opts) ->
    URLRouteB = ?BU:to_binary(URLRoute),
    [{URLRoute, Handler, [{route,URLRouteB}|Opts]}].
