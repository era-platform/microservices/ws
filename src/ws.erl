%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 18.05.2021
%%% @doc Webserver microservice app.
%%%   Application could be setup by application:set_env:
%%%      log_destination
%%%          descriptor to define log file folder and prefix.
%%%          Default: {'ws','ws'}
%%%      http_port
%%%      https_port
%%%      http_iface
%%%      https_iface
%%%      temp_path
%%%      websocket_log_level
%%%      http_log_level

-module(ws).
-author('Peter <tbotc@yandex.ru>').

-behaviour(application).

-export([start/0]).

-export([start/2,
         stop/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public
%% ====================================================================

%% -------------------------------------------
%% Start method
%% -------------------------------------------
-spec start() -> ok | {error,Reason::term()} | {retry_after,Timeout::non_neg_integer(),Reason::term()}.
%% -------------------------------------------
start() ->
    case application:ensure_all_started(?APP, permanent) of
        {ok, _Started} -> ok;
        Error -> Error
    end.

%% ===================================================================
%% Callback functions (application)
%% ===================================================================

%% -------------------------------------
%% @doc Starts application
%% -------------------------------------
start(_, _) ->
    ?LOG('$info',"~ts. Application start (~120tp)", [?APP, self()]),
    setup_dependencies(),
    ensure_deps_started(),
    ?SUPV:start_link(undefined).

%% -------------------------------------
%% @doc Stops application.
%% -------------------------------------
stop(State) ->
    ?LOG('$info',"~ts: Application stopped (~120p)", [?APP, State]),
    ok.

%% ====================================================================
%% Internal
%% ====================================================================

%% @private
setup_dependencies() ->
    % webserverlib
    RoutesWebsocket = ?RoutesWebsocket:get_routes(),
    RoutesRest = ?RoutesRest:get_routes(),
    RoutesStatic = ?RoutesStatic:get_routes(),
    Listener = #{protocol => http,
                 port => ?CFG:http_port(),
                 addr => ?CFG:http_iface(),
                 routes => RoutesWebsocket++RoutesRest++RoutesStatic},
    ?BU:set_env(?WEBSERVERLIB, listener_default, Listener),
    ?BU:set_env(?WEBSERVERLIB, 'log_destination', {ws,wslib}),
    ok.

%% --------------------------------------
%% @private
%% starts deps applications
%% --------------------------------------
ensure_deps_started() ->
    application:ensure_all_started(?PLATFORMLIB, permanent),
    application:ensure_all_started(?WEBSERVERLIB, permanent),
    WslArgs = [],
    ?WEBSERVERLIB:start(normal, WslArgs). % THIS ONE TRIGGERS START
