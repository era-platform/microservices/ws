%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.05.2021
%%% @doc Emulated domain 'noauth' ws application service supervisor

-module(ws_svc_noauth_supv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(supervisor).

-export([start_link/1]).
-export([init/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    supervisor:start_link(?MODULE, Opts).

%% ====================================================================
%% Callback functions
%% ====================================================================

init(_) ->
    Domain = ?NoAuthDomain,
    Opts = #{domain => Domain,
             sync_ref => make_ref()},
    ChildrenSpec = [{?NoAuth_Srv, {?NoAuth_Srv, start_link, [Opts]}, permanent, 1000, worker, [?NoAuth_Srv]},
                    {?NoAuth_ModifySrv, {?NoAuth_ModifySrv, start_link, [Opts]}, permanent, 1000, worker, [?NoAuth_ModifySrv]}],
    ?LOG('$info', "~ts. '~ts' supervisor inited", [?APP, Domain]),
    {ok, {{rest_for_one, 10, 2}, ChildrenSpec}}.