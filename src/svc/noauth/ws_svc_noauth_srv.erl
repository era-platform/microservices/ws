%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 26.06.2021
%%% @doc Emulated domain ?NoAuthDomain ws application service server (sessions)
%%%     Opts:
%%%         domain = ?NoAuthDomain
%%%         sync_ref

-module(ws_svc_noauth_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-record(dstate, {
    domain :: binary() | atom(),
    sync_ref :: reference(),
    ref :: reference(),
    modify_pid :: pid() | undefined,
    modify_monref :: reference() | undefined
}).

%% ====================================================================
%% Public functions
%% ====================================================================

start_link(Opts) ->
    Domain = maps:get('domain',Opts),
    GlobalName = ?GN_NAMING:get_globalname(?MsvcType,Domain),
    ?GLOBAL:gen_server_start_link({gnr,multi,GlobalName}, ?MODULE, Opts, []).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    Domain = ?BU:get_by_key('domain',Opts),
    SyncRef = ?BU:get_by_key('sync_ref',Opts),
    Self = self(),
    Ref = make_ref(),
    State = #dstate{domain = Domain,
                    sync_ref = SyncRef,
                    ref = Ref},
    % ----
    ?BLstore:store_u({?APP,sync,Domain},{'facade',SyncRef,self()}),
    Self ! {'init',Ref},
    % ----
    ?LOG('$info', "~ts. '~ts' srv inited", [?APP,Domain]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% Debug restart
handle_call({restart}, _From, State) ->
    {stop,State};

%% --------------
%% Other
handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

%% -------------
%% Other
handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% --------------
%% On register of modify srv. Skip if duplicate.
handle_info({register_srv,?DModifySrv,SyncRef,Pid}, #dstate{sync_ref=SyncRef,modify_pid=Pid}=State) -> {noreply,State};
handle_info({register_srv,?DModifySrv,SyncRef,Pid}, #dstate{sync_ref=SyncRef,domain=Domain}=State) ->
    ?LOG('$trace',"~ts. '~ts' modify-srv registered: ~p",[?APP,Domain,Pid]),
    MonRef = erlang:monitor(process,Pid),
    {noreply, State#dstate{modify_pid=Pid,
                           modify_monref=MonRef}};

%% --------------
%% DOWN modify srv
handle_info({'DOWN',MonRef,process,Pid,_Reason}, #dstate{modify_monref=MonRef,modify_pid=Pid}=State) ->
    {noreply, State#dstate{modify_pid=undefined,
                           modify_monref=undefined}};

%% --------------
%% CRUD operation on 'sessions'
handle_info({'$call', {FromPid,Ref}, {crud,?SessionsCN,{Operation,Map}}}=CallRequest, State) ->
    FunReply = fun(Reply) -> FromPid ! {'$reply',Ref,Reply} end,
    case Operation of
        O when O=='create'; O=='replace' ->
            case maps:get('content',Map,undefined) of
                undefined -> FunReply({error,{400,<<"Invalid content. Expected JSON object">>}});
                Content -> forward_in_domain(Content,FunReply,CallRequest)
            end;
        _ -> FunReply({error,{401,<<"Not logged in">>}})
    end,
    {noreply,State};

%% --------------
%% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -------------------------------------
%% Forward in destination domain
%% -------------------------------------
forward_in_domain(Content,FunReply,CallRequest) ->
    forward_in_domain_1(Content,FunReply,CallRequest).

%% @private
%% decode content json object
forward_in_domain_1(Content,FunReply,CallRequest) when is_map(Content) ->
    forward_in_domain_2(Content,FunReply,CallRequest);
%%
forward_in_domain_1(Content,FunReply,CallRequest) when is_binary(Content) ->
    Res = try jsx:decode(Content,[return_maps])
          catch _:_ -> error
          end,
    case Res of
        JSON when is_map(JSON) -> forward_in_domain_2(JSON,FunReply,CallRequest);
        _ -> FunReply({error,{400,?BU:strbin("Invalid content. Expected JSON object",[])}})
    end;
%%
forward_in_domain_1(_,FunReply,_) ->
    FunReply({error,{400,?BU:strbin("Invalid content. Expected JSON object",[])}}).

%% @private
%% extract domain
forward_in_domain_2(JSON,FunReply,CallRequest) ->
    Domain = case ?WsUtils:has_dc() of
                 false -> 'root';
                 true -> maps:get(<<"domain">>,JSON,undefined)
             end,
    case Domain of
        undefined -> FunReply({error,{400,<<"'domain' not specified">>}});
        _ -> forward_in_domain_3(Domain,FunReply,CallRequest)
    end.

%% @private
%% forward request in domain
forward_in_domain_3(Domain,FunReply,CallRequest) ->
    GnWs = ?GN_NAMING:get_globalname(?MsvcType,Domain),
    case ?GN_REG:whereis_name(GnWs) of
        undefined ->
            GnDms = ?GN_NAMING:get_globalname(?MsvcDms,Domain),
            case ?GN_REG:whereis_name(GnDms) of
                undefined -> FunReply({error,{404,<<"Domain not found">>}});
                Pid when is_pid(Pid) -> FunReply({error,{500,<<"Webserver session service not accessible">>}})
            end;
        Pid when is_pid(Pid) ->
            Pid ! CallRequest
    end.