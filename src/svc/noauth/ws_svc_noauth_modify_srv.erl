%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 25.06.2021
%%% @doc Modify/worker gen-server for ws emulated domain 'noauth' service
%%%     Opts:
%%%         domain = ?NoAuthDomain
%%%         sync_ref

-module(ws_svc_noauth_modify_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([work/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-record(dstate, {
    domain :: binary() | atom(),
    sync_ref :: reference(),
    ref :: reference()
}).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link(?MODULE, Opts, []).

%% cast work to modify srv
work(Pid,SyncRef,Fun) ->
    gen_server:cast(Pid, {work,SyncRef,Fun}).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    Domain = ?BU:get_by_key('domain',Opts),
    SyncRef = ?BU:get_by_key('sync_ref',Opts),
    Ref = make_ref(),
    State = #dstate{domain = Domain,
                    sync_ref = SyncRef,
                    ref = Ref},
    % ----
    self() ! {'init',Ref},
    % ----
    ?LOG('$info', "~ts. '~ts' modify srv inited", [?APP,Domain]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast({work,SyncRef,Fun}, #dstate{sync_ref=SyncRef}=State) ->
    Fun(),
    {noreply, State};

%% --------------
%% Other
handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% --------------
%% init. sync to facade
handle_info({'init',Ref}, #dstate{ref=Ref,domain=Domain,sync_ref=SyncRef}=State) ->
    State1 = case ?BLstore:find_u({?APP,sync,Domain}) of
                 false -> State;
                 {_,{facade,SyncRef,Pid}} when is_pid(Pid) ->
                     case process_info(Pid,status) of
                         {status,_} -> Pid ! {register_srv,?MODULE,SyncRef,self()};
                         undefined -> erlang:send_after(1000,self(),{'init',Ref})
                     end,
                     State
             end,
    {noreply, State1};

% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================
