%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 25.06.2021
%%% @doc Search gen-server for ws domain service
%%%     Opts:
%%%         domain
%%%         ets_data
%%%         ets_hash
%%%         sync_ref

-module(ws_svc_domain_search_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-record(dstate, {
    domain :: binary() | atom(),
    sync_ref :: reference(),
    ref :: reference(),
    ets_data :: ets:tab(),
    ets_hash :: ets:tab()
}).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link(?MODULE, Opts, []).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    Domain = ?BU:get_by_key(domain,Opts),
    SyncRef = ?BU:get_by_key(sync_ref,Opts),
    Ref = make_ref(),
    State = #dstate{domain = Domain,
                    sync_ref = SyncRef,
                    ref = Ref,
                    ets_data = ?BU:get_by_key(ets_data,Opts),
                    ets_hash = ?BU:get_by_key(ets_hash,Opts)},
    % ----
    self() ! {'init',Ref},
    % ----
    ?LOG('$info', "~ts. '~ts' search srv inited", [?APP,Domain]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

%% --------------
%% find_session
%% return {ok,Session::map()} | false
handle_cast({request,{'find_session',[Id]},FunReply}, #dstate{ets_data=EtsData}=State) ->
    Reply = case ets:lookup(EtsData,Id) of
                [] -> false;
                [{_,SessionItem}] -> {ok,SessionItem}
            end,
    FunReply(Reply),
    {noreply, State};

%% --------------
%% get_user_sessions
%% return {ok,SessionIds::[binary()]} | false
handle_cast({request,{'get_user_sessions',[UserId]},FunReply}, #dstate{ets_hash=EtsHash}=State) ->
    Reply = case ets:lookup(EtsHash,UserId) of
                [] -> false;
                [{_,Ids}] -> {ok,Ids}
            end,
    FunReply(Reply),
    {noreply, State};

%% --------------
%% Other
handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% --------------
%% init. sync to facade
handle_info({'init',Ref}, #dstate{ref=Ref,domain=Domain,sync_ref=SyncRef}=State) ->
    State1 = case ?BLstore:find_u({?APP,sync,Domain}) of
                 false -> State;
                 {_,{facade,SyncRef,Pid}} when is_pid(Pid) ->
                     case process_info(Pid,status) of
                         {status,_} -> Pid ! {register_srv,?MODULE,SyncRef,self()};
                         undefined -> erlang:send_after(1000,self(),{'init',Ref})
                     end,
                     State
             end,
    {noreply, State1};

% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

