%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 26.06.2021
%%% @doc Routines of session modification operations: create, replace, update.
%%%      Called from domain gen_server

-module(ws_svc_domain_crud).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([create_session/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -------------------------------------
%% Authenticate by create session
%% -------------------------------------
create_session(FunReply,CallRequest,State) ->
    {'$call',_,{crud,_,{_Operation,Map}}}=CallRequest,
    Content = maps:get('content',Map,undefined),
    create_session_1(Content,FunReply,CallRequest,State).

%% @private
%% decode content json object
create_session_1(Content,FunReply,CallRequest,State) when is_map(Content) ->
    create_session_2(Content,FunReply,CallRequest,State);
%%
create_session_1(Content,FunReply,CallRequest,State) when is_binary(Content) ->
    Res = try jsx:decode(Content,[return_maps])
          catch _:_ -> error
          end,
    case Res of
        JSON when is_map(JSON) -> create_session_2(JSON,FunReply,CallRequest,State);
        _ -> FunReply({error,{400,?BU:strbin("Invalid content. Expected JSON object",[])}})
    end;
%%
create_session_1(_,FunReply,_,_) ->
    FunReply({error,{400,?BU:strbin("Invalid content. Expected JSON object",[])}}).

%% @private
%% extract domain
create_session_2(JSON,FunReply,CallRequest,State) ->
    Domain = maps:get('domain',State), % real domain
    Dom = case ?WsUtils:has_dc() of
              false -> 'root';
              true -> maps:get(<<"domain">>,JSON,undefined)
          end,
    case Dom of
        undefined -> FunReply({error,{400,<<"'domain' not specified">>}});
        Domain -> create_session_3(Domain,JSON,FunReply,CallRequest,State);
        <<"root">> when Domain=='root' -> create_session_3(Domain,JSON,FunReply,CallRequest,State);
        _ -> FunReply({error,{400,?BU:strbin("Invalid 'domain'. Should match current domain.",[])}})
    end.

%% @private
%% extract login, password
create_session_3(Domain,JSON,FunReply,CallRequest,State) ->
    case ?WsUtils:has_iam() of
        false ->
            UserId = ?BU:emptyid(),
            create_session_4(UserId,Domain,FunReply,CallRequest,State);
        true ->
            case {maps:get(<<"login">>,JSON,undefined), maps:get(<<"password">>,JSON,undefined)} of
                {undefined,_} -> FunReply({error,{400,<<"'login' not specified">>}});
                {_,undefined} -> FunReply({error,{400,<<"'password' not specified">>}});
                {Login,Pwd} ->
                    GnIam = ?GN_NAMING:get_globalname(?MsvcIam,Domain),
                    case catch ?GLOBAL:gen_server_call(GnIam,{authenticate,[login,Login,Pwd]}) of
                        {'EXIT',_} -> FunReply({error,500,<<"IAM call crashed">>});
                        {error,_}=Err ->  FunReply(Err);
                        {ok,UserId} -> create_session_4(UserId,Domain,FunReply,CallRequest,State)
                    end end end.

%% @private
%% create session object
create_session_4(UserId,Domain,FunReply,CallRequest,State) ->
    SessionItem = #{<<"id">> => ?BU:newid(),
                    <<"userid">> => UserId,
                    <<"domain">> => Domain,
                    <<"expires">> => ?SessionTTL, % in seconds
                    <<"data">> => #{}},
    create_session_5(SessionItem,UserId,Domain,FunReply,CallRequest,State).

%% @private
%% push session to dms
create_session_5(SessionItem,_UserId,Domain,FunReply,CallRequest,State) ->
    {'$call',_,{crud,?SessionsCN,{Operation,Map}}}=CallRequest,
    Map1 = Map#{'content' => SessionItem},
    CrudReq1 = {crud,?SessionsCN,{Operation,Map1}},
    Fun = fun() ->
                GlobalName = ?GN_NAMING:get_globalname(?MsvcDms,Domain),
                Res = case catch ?GLOBAL:gen_server_call(GlobalName, CrudReq1, 30000) of
                          {ok,SessionItem1} when is_map(SessionItem1) ->
                              SessionId = maps:get(<<"id">>,SessionItem1),
                              {ok,SessionItem1#{<<"token">> => ?MakeToken(SessionId,Domain)}};
                          T -> T
                      end,
                FunReply(Res)
          end,
    ModifyPid = maps:get(modify_pid,State),
    SyncRef = maps:get(sync_ref,State),
    ?DModifySrv:work(ModifyPid,SyncRef,Fun).

%% ====================================================================
%% Internal functions
%% ====================================================================