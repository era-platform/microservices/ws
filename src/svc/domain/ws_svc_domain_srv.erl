%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 25.06.2021
%%% @doc Domain ws application service server (sessions)
%%%     Opts:
%%%         domain
%%%         ets_data
%%%         ets_hash
%%%         sync_ref

-module(ws_svc_domain_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-record(dstate, {
    domain :: binary() | atom(),
    sync_ref :: reference(),
    %
    subscrid :: binary(),
    %
    ref :: reference(),
    timerref :: reference(),
    %
    ets_data :: ets:tab(),
    ets_hash :: ets:tab(),
    %
    is_paused = false :: boolean(),
    pause_reason :: term(),
    %
    stored_events = [] :: list(),
    %
    search_pid :: pid(),
    search_monref :: reference(),
    modify_pid :: pid(),
    modify_monref :: reference(),
    %
    start_ts :: non_neg_integer(),
    loaded_ts :: non_neg_integer()
}).

-define(TimeoutInit, 10000).
-define(TimeoutRegular, 60000).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    Domain = ?BU:get_by_key('domain',Opts),
    GlobalName = ?GN_NAMING:get_globalname(?MsvcType,Domain),
    ?GLOBAL:gen_server_start_link({gnr,multi,GlobalName}, ?MODULE, Opts, []).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    Domain = ?BU:get_by_key(domain,Opts),
    SyncRef = ?BU:get_by_key(sync_ref,Opts),
    Self = self(),
    Ref = make_ref(),
    State = #dstate{domain = Domain,
                    sync_ref = SyncRef,
                    subscrid = SubscrId=?BU:strbin("sid_WS_sessions:~ts;~ts",[node(),Domain]),
                    ref = Ref,
                    start_ts = ?BU:timestamp(),
                    ets_data = ?BU:get_by_key(ets_data,Opts),
                    ets_hash = ?BU:get_by_key(ets_hash,Opts)},
    % ----
    ?BLstore:store_u({?APP,sync,Domain},{'facade',SyncRef,self()}),
    Self ! {'init',Ref},
    % ----
    SubscrOpts = #{<<"ttl_first">> => 20,
                   fun_report => fun(Report) -> Self ! {'subscriber_report',SyncRef,Report} end},
    ?DMS_SUBSCR:subscribe_async(?APP,Domain,?SessionsCN,self(),SubscrId,SubscrOpts),
    % ----
    ?LOG('$info', "~ts. '~ts' srv inited", [?APP,Domain]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% Debug restart
handle_call({restart}, _From, State) ->
    {stop,State};

%% --------------
%% Search
handle_call(_, _From, #dstate{is_paused=true,pause_reason=PauseReason}=State) ->
    {reply,PauseReason,State};
%%
handle_call(_, _From, #dstate{search_pid=undefined}=State) ->
    RetryTimeout = retry_timeout(State),
    Reply = {retry_after,RetryTimeout,<<"Search-srv process is not ready yet">>},
    {reply,Reply,State};
%%
handle_call({find_session,_Args}=Req, From, #dstate{search_pid=Pid}=State) ->
    FunReply = fun(Reply) -> gen_server:reply(From,Reply) end,
    gen_server:cast(Pid, {request,Req,FunReply}),
    {noreply,State};
%%
handle_call({get_user_sessions,_Args}=Req, From, #dstate{search_pid=Pid}=State) ->
    FunReply = fun(Reply) -> gen_server:reply(From,Reply) end,
    gen_server:cast(Pid, {request,Req,FunReply}),
    {noreply,State};

%% --------------
%% Other
handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

%% -------------
%% Other
handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% --------------
%% init
handle_info({init,Ref}, #dstate{ref=Ref}=State) ->
    State1 = init_data(State),
    {noreply, State1};

%% --------------
%% subscribe helper report (state changed)
handle_info({'subscriber_report',SyncRef,Report}, #dstate{sync_ref=SyncRef,timerref=TimerRef,loaded_ts=LoadedTS}=State) ->
    State1 = case Report of
                 ok ->
                     Timeout = case ?BU:timestamp() of
                                   NowTS when LoadedTS/=undefined, NowTS - LoadedTS < 5000 -> LoadedTS + 5000 - NowTS;
                                   _ -> 0
                               end,
                     ?BU:cancel_timer(TimerRef),
                     Ref1 = make_ref(),
                     State#dstate{ref=Ref1,
                                  timerref = erlang:send_after(Timeout, self(), {timer,init,Ref1})};
                 _ -> State
             end,
    {noreply, State1};

%% --------------
%% Init timer expired
handle_info({timer,'init',Ref}, #dstate{ref=Ref}=State) ->
    State1 = init_data(State),
    {noreply, State1};

%% --------------
%% Init timer expired
handle_info({timer,'regular',Ref}, #dstate{ref=Ref}=State) ->
    Ref1 = make_ref(),
    State1 =  State#dstate{ref=Ref1,
                           timerref = erlang:send_after(?TimeoutRegular, self(), {timer,regular,Ref1})},
    {noreply, State1};

%% --------------
%% On register of search srv. Skip if duplicate.
handle_info({register_srv,?DSearchSrv,SyncRef,Pid}, #dstate{sync_ref=SyncRef,search_pid=Pid}=State) -> {noreply,State};
handle_info({register_srv,?DSearchSrv,SyncRef,Pid}, #dstate{sync_ref=SyncRef,domain=Domain}=State) ->
    ?LOG('$trace',"~ts. '~ts' search-srv registered: ~p",[?APP,Domain,Pid]),
    MonRef = erlang:monitor(process,Pid),
    {noreply, State#dstate{search_pid=Pid,
                           search_monref=MonRef}};

%% --------------
%% On register of modify srv. Skip if duplicate.
handle_info({register_srv,?DModifySrv,SyncRef,Pid}, #dstate{sync_ref=SyncRef,modify_pid=Pid}=State) -> {noreply,State};
handle_info({register_srv,?DModifySrv,SyncRef,Pid}, #dstate{sync_ref=SyncRef,domain=Domain}=State) ->
    ?LOG('$trace',"~ts. '~ts' modify-srv registered: ~p",[?APP,Domain,Pid]),
    MonRef = erlang:monitor(process,Pid),
    {noreply, State#dstate{modify_pid=Pid,
                           modify_monref=MonRef}};

%% --------------
%% DOWN search srv
handle_info({'DOWN',MonRef,process,Pid,_Reason}, #dstate{search_monref=MonRef,search_pid=Pid}=State) ->
    {noreply, State#dstate{search_pid=undefined,
                           search_monref=undefined}};

%% --------------
%% DOWN modify srv
handle_info({'DOWN',MonRef,process,Pid,_Reason}, #dstate{modify_monref=MonRef,modify_pid=Pid}=State) ->
    {noreply, State#dstate{modify_pid=undefined,
                           modify_monref=undefined}};

%% --------------
%% On notify from DMS about changes in subscriptions
handle_info({notify,SubscrId,EventInfo}, #dstate{subscrid=SubscrId,is_paused=true,stored_events=StoredEvents}=State) ->
    State1 = State#dstate{stored_events=[EventInfo|StoredEvents]},
    {noreply, State1};
%%
handle_info({notify,SubscrId,EventInfo}, #dstate{subscrid=SubscrId}=State) ->
    State1 = apply_changed_event(EventInfo, State),
    {noreply, State1};

%% --------------
%% CRUD operation
handle_info({'$call', {FromPid,Ref}, {crud,_,_}=_CrudReq}, #dstate{modify_pid=undefined}=State) ->
    RetryTimeout = retry_timeout(State),
    Reply = {retry_later,RetryTimeout,State},
    FromPid ! {'$reply',Ref,Reply},
    {noreply,State};
%% --------------
%% CRUD operation on 'sessions'
handle_info({'$call', {FromPid,Ref}, {crud,?SessionsCN,{Operation,Map}}=CrudReq}=CallRequest,
            #dstate{domain=Domain,sync_ref=SyncRef,modify_pid=ModifyPid,search_pid=SearchPid}=State) ->
    FunReply = fun(Reply) -> FromPid ! {'$reply',Ref,Reply} end,
    case {Operation,maps:get('object',Map)} of
        {'create','collection'} ->
            case maps:get('session_id',Map,undefined) of
                undefined ->
                    % to worker module
                    StateX = #{domain=>Domain,sync_ref=>SyncRef,modify_pid=>ModifyPid,search_pid=>SearchPid},
                    ?DCrud:create_session(FunReply,CallRequest,StateX);
                _ -> FunReply({error,{invalid_operation,<<"Session already exists">>}})
            end;
        _ ->
            Fun = fun() ->
                        GlobalName = ?GN_NAMING:get_globalname(?MsvcDms,Domain),
                        Res = (catch ?GLOBAL:gen_server_call(GlobalName, CrudReq, 30000)),
                        FunReply(Res)
                  end,
            ?DModifySrv:work(ModifyPid,SyncRef,Fun)
    end,
    {noreply,State};

%% --------------
%% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ------------------------------------------
%% initialization of data. Load all subscriptions from DMS.
%% ------------------------------------------
init_data(#dstate{}=State) ->
    State1 = State#dstate{stored_events=[]},
    Ref1 = make_ref(),
    case catch load(State1) of
        {ok,State2} ->
            State3 = State2#dstate{is_paused = false,
                                   pause_reason = undefined,
                                   loaded_ts = ?BU:timestamp(),
                                   ref=Ref1,
                                   timerref = erlang:send_after(?TimeoutRegular, self(), {timer,regular,Ref1})},
            apply_changed_events(State3);
        {error,_}=Err ->
            State1#dstate{is_paused = true,
                          pause_reason = Err,
                          ref=Ref1,
                          timerref = erlang:send_after(?TimeoutInit, self(), {timer,init,Ref1})}
    end.

%% @private
load(#dstate{domain=Domain,ets_data=EtsData}=State) ->
    ets:delete_all_objects(EtsData),
    case ?DMS_CACHE:read_direct(Domain,?SessionsCN,#{}) of
        {error,_}=Err -> throw(Err);
        {ok,Items} ->
            lists:foreach(fun(Item) -> ets:insert(EtsData,{maps:get(<<"id">>,Item),shrink(Item)}) end, Items)
    end,
    build_hashes(State),
    {ok,State}.

%% @private
shrink(Item) -> Item.

%% @private
build_hashes(#dstate{ets_data=EtsData,ets_hash=EtsHash}) ->
    ets:delete_all_objects(EtsHash),
    ets:foldl(fun({Id,Item},_) ->
                    UserId = maps:get(<<"userid">>,Item),
                    hash_insert(EtsHash,UserId,Id)
              end, ok, EtsData).

%% @private
hash_insert(EtsHash,UserId,Id) ->
    case ets:lookup(EtsHash,UserId) of
        [] -> ets:insert(EtsHash,{UserId,[Id]});
        [{_,Ids}] -> ets:insert(EtsHash,{UserId,[Id|Ids]})
    end.

%% @private
hash_remove(EtsHash,UserIdPrev,Id) ->
    case ets:lookup(EtsHash,UserIdPrev) of
        [] -> ok;
        [{_,[Id]}] -> ets:delete(EtsHash,UserIdPrev);
        [{_,[_]}] -> ok;
        [{_,Ids}] -> ets:insert(EtsHash,{UserIdPrev,lists:delete(Id,Ids)})
    end.

%% ------------------------------------------
%% Applying of events about changes in subscriptions
%% ------------------------------------------
apply_changed_events(#dstate{stored_events=StoredEvents}=State) ->
    State1 = lists:foldr(fun(EventInfo,StateX) -> apply_changed_event(EventInfo,StateX) end, State, StoredEvents),
    State1#dstate{stored_events=[]}.

%% Apply one event
apply_changed_event(EventInfo, #dstate{domain=Domain,ets_data=EtsData,ets_hash=EtsHash,timerref=TimerRef,loaded_ts=LoadedTS}=State) ->
    Ftimeout = fun(GuaranteeTimeout) ->
                        case ?BU:timestamp() of
                            NowTS when NowTS - LoadedTS > GuaranteeTimeout -> 0;
                            NowTS -> GuaranteeTimeout - (NowTS - LoadedTS)
                        end end,
    FunReload = fun(GuaranteeTimeout) ->
                        ?BU:cancel_timer(TimerRef),
                        Ref1 = make_ref(),
                        State#dstate{ref=Ref1,
                                     timerref = erlang:send_after(Ftimeout(GuaranteeTimeout), self(), {timer,init,Ref1})}
                end,
    Data = maps:get(<<"data">>,EventInfo),
    case maps:get(<<"operation">>,Data) of
        'clear' ->
            ets:delete_all_objects(EtsData),
            ets:delete_all_objects(EtsHash),
            State;
        'reload' ->
            FunReload(5000);
        O ->
            Item = maps:get(<<"entity">>,Data),
            Id = maps:get(<<"id">>,Item),
            UserId = maps:get(<<"userid">>,Item),
            case O of
                'create' ->
                    ItemShrink = shrink(Item),
                    ets:insert(EtsData,{Id,ItemShrink}),
                    hash_insert(EtsHash,UserId,Id),
                    State;
                'update' ->
                    [{_,ItemPrev}] = ets:lookup(EtsData,Id),
                    case maps:get(<<"userid">>,ItemPrev) of
                        UserId -> ok;
                        UserIdPrev ->
                            hash_insert(EtsHash,UserId,Id),
                            hash_remove(EtsHash,UserIdPrev,Id)
                    end,
                    ItemShrink = shrink(Item),
                    ets:insert(EtsData,{Id,ItemShrink}),
                    State;
                'delete' ->
                    ets:delete(EtsData,Id),
                    hash_remove(EtsHash,UserId,Id),
                    State;
                'corrupt' ->
                    spawn(fun() -> on_corrupt(Domain,Item,{EtsData,EtsHash}) end),
                    State
            end
    end.

%% @private
%% Async process
on_corrupt(Domain,Item,{EtsData,EtsHash}) ->
    Id = maps:get(<<"id">>,Item),
    UserId = maps:get(<<"userid">>,Item),
    GlobalName = ?GN_NAMING:get_globalname(?MsvcDms,Domain),
    Args = {'read',#{class => ?SessionsCN,
                     object => {'entity',Id,[]},
                     qs => #{}}},
    case catch ?GLOBAL:gen_server_call(GlobalName,{crud,?SessionsCN,Args}) of
        {'EXIT',Reason} ->
            ?LOG('$crash',"Call to DMS (read subscription '~ts', id='~ts', userid='~ts') crashed: ~n\t~120tp",[Domain,Id,UserId,Reason]),
            ok;
        {error,_}=Err ->
            ?LOG('$error',"Call to DMS (read subscription '~ts', id='~ts', userid='~ts') error: ~n\t~120tp",[Domain,Id,UserId,Err]),
            ets:delete(EtsData,Id),
            hash_remove(EtsHash,UserId,Id);
        {ok,Item} ->
            ItemShrink = shrink(Item),
            ets:insert(EtsData,{Id,ItemShrink}),
            hash_insert(EtsHash,UserId,Id)
    end.

%% -------------------------------------
%% @private
retry_timeout(#dstate{start_ts=StartTS}) ->
    Diff = ?BU:timestamp() - StartTS,
    erlang:min(5000,erlang:max(20,Diff)).

%% ---------------------------------------------------------------------

