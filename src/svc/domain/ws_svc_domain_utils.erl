%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 25.06.2021
%%% @doc

-module(ws_svc_domain_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([build_data_name/1,
         build_hash_name/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

build_data_name(Domain) ->
    ?BU:to_atom_new(?BU:strbin("ws_map#~ts", [Domain])).

build_hash_name(Domain) ->
    ?BU:to_atom_new(?BU:strbin("ws_hash#~ts", [Domain])).

%% ====================================================================
%% Internal functions
%% ====================================================================