%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 25.06.2021
%%% @doc Controlled classes descriptions (for dms collection 'classes')

-module(ws_svc_fixture_coll_classes).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([sessions/1,
         connections/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -------------------------------------
%% CLASS "sessions"
%% return fixtured entity of controlled class
%% -------------------------------------
sessions(Domain) ->
    #{
        <<"id">> => ?BU:to_guid(<<"84887008-017a-423d-4a5b-7cd30a921f58">>),
        <<"classname">> => ?SessionsCN,
        <<"name">> => <<"sessions">>,
        <<"description">> => <<"General platform collection. Sessions of webserver.">>,
        <<"storage_mode">> => <<"runtime">>,
        <<"cache_mode">> => <<"full">>,
        <<"integrity_mode">> => <<"sync_fast_read">>,
        <<"opts">> => #{
            <<"validator">> => ?BU:strbin("~ts:validator",[?MsvcType]),
            <<"appserver">> => ?GN_NAMING:get_globalname(?MsvcType,Domain),
            <<"notify">> => true,
            <<"check_required_fill_defaults">> => true,
            <<"replace_without_read">> => false, % ! to block changes of userid
            <<"notify_integrity_timeout">> => 5000,
            <<"max_limit">> => 1000,
            <<"max_size">> => 1000000,
            <<"expires_mode">> => <<"modify">>,
            <<"expires_ttl_property">> => <<"expires">>,
            <<"expires_ts_property">> => <<"timestamp">>,
            <<"lookup_properties">> => [<<"id">>]
        },
        <<"properties">> => [
            #{
                <<"name">> => <<"expires">>,
                <<"data_type">> => <<"integer">>,
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"timestamp">>,
                <<"data_type">> => <<"long">>,
                <<"required">> => false,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"data">>,
                <<"data_type">> => <<"any">>,
                <<"required">> => false,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"userid">>,
                <<"data_type">> => <<"uuid">>,
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"domain">>,
                <<"data_type">> => <<"string">>,
                <<"required">> => true,
                <<"multi">> => false
            }
        ]}.

%% -------------------------------------
%% CLASS "connections"
%% return fixtured entity of controlled class
%% -------------------------------------
connections(_Domain) ->
    #{
        <<"id">> => ?BU:to_guid(<<"1d661cb9-017a-5800-26b6-7cd30a921f58">>),
        <<"classname">> => ?ConnectionsCN,
        <<"name">> => <<"Authenticated connections">>,
        <<"description">> => <<"General platform collection. Authenticated connections.">>,
        <<"storage_mode">> => <<"runtime">>,
        <<"cache_mode">> => <<"full">>,
        <<"integrity_mode">> => <<"sync_fast_read">>,
        <<"opts">> => #{
            <<"validator">> => ?BU:strbin("~ts:validator",[?MsvcType]),
            %<<"appserver">> => ?GN_NAMING:get_globalname(?MsvcType,Domain),
            <<"notify">> => true,
            <<"check_required_fill_defaults">> => true,
            <<"replace_without_read">> => false, % ! to block changes of userid
            <<"notify_integrity_timeout">> => 5000,
            <<"max_limit">> => 1000,
            <<"max_size">> => 1000000,
            <<"expires_mode">> => <<"modify">>,
            <<"expires_ttl_property">> => <<"expires">>,
            <<"expires_ts_property">> => <<"timestamp">>,
            <<"lookup_properties">> => [<<"id">>]
        },
        <<"properties">> => [
            #{
                <<"name">> => <<"expires">>,
                <<"data_type">> => <<"integer">>,
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"timestamp">>,
                <<"data_type">> => <<"long">>,
                <<"required">> => false,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"domain">>,
                <<"data_type">> => <<"string">>,
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"userid">>,
                <<"data_type">> => <<"uuid">>,
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"session_id">>,
                <<"data_type">> => <<"uuid">>,
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"capabilities">>,
                <<"data_type">> => <<"string">>,
                <<"required">> => false,
                <<"multi">> => true
            },
            #{
                <<"name">> => <<"remote_ip">>,
                <<"data_type">> => <<"string">>,
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"dt_start">>,
                <<"data_type">> => <<"datetime">>,
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"type">>,
                <<"data_type">> => <<"enum">>,
                <<"items">> => [<<"websocket">>],
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"info">>, % endpoint, pid, node, site,
                <<"data_type">> => <<"any">>,
                <<"merge_levels">> => 1,
                <<"required">> => false,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"setup_info">>, % app_path, params
                <<"data_type">> => <<"any">>,
                <<"merge_levels">> => 1,
                <<"required">> => false,
                <<"multi">> => false
            }
        ]}.

%% ====================================================================
%% Internal functions
%% ====================================================================