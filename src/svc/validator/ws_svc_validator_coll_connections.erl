%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 29.06.2021
%%% @doc Validates 'connections' entity on any modify operation.

-module(ws_svc_validator_coll_connections).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([validate/4]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------------------------------
%% Validate storage entity on any modify operation
%% --------------------------------------
-spec validate(Domain::binary() | atom(),
               Operation :: create | replace | update | delete,
               EntityInfo::{Id::binary(),E0::map(),E1::map()} | undefined,
               ModifierInfo::{Type::atom(),Id::binary()} | undefined)
      -> {ok,Entity::map()} | {error,Reason::term()}.
%% --------------------------------------
validate(Domain,Operation,{_Id,E0,E1}=_EntityInfo,_ModifierInfo) ->
    FunIsFixt = fun(_E) -> false end,
    IsFixtureEntity = FunIsFixt(E0) orelse FunIsFixt(E1),
    case IsFixtureEntity of
        _ when Operation=='delete' ->
            {ok,undefined};
        _ ->
            case check_entity(Domain,Operation,{E0,E1}) of
                {error,_}=Err -> Err;
                {ok,EntityV} -> {ok,EntityV}
            end end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -------------------------------------
%% Check storage properties
%% -------------------------------------
check_entity(Domain,Operation,{Item0,Item}) ->
    check_1(Domain,Operation,{Item0,Item}).

%% not changed domain and userid
check_1(Domain,Operation,{undefined,Item}) -> check_x(Domain,Operation,Item);
check_1(Domain,Operation,{Item0,Item1}) when Operation=='replace'; Operation=='update' ->
    F = fun(Key,Item) -> maps:get(Key,Item,undefined) end,
    case {F(<<"userid">>,Item0),F(<<"userid">>,Item1),F(<<"domain">>,Item0),F(<<"domain">>,Item1)} of
        {U,U,D,D} -> check_2(Domain,Operation,Item1);
        {U,U,_,_} -> {error,{invalid_operation,<<"field=domain|Change of 'domain' is restricted">>}};
        {_,_,D,D} -> {error,{invalid_operation,<<"field=userid|Change of 'userid' is restricted">>}}
    end;
check_1(Domain,Operation,{_Item0,Item}) -> check_2(Domain,Operation,Item).

%% check pid, node, site (ensure not from WS API directly)
check_2(Domain,Operation,Item) ->
    case {maps:get(<<"type">>,Item), maps:get(<<"info">>,Item,undefined)} of
        {<<"websocket">>, Map} when is_map(Map) ->
            case {maps:get(pid,Map,undefined), maps:get(node,Map,undefined), maps:get(site,Map,undefined)} of
                {Pid,Node,Site} when is_pid(Pid), is_atom(Node), is_binary(Site) -> check_x(Domain,Operation,Item);
                _ -> {error,{access_denied,<<"Restricted for direct setup">>}}
            end;
        _ -> {error,{access_denied,<<"Restricted for direct setup">>}}
    end.

%% final
check_x(_Domain,_Operation,Item) -> {ok,Item}.